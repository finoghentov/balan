<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactFormRequest;
use App\Models\Advantage;
use App\Models\Competence;
use App\Models\ObjectType;
use App\Models\Partner;
use App\Models\Portfolio;
use App\Models\ServiceType;
use Illuminate\Http\Request;
use Telegram\Bot\Laravel\Facades\Telegram;

class SiteController extends Controller
{
    public function index(Request $request)
    {
        $competences = Competence::query()->with('media')->where(['active' => true])->orderBy('order')->get();
        $advantages = Advantage::query()->where(['active' => true])->orderBy('order')->get();
        $portfolio = Portfolio::query()->where(['active' => true])->orderBy('order')->get();
        $objectTypes = ObjectType::query()->where(['active' => true])->orderBy('order')->get();
        $serviceTypes = ServiceType::query()->where(['active' => true])->orderBy('order')->get();
        $partners = Partner::query()->where(['active' => true])->inRandomOrder()->get();

        return view('site.index', compact(
            'competences', 'advantages', 'portfolio', 'partners', 'objectTypes', 'serviceTypes'
        ));
    }

    public function contact(ContactFormRequest $request)
    {


        $chatIds = explode(',', config('telegram.chat_ids'));

        $objectType = ObjectType::query()->whereId($request->object_type)->first() ?? new ObjectType();
        $serviceType = ServiceType::query()->whereId($request->service_type)->first() ?? new ServiceType();
        $message = <<<TEXT
Новый клиент
Имя: $request->name
Телефон: $request->phone
Тип объекта: $objectType->title
Тип услуги: $serviceType->title
TEXT;

        foreach ($chatIds as $id) {
            Telegram::sendMessage([
                'chat_id' => $id,
                'text' => $message
            ]);
        }

        return redirect()->to(url()->previous().'#contact-us')->with('success', __('site.success_messages'));
    }
}
