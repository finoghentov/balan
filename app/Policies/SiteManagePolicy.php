<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Spatie\Permission\Models\Role;

class SiteManagePolicy
{
    use HandlesAuthorization;


    public function viewAny(User $user): bool
    {
        return $user->can('manage site');
    }

    /**
     * @param User $user
     * @param User $interactUser
     * @return bool
     */
    public function view(User $user): bool
    {
        return $user->can('manage site');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->can('manage site');
    }

    /**
     * @param User $user
     * @param User $interactUser
     * @return bool
     */
    public function update(User $user): bool
    {
        return $user->can('manage site');
    }

    /**
     * @param User $user
     * @param User $interactUser
     * @return bool
     */
    public function delete(User $user): bool
    {
        return $user->can('manage site');
    }
}
