<?php

namespace App\Policies;

use App\Models\User;
use App\Nova\NovaSettings\General;
use Illuminate\Auth\Access\HandlesAuthorization;
use Spatie\Permission\Models\Role;
use Stepanenko3\NovaSettings\Models\Settings;

class SettingPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasAnyRole(
            Role::query()->whereIn('name', config('nova-settings.roles'))->get()
        );
    }

    public function view(User $user)
    {
        return $user->hasAnyRole(
            Role::query()->whereIn('name', config('nova-settings.roles'))->get()
        );
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasRole(
            Role::whereName(config('permission.admin_role_name'))->first()
        );
    }

    /**
     * @param User $user
     * @param Settings $setting
     * @return bool
     */
    public function update(User $user, Settings $setting)
    {
        $siteTypes = [
            General::class
        ];

        if ($user->hasRole(
            Role::whereName(config('permission.admin_role_name'))->first()
        )) {
            return true;
        }

        if ($user->hasRole(Role::whereName('Менеджер сайта')->first()) && in_array($setting->type, $siteTypes)) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function delete(User $user)
    {
        return $user->hasRole(
            Role::whereName(config('permission.admin_role_name'))->first()
        );
    }
}
