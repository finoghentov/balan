<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Spatie\Permission\Models\Role;

class UserPolicy
{
    use HandlesAuthorization;


    public function viewAny(User $user): bool
    {
        return $user->can('view users');
    }

    /**
     * @param User $user
     * @param User $interactUser
     * @return bool
     */
    public function view(User $user, User $interactUser): bool
    {
        return $user->can('view users');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->can('create users');
    }

    /**
     * @param User $user
     * @param User $interactUser
     * @return bool
     */
    public function update(User $user, User $interactUser): bool
    {
        if ($interactUser->hasRole(Role::whereName(config('permission.admin_role_name'))->first())) {
            return $user->hasRole(Role::whereName(config('permission.admin_role_name'))->first());
        }

        return $user->can('update users');
    }

    /**
     * @param User $user
     * @param User $interactUser
     * @return bool
     */
    public function delete(User $user, User $interactUser): bool
    {
        return $user->can('delete users') && !$interactUser->hasRole(Role::whereName(config('permission.admin_role_name'))->first());
    }
}
