<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Spatie\Permission\Models\Role;

class RolePolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): bool
    {
        return $user->hasRole(Role::whereName(config('permission.admin_role_name'))->first());
    }

    /**
     * @param User $user
     * @param User $interactUser
     * @return bool
     */
    public function view(User $user): bool
    {
        return $user->hasRole(Role::whereName(config('permission.admin_role_name'))->first());
    }
}
