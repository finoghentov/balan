<?php

namespace App\Policies;

use App\Models\User;
use Spatie\Permission\Models\Role;

class ActivityPolicy
{
    public function viewAny(User $user)
    {
        return $user->hasRole(Role::whereName(config('permission.admin_role_name'))->first());
    }

    public function view(User $user)
    {
        return $user->hasRole(Role::whereName(config('permission.admin_role_name'))->first());
    }

    public function create()
    {
        return true;
    }
}
