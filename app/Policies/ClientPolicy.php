<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return $user->can('view clients');
    }

    public function view(User $user)
    {
        return $user->can('view clients');
    }

    public function create(User $user)
    {
        return $user->can('create clients');
    }

    public function update(User $user)
    {
        return $user->can('update clients');
    }

    public function delete(User $user)
    {
        return $user->can('delete clients');
    }
}
