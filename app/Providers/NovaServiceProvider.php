<?php

namespace App\Providers;

use App\Models\User;
use App\Nova\Dashboards\Main;
use App\Nova\Dashboards\Test;
use App\Nova\Menu\MainMenuResolver;
use App\Policies\PermissionPolicy;
use App\Policies\RolePolicy;
use Bolechen\NovaActivitylog\NovaActivitylog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Laravel\Nova\Menu\Menu;
use Laravel\Nova\Menu\MenuCollection;
use Laravel\Nova\Menu\MenuGroup;
use Laravel\Nova\Nova;
use Laravel\Nova\NovaApplicationServiceProvider;
use Laravel\Nova\Util;
use Spatie\Permission\Models\Role;
use Statikbe\NovaTranslationManager\TranslationManager;
use Stepanenko3\LogsTool\LogsTool;
use Stepanenko3\NovaFilemanager\FilemanagerTool;
use Stepanenko3\NovaSettings\NovaSettingsTool;
use Vyuldashev\NovaPermission\NovaPermissionTool;

use function PHPUnit\Framework\callback;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Nova::createUserUsing(null, function ($name, $email, $password) {
            $model = Util::userModel();

            $role = Role::query()->firstOrCreate([
                'name' => config('permission.admin_role_name'),
                'guard_name' => 'web'
            ]);

            /**
             * @var User $user
             */
            $user = tap((new $model())->forceFill([
                'name' => $name,
                'email' => $email,
                'password' => Hash::make($password),
            ]))->save();

            $user->assignRole($role);
        });

        Nova::mainMenu(app(MainMenuResolver::class));
        parent::boot();
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
                ->withAuthenticationRoutes()
                ->withPasswordResetRoutes()
                ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewNova', function ($user) {
            return in_array($user->email, [
                //
            ]);
        });
    }

    /**
     * Get the dashboards that should be listed in the Nova sidebar.
     *
     * @return array
     */
    protected function dashboards()
    {
        Nova::footer(function () {
            return '';
        });

        return [
            new Main
        ];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
        return [
            NovaPermissionTool::make()
                ->rolePolicy(RolePolicy::class)
                ->permissionPolicy(PermissionPolicy::class),
            FilemanagerTool::make()
                ->canSee(fn() => auth()->user()->hasRole(
                    Role::whereName(config('permission.admin_role_name'))->first())
                ),
            LogsTool::make()
                ->canSee(fn() => auth()->user()->hasRole(
                    Role::whereName(config('permission.admin_role_name'))->first())
                ),
            NovaActivitylog::make(),
            TranslationManager::make()->canSee(function () {
                return auth()->user()->hasRole(Role::whereName(config('permission.admin_role_name'))->first());
            })
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
