<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use App\Models\Advantage;
use App\Models\Competence;
use App\Models\ObjectType;
use App\Models\Partner;
use App\Models\Portfolio;
use App\Models\ServiceType;
use App\Policies\ActivityPolicy;
use App\Policies\SettingPolicy;
use App\Policies\SiteManagePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Spatie\Activitylog\Models\Activity;
use Stepanenko3\NovaSettings\Models\Settings;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        Activity::class => ActivityPolicy::class,
        Settings::class => SettingPolicy::class,
        Partner::class => SiteManagePolicy::class,
        Competence::class => SiteManagePolicy::class,
        Advantage::class => SiteManagePolicy::class,
        Portfolio::class => SiteManagePolicy::class,
        ServiceType::class => SiteManagePolicy::class,
        ObjectType::class => SiteManagePolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        //
    }
}
