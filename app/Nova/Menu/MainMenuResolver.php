<?php

namespace App\Nova\Menu;

use Illuminate\Http\Request;
use Laravel\Nova\Menu\Menu;
use Laravel\Nova\Menu\MenuGroup;
use Laravel\Nova\Menu\MenuSection;

class MainMenuResolver
{
    public function __invoke(Request $request, Menu $menu)
    {
        $resourceGroup = $menu->items[1]->items;
        $groups = [];

        foreach ($resourceGroup as $group) {
            if ($group instanceof MenuGroup) {
                $item = new MenuSection($group->name, $group->items);
                $item->collapsable();
            } else {
                $item = $group;
            }

            $groups[] = $item;
        }

        if (!empty($groups)) {
            $menu->items->splice(1, 3, $groups);
        }

        return $menu;
    }
}
