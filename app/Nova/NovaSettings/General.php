<?php

namespace App\Nova\NovaSettings;

use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Panel;
use Laravel\Nova\Fields\Boolean;
use Stepanenko3\NovaSettings\Types\AbstractType;
use Waynestate\Nova\CKEditor4Field\CKEditor;

class General extends AbstractType
{
    public function fields(): array
    {
        return [
            Image::make(__('Logo'), 'logo')->disk('web'),
            CKEditor::make(__('Contact Us Form'), 'contact_us_form_content'),
            Text::make(__('Footer Map'), 'footer_map_iframe'),
            CKEditor::make(__('Footer Contacts'), 'footer_contact_description'),
        ];
    }
}
