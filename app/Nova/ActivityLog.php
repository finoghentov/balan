<?php

namespace App\Nova;

use App\Nova\Filters\FromCreatedAtFilter;
use App\Nova\Filters\ToCreatedAtFilter;
use Laravel\Nova\Fields\Filters\DateFilter;
use Laravel\Nova\Http\Requests\NovaRequest;

class ActivityLog extends \Bolechen\NovaActivitylog\Resources\Activitylog
{
    public static $searchable = false;

    public static function group()
    {
        return __('nova-permission-tool::navigation.sidebar-label');
    }

    public function filters(NovaRequest $request)
    {
        return [
            new FromCreatedAtFilter(),
            new ToCreatedAtFilter()
        ];
    }
}
