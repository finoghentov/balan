-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: crm
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `action_events`
--

DROP TABLE IF EXISTS `action_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `action_events` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `batch_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint unsigned NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `actionable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `actionable_id` bigint unsigned NOT NULL,
  `target_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `target_id` bigint unsigned NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint unsigned DEFAULT NULL,
  `fields` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'running',
  `exception` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `original` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `changes` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `action_events_actionable_type_actionable_id_index` (`actionable_type`,`actionable_id`),
  KEY `action_events_target_type_target_id_index` (`target_type`,`target_id`),
  KEY `action_events_batch_id_model_type_model_id_index` (`batch_id`,`model_type`,`model_id`),
  KEY `action_events_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_events`
--

LOCK TABLES `action_events` WRITE;
/*!40000 ALTER TABLE `action_events` DISABLE KEYS */;
INSERT INTO `action_events` (`id`, `batch_id`, `user_id`, `name`, `actionable_type`, `actionable_id`, `target_type`, `target_id`, `model_type`, `model_id`, `fields`, `status`, `exception`, `created_at`, `updated_at`, `original`, `changes`) VALUES (2,'9875a961-eff4-4ff0-bad4-4e69b13efc1e',1,'Create','App\\Models\\Competence',2,'App\\Models\\Competence',2,'App\\Models\\Competence',2,'','finished','','2023-02-13 18:00:03','2023-02-13 18:00:03',NULL,'{\"title\":\"test1\",\"excerpt\":\"test2\",\"description\":\"<div>test3<\\/div>\",\"updated_at\":\"2023-02-13T18:00:03.000000Z\",\"created_at\":\"2023-02-13T18:00:03.000000Z\",\"id\":2}'),(3,'9875aae6-870d-4b83-9631-208920c165f3',1,'Create','App\\Models\\Competence',1,'App\\Models\\Competence',1,'App\\Models\\Competence',1,'','finished','','2023-02-13 18:04:18','2023-02-13 18:04:18',NULL,'{\"title\":\"1\",\"excerpt\":\"2\",\"description\":\"<div>3<\\/div>\",\"order\":1,\"updated_at\":\"2023-02-13T18:04:18.000000Z\",\"created_at\":\"2023-02-13T18:04:18.000000Z\",\"id\":1}'),(4,'9875aaf2-82df-4050-9f00-8435f5cf27dc',1,'Create','App\\Models\\Competence',2,'App\\Models\\Competence',2,'App\\Models\\Competence',2,'','finished','','2023-02-13 18:04:26','2023-02-13 18:04:26',NULL,'{\"title\":\"4\",\"excerpt\":\"5\",\"description\":\"<div>6<\\/div>\",\"order\":2,\"updated_at\":\"2023-02-13T18:04:26.000000Z\",\"created_at\":\"2023-02-13T18:04:26.000000Z\",\"id\":2}'),(5,'9875abda-aabc-46cd-980d-de2eda1b4a1c',1,'Delete','App\\Models\\Competence',1,'App\\Models\\Competence',1,'App\\Models\\Competence',1,'','finished','','2023-02-13 18:06:58','2023-02-13 18:06:58',NULL,NULL),(6,'9875abdd-5299-41d2-a122-16b290ebd2c0',1,'Delete','App\\Models\\Competence',2,'App\\Models\\Competence',2,'App\\Models\\Competence',2,'','finished','','2023-02-13 18:07:00','2023-02-13 18:07:00',NULL,NULL),(7,'9875ac5f-9d6a-4b6b-9745-33a2a20fd2ef',1,'Create','App\\Models\\Competence',3,'App\\Models\\Competence',3,'App\\Models\\Competence',3,'','finished','','2023-02-13 18:08:25','2023-02-13 18:08:25',NULL,'{\"title\":\"\\u0412\\u0438\\u0434\\u0435\\u043e\\u043d\\u0430\\u0431\\u043b\\u044e\\u0434\\u0435\\u043d\\u0438\\u0435\",\"active\":true,\"excerpt\":\"\\u0423\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u043a\\u0430 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c \\u0432\\u0438\\u0434\\u0435\\u043e\\u043d\\u0430\\u0431\\u043b\\u044e\\u0434\\u0435\\u043d\\u0438\\u044f: \\u043e\\u0442 \\u043f\\u0440\\u043e\\u0441\\u0442\\u0435\\u0439\\u0448\\u0438\\u0445 \\u0434\\u043b\\u044f \\u043d\\u0430\\u0431\\u043b\\u044e\\u0434\\u0435\\u043d\\u0438\\u044f \\u0437\\u0430 \\u0434\\u0430\\u0447\\u043d\\u044b\\u043c \\u0443\\u0447\\u0430\\u0441\\u0442\\u043a\\u043e\\u043c \\u0438 \\u0434\\u043e\\u043c\\u043e\\u043c - \\u0434\\u043e \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c \\u0441 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u043a\\u043e\\u0439 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438 \\u043c\\u043d\\u043e\\u0433\\u043e\\u0444\\u0443\\u043d\\u043a\\u0446\\u0438\\u043e\\u043d\\u0430\\u043b\\u044c\\u043d\\u044b\\u043c \\u041f\\u041e\",\"description\":\"<div>\\u0423\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u043a\\u0430 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c \\u0432\\u0438\\u0434\\u0435\\u043e\\u043d\\u0430\\u0431\\u043b\\u044e\\u0434\\u0435\\u043d\\u0438\\u044f: \\u043e\\u0442 \\u043f\\u0440\\u043e\\u0441\\u0442\\u0435\\u0439\\u0448\\u0438\\u0445 \\u0434\\u043b\\u044f \\u043d\\u0430\\u0431\\u043b\\u044e\\u0434\\u0435\\u043d\\u0438\\u044f \\u0437\\u0430 \\u0434\\u0430\\u0447\\u043d\\u044b\\u043c \\u0443\\u0447\\u0430\\u0441\\u0442\\u043a\\u043e\\u043c \\u0438 \\u0434\\u043e\\u043c\\u043e\\u043c - \\u0434\\u043e \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c \\u0441 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u043a\\u043e\\u0439 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438 \\u043c\\u043d\\u043e\\u0433\\u043e\\u0444\\u0443\\u043d\\u043a\\u0446\\u0438\\u043e\\u043d\\u0430\\u043b\\u044c\\u043d\\u044b\\u043c \\u041f\\u041e<br><br><\\/div>\",\"order\":1,\"updated_at\":\"2023-02-13T18:08:25.000000Z\",\"created_at\":\"2023-02-13T18:08:25.000000Z\",\"id\":3}'),(8,'9875ac91-875d-4f00-9e35-476e74897e83',1,'Create','App\\Models\\Competence',4,'App\\Models\\Competence',4,'App\\Models\\Competence',4,'','finished','','2023-02-13 18:08:58','2023-02-13 18:08:58',NULL,'{\"title\":\"\\u0421\\u041a\\u0423\\u0414\",\"active\":true,\"excerpt\":\"\\u0423\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u043a\\u0430 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044f \\u0438 \\u0443\\u043f\\u0440\\u0430\\u0432\\u043b\\u0435\\u043d\\u0438\\u044f \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u043e\\u043c: \\u043e\\u0442 \\u0443\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u043a\\u0438 \\u0432\\u0438\\u0434\\u0435\\u043e\\u0434\\u043e\\u043c\\u043e\\u0444\\u043e\\u043d\\u0430 \\u0438 \\u0437\\u0430\\u043c\\u043a\\u0430 \\u043d\\u0430 \\u0442\\u0430\\u043c\\u0431\\u0443\\u0440\\u043d\\u0443\\u044e \\u0434\\u0432\\u0435\\u0440\\u044c \\u2013 \\u0434\\u043e \\u043e\\u0440\\u0433\\u0430\\u043d\\u0438\\u0437\\u0430\\u0446\\u0438\\u0438 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b \\u043f\\u0440\\u043e\\u0445\\u043e\\u0434\\u0430 \\u041a\\u041f\\u041f \\u0432 \\u0430\\u044d\\u0440\\u043e\\u043f\\u043e\\u0440\\u0442\\u0435\",\"description\":\"<div>\\u0423\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u043a\\u0430 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044f \\u0438 \\u0443\\u043f\\u0440\\u0430\\u0432\\u043b\\u0435\\u043d\\u0438\\u044f \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u043e\\u043c: \\u043e\\u0442 \\u0443\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u043a\\u0438 \\u0432\\u0438\\u0434\\u0435\\u043e\\u0434\\u043e\\u043c\\u043e\\u0444\\u043e\\u043d\\u0430 \\u0438 \\u0437\\u0430\\u043c\\u043a\\u0430 \\u043d\\u0430 \\u0442\\u0430\\u043c\\u0431\\u0443\\u0440\\u043d\\u0443\\u044e \\u0434\\u0432\\u0435\\u0440\\u044c \\u2013 \\u0434\\u043e \\u043e\\u0440\\u0433\\u0430\\u043d\\u0438\\u0437\\u0430\\u0446\\u0438\\u0438 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b \\u043f\\u0440\\u043e\\u0445\\u043e\\u0434\\u0430 \\u041a\\u041f\\u041f \\u0432 \\u0430\\u044d\\u0440\\u043e\\u043f\\u043e\\u0440\\u0442\\u0435<br><br><\\/div>\",\"order\":2,\"updated_at\":\"2023-02-13T18:08:58.000000Z\",\"created_at\":\"2023-02-13T18:08:58.000000Z\",\"id\":4}'),(9,'9875acaa-62b9-4bcc-95f3-cb38d62022cd',1,'Create','App\\Models\\Competence',5,'App\\Models\\Competence',5,'App\\Models\\Competence',5,'','finished','','2023-02-13 18:09:14','2023-02-13 18:09:14',NULL,'{\"title\":\"\\u0421\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b \\u0430\\u0432\\u0442\\u043e\\u043f\\u0440\\u043e\\u0435\\u0437\\u0434\\u0430\",\"active\":true,\"excerpt\":\"\\u0423\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u043a\\u0430 \\u0430\\u0432\\u0442\\u043e\\u043c\\u0430\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u0440\\u0430\\u0441\\u043f\\u0430\\u0448\\u043d\\u044b\\u0445, \\u043e\\u0442\\u043a\\u0430\\u0442\\u043d\\u044b\\u0445, \\u0441\\u0435\\u043a\\u0446\\u0438\\u043e\\u043d\\u043d\\u044b\\u0445 \\u0432\\u043e\\u0440\\u043e\\u0442. \\u0423\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u043a\\u0430 \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043c\\u0435\\u0445\\u0430\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445, \\u0433\\u0438\\u0434\\u0440\\u0430\\u0432\\u043b\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445, \\u0430\\u043d\\u0442\\u0438\\u0432\\u0430\\u043d\\u0434\\u0430\\u043b\\u044c\\u043d\\u044b\\u0445 \\u0448\\u043b\\u0430\\u0433\\u0431\\u0430\\u0443\\u043c\\u043e\\u0432. \\u0421\\u043f\\u043e\\u0441\\u043e\\u0431\\u044b \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u0430: \\u0441\\u0447\\u0438\\u0442\\u044b\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043d\\u043e\\u043c\\u0435\\u0440\\u043e\\u0432, RFID \\u043c\\u0435\\u0442\\u043a\\u0438, \\u043f\\u0443\\u043b\\u044c\\u0442\\u044b, GSM \\u043c\\u043e\\u0434\\u0443\\u043b\\u044c.\",\"description\":\"<div>\\u0423\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u043a\\u0430 \\u0430\\u0432\\u0442\\u043e\\u043c\\u0430\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u0440\\u0430\\u0441\\u043f\\u0430\\u0448\\u043d\\u044b\\u0445, \\u043e\\u0442\\u043a\\u0430\\u0442\\u043d\\u044b\\u0445, \\u0441\\u0435\\u043a\\u0446\\u0438\\u043e\\u043d\\u043d\\u044b\\u0445 \\u0432\\u043e\\u0440\\u043e\\u0442. \\u0423\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u043a\\u0430 \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043c\\u0435\\u0445\\u0430\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445, \\u0433\\u0438\\u0434\\u0440\\u0430\\u0432\\u043b\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445, \\u0430\\u043d\\u0442\\u0438\\u0432\\u0430\\u043d\\u0434\\u0430\\u043b\\u044c\\u043d\\u044b\\u0445 \\u0448\\u043b\\u0430\\u0433\\u0431\\u0430\\u0443\\u043c\\u043e\\u0432. \\u0421\\u043f\\u043e\\u0441\\u043e\\u0431\\u044b \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u0430: \\u0441\\u0447\\u0438\\u0442\\u044b\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043d\\u043e\\u043c\\u0435\\u0440\\u043e\\u0432, RFID \\u043c\\u0435\\u0442\\u043a\\u0438, \\u043f\\u0443\\u043b\\u044c\\u0442\\u044b, GSM \\u043c\\u043e\\u0434\\u0443\\u043b\\u044c.<br><br><\\/div>\",\"order\":3,\"updated_at\":\"2023-02-13T18:09:14.000000Z\",\"created_at\":\"2023-02-13T18:09:14.000000Z\",\"id\":5}'),(10,'9875af19-305e-4869-ba8c-4aeaeb30b486',1,'Update','App\\Models\\Competence',3,'App\\Models\\Competence',3,'App\\Models\\Competence',3,'','finished','','2023-02-13 18:16:02','2023-02-13 18:16:02','{\"active\":1}','{\"active\":true}'),(11,'98777c42-c541-489d-badd-d0af8f9b432d',1,'Create','App\\Models\\Advantage',1,'App\\Models\\Advantage',1,'App\\Models\\Advantage',1,'','finished','','2023-02-14 15:45:32','2023-02-14 15:45:32',NULL,'{\"title\":\"\\u041c\\u043e\\u043d\\u0442\\u0430\\u0436\\u043d\\u0438\\u043a\\u0438 \\u0432 \\u0448\\u0442\\u0430\\u0442\\u0435\",\"active\":true,\"excerpt\":\"\\u0428\\u0435\\u0441\\u0442\\u044c \\u043a\\u0432\\u0430\\u043b\\u0438\\u0444\\u0438\\u0446\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0445 \\u043c\\u043e\\u043d\\u0442\\u0430\\u0436\\u043d\\u0438\\u043a\\u043e\\u0432 \\u0441 \\u043e\\u043f\\u044b\\u0442\\u043e\\u043c \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b \\u0431\\u043e\\u043b\\u0435\\u0435 10 \\u043b\\u0435\\u0442\",\"icon\":\"pe-tools\",\"updated_at\":\"2023-02-14T15:45:32.000000Z\",\"created_at\":\"2023-02-14T15:45:32.000000Z\",\"id\":1}'),(12,'98777c8c-bb09-404b-9e84-8b1fc44373d4',1,'Create','App\\Models\\Advantage',2,'App\\Models\\Advantage',2,'App\\Models\\Advantage',2,'','finished','','2023-02-14 15:46:21','2023-02-14 15:46:21',NULL,'{\"title\":\"\\u0412\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u043e\\u0432 \\u0432 \\u0441\\u0436\\u0430\\u0442\\u044b\\u0435 \\u0441\\u0440\\u043e\\u043a\\u0438\",\"active\":true,\"excerpt\":\"\\u0412\\u044b\\u0445\\u043e\\u0434 \\u043d\\u0430 \\u043e\\u0431\\u044a\\u0435\\u043a\\u0442 \\u0438 \\u043d\\u0430\\u0447\\u0430\\u043b\\u043e \\u0440\\u0430\\u0431\\u043e\\u0442 \\u043d\\u0430 \\u0441\\u043b\\u0435\\u0434\\u0443\\u044e\\u0449\\u0438\\u0439 \\u0434\\u0435\\u043d\\u044c \\u043f\\u043e\\u043b\\u0435 \\u043f\\u043e\\u0434\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u044f \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440\\u0430. \\u041f\\u0440\\u0438 \\u043d\\u0435\\u043e\\u0431\\u0445\\u043e\\u0434\\u0438\\u043c\\u043e\\u0441\\u0442\\u0438, \\u0432\\u043e\\u0437\\u043c\\u043e\\u0436\\u043d\\u043e \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u0438\\u0435 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b \\u0432 \\u043d\\u043e\\u0447\\u043d\\u043e\\u0435 \\u0432\\u0440\\u0435\\u043c\\u044f\",\"icon\":\"pe-camera\",\"updated_at\":\"2023-02-14T15:46:21.000000Z\",\"created_at\":\"2023-02-14T15:46:21.000000Z\",\"id\":2}'),(13,'98777cb7-c332-4adc-bed7-f8de1a7e38c7',1,'Create','App\\Models\\Advantage',3,'App\\Models\\Advantage',3,'App\\Models\\Advantage',3,'','finished','','2023-02-14 15:46:49','2023-02-14 15:46:49',NULL,'{\"title\":\"\\u0410\\u043a\\u043a\\u0443\\u0440\\u0430\\u0442\\u043d\\u044b\\u0439 \\u043c\\u043e\\u043d\\u0442\\u0430\\u0436\",\"active\":true,\"excerpt\":\"\\u0421\\u0432\\u0435\\u0440\\u043b\\u0435\\u043d\\u0438\\u0435 \\u043e\\u0442\\u0432\\u0435\\u0440\\u0441\\u0442\\u0438\\u0439 \\u0432 \\u043f\\u043e\\u043c\\u0435\\u0449\\u0435\\u043d\\u0438\\u044f\\u0445 \\u0441 \\u0447\\u0438\\u0441\\u0442\\u043e\\u0432\\u043e\\u0439 \\u043e\\u0442\\u0434\\u0435\\u043b\\u043a\\u043e\\u0439 \\u0432\\u0435\\u0434\\u0435\\u0442\\u0441\\u044f \\u0441 \\u043f\\u044b\\u043b\\u0435\\u0441\\u043e\\u0441\\u043e\\u043c. \\u041c\\u043e\\u043d\\u0442\\u0430\\u0436\\u043d\\u0438\\u043a\\u0438 \\u0443\\u0431\\u0435\\u0440\\u0443\\u0442 \\u043f\\u043e\\u0441\\u043b\\u0435 \\u0441\\u0435\\u0431\\u044f \\u043c\\u0443\\u0441\\u043e\\u0440.\",\"icon\":\"pe-photo\",\"updated_at\":\"2023-02-14T15:46:49.000000Z\",\"created_at\":\"2023-02-14T15:46:49.000000Z\",\"id\":3}'),(14,'9877842f-ed46-4d01-865f-1798883e8f02',1,'Create','App\\Models\\Advantage',4,'App\\Models\\Advantage',4,'App\\Models\\Advantage',4,'','finished','','2023-02-14 16:07:42','2023-02-14 16:07:42',NULL,'{\"title\":\"1\",\"active\":true,\"excerpt\":\"1\",\"icon\":\"1\",\"order\":2,\"updated_at\":\"2023-02-14T16:07:42.000000Z\",\"created_at\":\"2023-02-14T16:07:42.000000Z\",\"id\":4}'),(15,'98778f3b-4654-4e19-abab-0f9eb05bf760',1,'Create','App\\Models\\Partner',1,'App\\Models\\Partner',1,'App\\Models\\Partner',1,'','finished','','2023-02-14 16:38:35','2023-02-14 16:38:35',NULL,'{\"title\":\"Rit\",\"active\":true,\"updated_at\":\"2023-02-14T16:38:35.000000Z\",\"created_at\":\"2023-02-14T16:38:35.000000Z\",\"id\":1}'),(16,'98778f44-b195-4580-8cab-dc6153d2cf13',1,'Create','App\\Models\\Partner',2,'App\\Models\\Partner',2,'App\\Models\\Partner',2,'','finished','','2023-02-14 16:38:41','2023-02-14 16:38:41',NULL,'{\"title\":\"Panduit\",\"active\":true,\"updated_at\":\"2023-02-14T16:38:41.000000Z\",\"created_at\":\"2023-02-14T16:38:41.000000Z\",\"id\":2}'),(17,'98778f4d-aba8-4e14-9e60-eaf48740c9f1',1,'Create','App\\Models\\Partner',3,'App\\Models\\Partner',3,'App\\Models\\Partner',3,'','finished','','2023-02-14 16:38:47','2023-02-14 16:38:47',NULL,'{\"title\":\"Legrand\",\"active\":true,\"updated_at\":\"2023-02-14T16:38:47.000000Z\",\"created_at\":\"2023-02-14T16:38:47.000000Z\",\"id\":3}'),(18,'98778f59-5ebf-4db4-8848-1fd85199b36d',1,'Create','App\\Models\\Partner',4,'App\\Models\\Partner',4,'App\\Models\\Partner',4,'','finished','','2023-02-14 16:38:55','2023-02-14 16:38:55',NULL,'{\"title\":\"DKC\",\"active\":true,\"updated_at\":\"2023-02-14T16:38:55.000000Z\",\"created_at\":\"2023-02-14T16:38:55.000000Z\",\"id\":4}'),(19,'98778f95-c8f3-4e33-bfb8-893ee5ddd97d',1,'Create','App\\Models\\Partner',5,'App\\Models\\Partner',5,'App\\Models\\Partner',5,'','finished','','2023-02-14 16:39:34','2023-02-14 16:39:34',NULL,'{\"title\":\"Cabeus\",\"active\":true,\"updated_at\":\"2023-02-14T16:39:34.000000Z\",\"created_at\":\"2023-02-14T16:39:34.000000Z\",\"id\":5}'),(20,'98798805-93b0-4749-9d60-980a6c42f781',1,'Create','App\\Models\\Portfolio',1,'App\\Models\\Portfolio',1,'App\\Models\\Portfolio',1,'','finished','','2023-02-15 16:10:05','2023-02-15 16:10:05',NULL,'{\"title\":\"\\u0414\\u043e\\u043c \\u0432 \\u0434\\u0435\\u0440\\u0435\\u0432\\u043d\\u0435\",\"active\":true,\"description\":\"<div>\\u0420\\u0430\\u0431\\u043e\\u0442\\u0430\\u043b\\u0438 \\u0432 \\u0434\\u043e\\u043c\\u0438\\u043a\\u0435 \\u0432 \\u0434\\u0435\\u0440\\u0435\\u0432\\u043d\\u0435<\\/div>\",\"order\":1,\"updated_at\":\"2023-02-15T16:10:05.000000Z\",\"created_at\":\"2023-02-15T16:10:05.000000Z\",\"id\":1}'),(21,'98798820-7b0a-4327-86ca-10609c1de27f',1,'Create','App\\Models\\Portfolio',2,'App\\Models\\Portfolio',2,'App\\Models\\Portfolio',2,'','finished','','2023-02-15 16:10:22','2023-02-15 16:10:22',NULL,'{\"title\":\"\\u0412\\u0442\\u043e\\u0440\\u043e\\u0439 \\u043e\\u0431\\u044a\\u0435\\u043a\\u0442\",\"active\":true,\"description\":\"<div>\\u0420\\u0430\\u0431\\u043e\\u0442\\u0430\\u043b\\u0438 \\u0432\\u043e \\u0432\\u0442\\u043e\\u0440\\u043e\\u043c \\u043e\\u0431\\u044a\\u0435\\u043a\\u0442\\u0435<\\/div>\",\"order\":2,\"updated_at\":\"2023-02-15T16:10:22.000000Z\",\"created_at\":\"2023-02-15T16:10:22.000000Z\",\"id\":2}'),(22,'98798836-ecbb-4f41-8810-7ee2cc96c54e',1,'Create','App\\Models\\Portfolio',3,'App\\Models\\Portfolio',3,'App\\Models\\Portfolio',3,'','finished','','2023-02-15 16:10:37','2023-02-15 16:10:37',NULL,'{\"title\":\"\\u0422\\u0440\\u0435\\u0442\\u0438 \\u0439 \\u043e\\u0431\\u044a\\u0435\\u043a\\u0442\",\"active\":true,\"description\":\"<div>\\u0420\\u0430\\u0431\\u043e\\u0442\\u0430\\u043b\\u0438 \\u0432 \\u0442\\u0440\\u0435\\u0442\\u044c\\u0435\\u043c \\u043e\\u0431\\u044a\\u0435\\u043a\\u0442\\u0435<\\/div>\",\"order\":3,\"updated_at\":\"2023-02-15T16:10:37.000000Z\",\"created_at\":\"2023-02-15T16:10:37.000000Z\",\"id\":3}'),(23,'987992ae-cae0-405c-94cc-057b3b31455e',1,'Create','Stepanenko3\\NovaSettings\\Models\\Settings',1,'Stepanenko3\\NovaSettings\\Models\\Settings',1,'Stepanenko3\\NovaSettings\\Models\\Settings',1,'','finished','','2023-02-15 16:39:53','2023-02-15 16:39:53',NULL,'{\"slug\":\"site\",\"env\":\"*\",\"type\":\"App\\\\Nova\\\\NovaSettings\\\\General\",\"updated_at\":\"2023-02-15T16:39:53.000000Z\",\"created_at\":\"2023-02-15T16:39:53.000000Z\",\"id\":1}'),(24,'98799431-6683-405b-9afc-ce2e4efc54de',1,'Update','Stepanenko3\\NovaSettings\\Models\\Settings',1,'Stepanenko3\\NovaSettings\\Models\\Settings',1,'Stepanenko3\\NovaSettings\\Models\\Settings',1,'','finished','','2023-02-15 16:44:07','2023-02-15 16:44:07','{\"settings\":null}','{\"settings\":\"{\\\"contact_us_form_content\\\":\\\"<div class=\\\\\\\"textwidget\\\\\\\">\\\\r\\\\n<p>&nbsp;<\\\\\\/p>\\\\r\\\\n\\\\r\\\\n<div class=\\\\\\\"dt-sc-contact-info\\\\\\\">71 Pilgrim Avenue, Chevy Chase,MD 20815<\\\\\\/div>\\\\r\\\\n\\\\r\\\\n<div class=\\\\\\\"dt-sc-contact-info\\\\\\\">+413-2619-789<\\\\\\/div>\\\\r\\\\n\\\\r\\\\n<div class=\\\\\\\"dt-sc-contact-info\\\\\\\">news@cctv.com<\\\\\\/div>\\\\r\\\\n<\\\\\\/div>\\\",\\\"footer_map_iframe\\\":null,\\\"footer_contact_description\\\":\\\"<p>&lt;div class=&quot;textwidget&quot;&gt;<br \\\\\\/>\\\\r\\\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;p&gt;<br \\\\\\/>\\\\r\\\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;\\\\\\/p&gt;<br \\\\\\/>\\\\r\\\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=&quot;dt-sc-contact-info &nbsp;&quot;&gt;&lt;span class=&quot;fa fa-home&quot;&gt; &lt;\\\\\\/span&gt;71 Pilgrim Avenue,<br \\\\\\/>\\\\r\\\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Chevy Chase,MD 20815<br \\\\\\/>\\\\r\\\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;\\\\\\/div&gt;<br \\\\\\/>\\\\r\\\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=&quot;dt-sc-contact-info &nbsp;&quot;&gt;&lt;span class=&quot;fa fa-phone&quot;&gt; &lt;\\\\\\/span&gt;+413-2619-789&lt;\\\\\\/div&gt;<br \\\\\\/>\\\\r\\\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=&quot;dt-sc-contact-info &nbsp;&quot;&gt;&lt;span class=&quot;fa fa-envelope&quot;&gt; &lt;\\\\\\/span&gt;news@cctv.com<br \\\\\\/>\\\\r\\\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;\\\\\\/div&gt;<\\\\\\/p>\\\\r\\\\n\\\\r\\\\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;\\\\\\/div&gt;<\\\\\\/p>\\\"}\"}'),(25,'98799a2f-5116-4fe3-8414-7481d97fd978',1,'Update','Stepanenko3\\NovaSettings\\Models\\Settings',1,'Stepanenko3\\NovaSettings\\Models\\Settings',1,'Stepanenko3\\NovaSettings\\Models\\Settings',1,'','finished','','2023-02-15 17:00:52','2023-02-15 17:00:52','{\"settings\":\"{\\\"footer_map_iframe\\\":null,\\\"contact_us_form_content\\\":\\\"<div class=\\\\\\\"textwidget\\\\\\\">\\\\r\\\\n<p>&nbsp;<\\\\\\/p>\\\\r\\\\n\\\\r\\\\n<div class=\\\\\\\"dt-sc-contact-info\\\\\\\">71 Pilgrim Avenue, Chevy Chase,MD 20815<\\\\\\/div>\\\\r\\\\n\\\\r\\\\n<div class=\\\\\\\"dt-sc-contact-info\\\\\\\">+413-2619-789<\\\\\\/div>\\\\r\\\\n\\\\r\\\\n<div class=\\\\\\\"dt-sc-contact-info\\\\\\\">news@cctv.com<\\\\\\/div>\\\\r\\\\n<\\\\\\/div>\\\",\\\"footer_contact_description\\\":\\\"<p>&lt;div class=&quot;textwidget&quot;&gt;<br \\\\\\/>\\\\r\\\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;p&gt;<br \\\\\\/>\\\\r\\\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;\\\\\\/p&gt;<br \\\\\\/>\\\\r\\\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=&quot;dt-sc-contact-info &nbsp;&quot;&gt;&lt;span class=&quot;fa fa-home&quot;&gt; &lt;\\\\\\/span&gt;71 Pilgrim Avenue,<br \\\\\\/>\\\\r\\\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Chevy Chase,MD 20815<br \\\\\\/>\\\\r\\\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;\\\\\\/div&gt;<br \\\\\\/>\\\\r\\\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=&quot;dt-sc-contact-info &nbsp;&quot;&gt;&lt;span class=&quot;fa fa-phone&quot;&gt; &lt;\\\\\\/span&gt;+413-2619-789&lt;\\\\\\/div&gt;<br \\\\\\/>\\\\r\\\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=&quot;dt-sc-contact-info &nbsp;&quot;&gt;&lt;span class=&quot;fa fa-envelope&quot;&gt; &lt;\\\\\\/span&gt;news@cctv.com<br \\\\\\/>\\\\r\\\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;\\\\\\/div&gt;<\\\\\\/p>\\\\r\\\\n\\\\r\\\\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;\\\\\\/div&gt;<\\\\\\/p>\\\"}\"}','{\"settings\":\"{\\\"footer_map_iframe\\\":\\\"https:\\\\\\/\\\\\\/yandex.ru\\\\\\/map-widget\\\\\\/v1\\\\\\/?ll=37.714054%2C55.796333&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fdata%3DCgk3NzE5NTk4NDUSQ9Cg0L7RgdGB0LjRjywg0JzQvtGB0LrQstCwLCAxLdGPINGD0LvQuNGG0LAg0JHRg9GF0LLQvtGB0YLQvtCy0LAsIDEiCg041hZCFdYwX0I%3D&z=14.52\\\",\\\"contact_us_form_content\\\":\\\"<div class=\\\\\\\"textwidget\\\\\\\">\\\\r\\\\n<ul>\\\\r\\\\n\\\\t<li>\\\\u0422\\\\u0435\\\\u043b\\\\u0435\\\\u0444\\\\u043e\\\\u043d: +7<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0448 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441:&nbsp;107076, \\\\u0433\\\\u043e\\\\u0440\\\\u043e\\\\u0434 \\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, \\\\u0443\\\\u043b 1-\\\\u042f \\\\u0411\\\\u0443\\\\u0445\\\\u0432\\\\u043e\\\\u0441\\\\u0442\\\\u043e\\\\u0432\\\\u0430, \\\\u0434. 12\\\\\\/11 \\\\u043a. 10, \\\\u043f\\\\u043e\\\\u043c\\\\u0435\\\\u0449. 9\\\\\\/18<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0437\\\\u0432\\\\u0430\\\\u043d\\\\u0438\\\\u0435: \\\\u041e\\\\u041e\\\\u041e &quot;\\\\u0411\\\\u0430\\\\u043b\\\\u0430\\\\u043d&quot;<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u0418\\\\u041d\\\\u041d:&nbsp;9718218176<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u042e\\\\u0440\\\\u0438\\\\u0434\\\\u0438\\\\u0447\\\\u0435\\\\u0441\\\\u043a\\\\u0438\\\\u0439 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441 \\\\u0431\\\\u0430\\\\u043d\\\\u043a\\\\u0430:&nbsp;\\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, 127287, \\\\u0443\\\\u043b. 2-\\\\u044f \\\\u0425\\\\u0443\\\\u0442\\\\u043e\\\\u0440\\\\u0441\\\\u043a\\\\u0430\\\\u044f, \\\\u0434. 38\\\\u0410, \\\\u0441\\\\u0442\\\\u0440. 26<\\\\\\/li>\\\\r\\\\n<\\\\\\/ul>\\\\r\\\\n<\\\\\\/div>\\\",\\\"footer_contact_description\\\":\\\"<ul>\\\\r\\\\n\\\\t<li>\\\\u0422\\\\u0435\\\\u043b\\\\u0435\\\\u0444\\\\u043e\\\\u043d: +7<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0448 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441:&nbsp;107076, \\\\u0433\\\\u043e\\\\u0440\\\\u043e\\\\u0434 \\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, \\\\u0443\\\\u043b 1-\\\\u042f \\\\u0411\\\\u0443\\\\u0445\\\\u0432\\\\u043e\\\\u0441\\\\u0442\\\\u043e\\\\u0432\\\\u0430, \\\\u0434. 12\\\\\\/11 \\\\u043a. 10, \\\\u043f\\\\u043e\\\\u043c\\\\u0435\\\\u0449. 9\\\\\\/18<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0437\\\\u0432\\\\u0430\\\\u043d\\\\u0438\\\\u0435: \\\\u041e\\\\u041e\\\\u041e &quot;\\\\u0411\\\\u0430\\\\u043b\\\\u0430\\\\u043d&quot;<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u0418\\\\u041d\\\\u041d:&nbsp;9718218176<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u042e\\\\u0440\\\\u0438\\\\u0434\\\\u0438\\\\u0447\\\\u0435\\\\u0441\\\\u043a\\\\u0438\\\\u0439 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441 \\\\u0431\\\\u0430\\\\u043d\\\\u043a\\\\u0430:&nbsp;\\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, 127287, \\\\u0443\\\\u043b. 2-\\\\u044f \\\\u0425\\\\u0443\\\\u0442\\\\u043e\\\\u0440\\\\u0441\\\\u043a\\\\u0430\\\\u044f, \\\\u0434. 38\\\\u0410, \\\\u0441\\\\u0442\\\\u0440. 26<\\\\\\/li>\\\\r\\\\n<\\\\\\/ul>\\\"}\"}'),(26,'9879a06a-c62e-4da2-930f-74ae2dbfa107',1,'Update','Stepanenko3\\NovaSettings\\Models\\Settings',1,'Stepanenko3\\NovaSettings\\Models\\Settings',1,'Stepanenko3\\NovaSettings\\Models\\Settings',1,'','finished','','2023-02-15 17:18:18','2023-02-15 17:18:18','{\"settings\":\"{\\\"footer_map_iframe\\\":\\\"https:\\\\\\/\\\\\\/yandex.ru\\\\\\/map-widget\\\\\\/v1\\\\\\/?ll=37.714054%2C55.796333&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fdata%3DCgk3NzE5NTk4NDUSQ9Cg0L7RgdGB0LjRjywg0JzQvtGB0LrQstCwLCAxLdGPINGD0LvQuNGG0LAg0JHRg9GF0LLQvtGB0YLQvtCy0LAsIDEiCg041hZCFdYwX0I%3D&z=14.52\\\",\\\"contact_us_form_content\\\":\\\"<div class=\\\\\\\"textwidget\\\\\\\">\\\\r\\\\n<ul>\\\\r\\\\n\\\\t<li>\\\\u0422\\\\u0435\\\\u043b\\\\u0435\\\\u0444\\\\u043e\\\\u043d: +7<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0448 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441:&nbsp;107076, \\\\u0433\\\\u043e\\\\u0440\\\\u043e\\\\u0434 \\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, \\\\u0443\\\\u043b 1-\\\\u042f \\\\u0411\\\\u0443\\\\u0445\\\\u0432\\\\u043e\\\\u0441\\\\u0442\\\\u043e\\\\u0432\\\\u0430, \\\\u0434. 12\\\\\\/11 \\\\u043a. 10, \\\\u043f\\\\u043e\\\\u043c\\\\u0435\\\\u0449. 9\\\\\\/18<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0437\\\\u0432\\\\u0430\\\\u043d\\\\u0438\\\\u0435: \\\\u041e\\\\u041e\\\\u041e &quot;\\\\u0411\\\\u0430\\\\u043b\\\\u0430\\\\u043d&quot;<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u0418\\\\u041d\\\\u041d:&nbsp;9718218176<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u042e\\\\u0440\\\\u0438\\\\u0434\\\\u0438\\\\u0447\\\\u0435\\\\u0441\\\\u043a\\\\u0438\\\\u0439 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441 \\\\u0431\\\\u0430\\\\u043d\\\\u043a\\\\u0430:&nbsp;\\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, 127287, \\\\u0443\\\\u043b. 2-\\\\u044f \\\\u0425\\\\u0443\\\\u0442\\\\u043e\\\\u0440\\\\u0441\\\\u043a\\\\u0430\\\\u044f, \\\\u0434. 38\\\\u0410, \\\\u0441\\\\u0442\\\\u0440. 26<\\\\\\/li>\\\\r\\\\n<\\\\\\/ul>\\\\r\\\\n<\\\\\\/div>\\\",\\\"footer_contact_description\\\":\\\"<ul>\\\\r\\\\n\\\\t<li>\\\\u0422\\\\u0435\\\\u043b\\\\u0435\\\\u0444\\\\u043e\\\\u043d: +7<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0448 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441:&nbsp;107076, \\\\u0433\\\\u043e\\\\u0440\\\\u043e\\\\u0434 \\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, \\\\u0443\\\\u043b 1-\\\\u042f \\\\u0411\\\\u0443\\\\u0445\\\\u0432\\\\u043e\\\\u0441\\\\u0442\\\\u043e\\\\u0432\\\\u0430, \\\\u0434. 12\\\\\\/11 \\\\u043a. 10, \\\\u043f\\\\u043e\\\\u043c\\\\u0435\\\\u0449. 9\\\\\\/18<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0437\\\\u0432\\\\u0430\\\\u043d\\\\u0438\\\\u0435: \\\\u041e\\\\u041e\\\\u041e &quot;\\\\u0411\\\\u0430\\\\u043b\\\\u0430\\\\u043d&quot;<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u0418\\\\u041d\\\\u041d:&nbsp;9718218176<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u042e\\\\u0440\\\\u0438\\\\u0434\\\\u0438\\\\u0447\\\\u0435\\\\u0441\\\\u043a\\\\u0438\\\\u0439 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441 \\\\u0431\\\\u0430\\\\u043d\\\\u043a\\\\u0430:&nbsp;\\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, 127287, \\\\u0443\\\\u043b. 2-\\\\u044f \\\\u0425\\\\u0443\\\\u0442\\\\u043e\\\\u0440\\\\u0441\\\\u043a\\\\u0430\\\\u044f, \\\\u0434. 38\\\\u0410, \\\\u0441\\\\u0442\\\\u0440. 26<\\\\\\/li>\\\\r\\\\n<\\\\\\/ul>\\\"}\"}','{\"settings\":\"{\\\"footer_map_iframe\\\":\\\"https:\\\\\\/\\\\\\/yandex.ru\\\\\\/map-widget\\\\\\/v1\\\\\\/?ll=37.714054%2C55.796333&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fdata%3DCgk3NzE5NTk4NDUSQ9Cg0L7RgdGB0LjRjywg0JzQvtGB0LrQstCwLCAxLdGPINGD0LvQuNGG0LAg0JHRg9GF0LLQvtGB0YLQvtCy0LAsIDEiCg041hZCFdYwX0I%3D&z=14.52\\\",\\\"contact_us_form_content\\\":\\\"<div class=\\\\\\\"textwidget\\\\\\\">\\\\r\\\\n<ul>\\\\r\\\\n\\\\t<li>\\\\u0422\\\\u0435\\\\u043b\\\\u0435\\\\u0444\\\\u043e\\\\u043d: +7<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0448 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441:&nbsp;107076, \\\\u0433\\\\u043e\\\\u0440\\\\u043e\\\\u0434 \\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, \\\\u0443\\\\u043b 1-\\\\u042f \\\\u0411\\\\u0443\\\\u0445\\\\u0432\\\\u043e\\\\u0441\\\\u0442\\\\u043e\\\\u0432\\\\u0430, \\\\u0434. 12\\\\\\/11 \\\\u043a. 10, \\\\u043f\\\\u043e\\\\u043c\\\\u0435\\\\u0449. 9\\\\\\/18<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0437\\\\u0432\\\\u0430\\\\u043d\\\\u0438\\\\u0435: \\\\u041e\\\\u041e\\\\u041e &quot;\\\\u0411\\\\u0430\\\\u043b\\\\u0430\\\\u043d&quot;<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u0418\\\\u041d\\\\u041d:&nbsp;9718218176<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u042e\\\\u0440\\\\u0438\\\\u0434\\\\u0438\\\\u0447\\\\u0435\\\\u0441\\\\u043a\\\\u0438\\\\u0439 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441 \\\\u0431\\\\u0430\\\\u043d\\\\u043a\\\\u0430:&nbsp;\\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, 127287, \\\\u0443\\\\u043b. 2-\\\\u044f \\\\u0425\\\\u0443\\\\u0442\\\\u043e\\\\u0440\\\\u0441\\\\u043a\\\\u0430\\\\u044f, \\\\u0434. 38\\\\u0410, \\\\u0441\\\\u0442\\\\u0440. 26<\\\\\\/li>\\\\r\\\\n<\\\\\\/ul>\\\\r\\\\n<\\\\\\/div>\\\",\\\"footer_contact_description\\\":\\\"<ul>\\\\r\\\\n\\\\t<li>\\\\u0422\\\\u0435\\\\u043b\\\\u0435\\\\u0444\\\\u043e\\\\u043d: +7<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0448 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441:&nbsp;107076, \\\\u0433\\\\u043e\\\\u0440\\\\u043e\\\\u0434 \\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, \\\\u0443\\\\u043b 1-\\\\u042f \\\\u0411\\\\u0443\\\\u0445\\\\u0432\\\\u043e\\\\u0441\\\\u0442\\\\u043e\\\\u0432\\\\u0430, \\\\u0434. 12\\\\\\/11 \\\\u043a. 10, \\\\u043f\\\\u043e\\\\u043c\\\\u0435\\\\u0449. 9\\\\\\/18<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0437\\\\u0432\\\\u0430\\\\u043d\\\\u0438\\\\u0435: \\\\u041e\\\\u041e\\\\u041e &quot;\\\\u0411\\\\u0430\\\\u043b\\\\u0430\\\\u043d&quot;<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u0418\\\\u041d\\\\u041d:&nbsp;9718218176<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u042e\\\\u0440\\\\u0438\\\\u0434\\\\u0438\\\\u0447\\\\u0435\\\\u0441\\\\u043a\\\\u0438\\\\u0439 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441 \\\\u0431\\\\u0430\\\\u043d\\\\u043a\\\\u0430:&nbsp;\\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, 127287, \\\\u0443\\\\u043b. 2-\\\\u044f \\\\u0425\\\\u0443\\\\u0442\\\\u043e\\\\u0440\\\\u0441\\\\u043a\\\\u0430\\\\u044f, \\\\u0434. 38\\\\u0410, \\\\u0441\\\\u0442\\\\u0440. 26<\\\\\\/li>\\\\r\\\\n<\\\\\\/ul>\\\",\\\"logo\\\":\\\"2v3RazqEVjNqaa24S36Y9jtzRQb9lYEypjMbcuuF.png\\\"}\"}'),(27,'9879a0f8-a49c-49d7-af6d-99e60219df5a',1,'Update','Stepanenko3\\NovaSettings\\Models\\Settings',1,'Stepanenko3\\NovaSettings\\Models\\Settings',1,'Stepanenko3\\NovaSettings\\Models\\Settings',1,'','finished','','2023-02-15 17:19:51','2023-02-15 17:19:51','[]','[]'),(28,'9879a101-8e56-46d1-8e02-599aa2cb185d',1,'Update','Stepanenko3\\NovaSettings\\Models\\Settings',1,'Stepanenko3\\NovaSettings\\Models\\Settings',1,'Stepanenko3\\NovaSettings\\Models\\Settings',1,'','finished','','2023-02-15 17:19:56','2023-02-15 17:19:56','{\"settings\":\"{\\\"logo\\\":null,\\\"footer_map_iframe\\\":\\\"https:\\\\\\/\\\\\\/yandex.ru\\\\\\/map-widget\\\\\\/v1\\\\\\/?ll=37.714054%2C55.796333&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fdata%3DCgk3NzE5NTk4NDUSQ9Cg0L7RgdGB0LjRjywg0JzQvtGB0LrQstCwLCAxLdGPINGD0LvQuNGG0LAg0JHRg9GF0LLQvtGB0YLQvtCy0LAsIDEiCg041hZCFdYwX0I%3D&z=14.52\\\",\\\"contact_us_form_content\\\":\\\"<div class=\\\\\\\"textwidget\\\\\\\">\\\\r\\\\n<ul>\\\\r\\\\n\\\\t<li>\\\\u0422\\\\u0435\\\\u043b\\\\u0435\\\\u0444\\\\u043e\\\\u043d: +7<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0448 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441:&nbsp;107076, \\\\u0433\\\\u043e\\\\u0440\\\\u043e\\\\u0434 \\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, \\\\u0443\\\\u043b 1-\\\\u042f \\\\u0411\\\\u0443\\\\u0445\\\\u0432\\\\u043e\\\\u0441\\\\u0442\\\\u043e\\\\u0432\\\\u0430, \\\\u0434. 12\\\\\\/11 \\\\u043a. 10, \\\\u043f\\\\u043e\\\\u043c\\\\u0435\\\\u0449. 9\\\\\\/18<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0437\\\\u0432\\\\u0430\\\\u043d\\\\u0438\\\\u0435: \\\\u041e\\\\u041e\\\\u041e &quot;\\\\u0411\\\\u0430\\\\u043b\\\\u0430\\\\u043d&quot;<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u0418\\\\u041d\\\\u041d:&nbsp;9718218176<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u042e\\\\u0440\\\\u0438\\\\u0434\\\\u0438\\\\u0447\\\\u0435\\\\u0441\\\\u043a\\\\u0438\\\\u0439 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441 \\\\u0431\\\\u0430\\\\u043d\\\\u043a\\\\u0430:&nbsp;\\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, 127287, \\\\u0443\\\\u043b. 2-\\\\u044f \\\\u0425\\\\u0443\\\\u0442\\\\u043e\\\\u0440\\\\u0441\\\\u043a\\\\u0430\\\\u044f, \\\\u0434. 38\\\\u0410, \\\\u0441\\\\u0442\\\\u0440. 26<\\\\\\/li>\\\\r\\\\n<\\\\\\/ul>\\\\r\\\\n<\\\\\\/div>\\\",\\\"footer_contact_description\\\":\\\"<ul>\\\\r\\\\n\\\\t<li>\\\\u0422\\\\u0435\\\\u043b\\\\u0435\\\\u0444\\\\u043e\\\\u043d: +7<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0448 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441:&nbsp;107076, \\\\u0433\\\\u043e\\\\u0440\\\\u043e\\\\u0434 \\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, \\\\u0443\\\\u043b 1-\\\\u042f \\\\u0411\\\\u0443\\\\u0445\\\\u0432\\\\u043e\\\\u0441\\\\u0442\\\\u043e\\\\u0432\\\\u0430, \\\\u0434. 12\\\\\\/11 \\\\u043a. 10, \\\\u043f\\\\u043e\\\\u043c\\\\u0435\\\\u0449. 9\\\\\\/18<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0437\\\\u0432\\\\u0430\\\\u043d\\\\u0438\\\\u0435: \\\\u041e\\\\u041e\\\\u041e &quot;\\\\u0411\\\\u0430\\\\u043b\\\\u0430\\\\u043d&quot;<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u0418\\\\u041d\\\\u041d:&nbsp;9718218176<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u042e\\\\u0440\\\\u0438\\\\u0434\\\\u0438\\\\u0447\\\\u0435\\\\u0441\\\\u043a\\\\u0438\\\\u0439 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441 \\\\u0431\\\\u0430\\\\u043d\\\\u043a\\\\u0430:&nbsp;\\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, 127287, \\\\u0443\\\\u043b. 2-\\\\u044f \\\\u0425\\\\u0443\\\\u0442\\\\u043e\\\\u0440\\\\u0441\\\\u043a\\\\u0430\\\\u044f, \\\\u0434. 38\\\\u0410, \\\\u0441\\\\u0442\\\\u0440. 26<\\\\\\/li>\\\\r\\\\n<\\\\\\/ul>\\\"}\"}','{\"settings\":\"{\\\"logo\\\":\\\"PWZGg39CBthsABzcIKH66M8jLm4edatYMQL5dtMs.png\\\",\\\"footer_map_iframe\\\":\\\"https:\\\\\\/\\\\\\/yandex.ru\\\\\\/map-widget\\\\\\/v1\\\\\\/?ll=37.714054%2C55.796333&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fdata%3DCgk3NzE5NTk4NDUSQ9Cg0L7RgdGB0LjRjywg0JzQvtGB0LrQstCwLCAxLdGPINGD0LvQuNGG0LAg0JHRg9GF0LLQvtGB0YLQvtCy0LAsIDEiCg041hZCFdYwX0I%3D&z=14.52\\\",\\\"contact_us_form_content\\\":\\\"<div class=\\\\\\\"textwidget\\\\\\\">\\\\r\\\\n<ul>\\\\r\\\\n\\\\t<li>\\\\u0422\\\\u0435\\\\u043b\\\\u0435\\\\u0444\\\\u043e\\\\u043d: +7<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0448 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441:&nbsp;107076, \\\\u0433\\\\u043e\\\\u0440\\\\u043e\\\\u0434 \\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, \\\\u0443\\\\u043b 1-\\\\u042f \\\\u0411\\\\u0443\\\\u0445\\\\u0432\\\\u043e\\\\u0441\\\\u0442\\\\u043e\\\\u0432\\\\u0430, \\\\u0434. 12\\\\\\/11 \\\\u043a. 10, \\\\u043f\\\\u043e\\\\u043c\\\\u0435\\\\u0449. 9\\\\\\/18<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0437\\\\u0432\\\\u0430\\\\u043d\\\\u0438\\\\u0435: \\\\u041e\\\\u041e\\\\u041e &quot;\\\\u0411\\\\u0430\\\\u043b\\\\u0430\\\\u043d&quot;<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u0418\\\\u041d\\\\u041d:&nbsp;9718218176<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u042e\\\\u0440\\\\u0438\\\\u0434\\\\u0438\\\\u0447\\\\u0435\\\\u0441\\\\u043a\\\\u0438\\\\u0439 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441 \\\\u0431\\\\u0430\\\\u043d\\\\u043a\\\\u0430:&nbsp;\\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, 127287, \\\\u0443\\\\u043b. 2-\\\\u044f \\\\u0425\\\\u0443\\\\u0442\\\\u043e\\\\u0440\\\\u0441\\\\u043a\\\\u0430\\\\u044f, \\\\u0434. 38\\\\u0410, \\\\u0441\\\\u0442\\\\u0440. 26<\\\\\\/li>\\\\r\\\\n<\\\\\\/ul>\\\\r\\\\n<\\\\\\/div>\\\",\\\"footer_contact_description\\\":\\\"<ul>\\\\r\\\\n\\\\t<li>\\\\u0422\\\\u0435\\\\u043b\\\\u0435\\\\u0444\\\\u043e\\\\u043d: +7<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0448 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441:&nbsp;107076, \\\\u0433\\\\u043e\\\\u0440\\\\u043e\\\\u0434 \\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, \\\\u0443\\\\u043b 1-\\\\u042f \\\\u0411\\\\u0443\\\\u0445\\\\u0432\\\\u043e\\\\u0441\\\\u0442\\\\u043e\\\\u0432\\\\u0430, \\\\u0434. 12\\\\\\/11 \\\\u043a. 10, \\\\u043f\\\\u043e\\\\u043c\\\\u0435\\\\u0449. 9\\\\\\/18<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u041d\\\\u0430\\\\u0437\\\\u0432\\\\u0430\\\\u043d\\\\u0438\\\\u0435: \\\\u041e\\\\u041e\\\\u041e &quot;\\\\u0411\\\\u0430\\\\u043b\\\\u0430\\\\u043d&quot;<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u0418\\\\u041d\\\\u041d:&nbsp;9718218176<\\\\\\/li>\\\\r\\\\n\\\\t<li>\\\\u042e\\\\u0440\\\\u0438\\\\u0434\\\\u0438\\\\u0447\\\\u0435\\\\u0441\\\\u043a\\\\u0438\\\\u0439 \\\\u0430\\\\u0434\\\\u0440\\\\u0435\\\\u0441 \\\\u0431\\\\u0430\\\\u043d\\\\u043a\\\\u0430:&nbsp;\\\\u041c\\\\u043e\\\\u0441\\\\u043a\\\\u0432\\\\u0430, 127287, \\\\u0443\\\\u043b. 2-\\\\u044f \\\\u0425\\\\u0443\\\\u0442\\\\u043e\\\\u0440\\\\u0441\\\\u043a\\\\u0430\\\\u044f, \\\\u0434. 38\\\\u0410, \\\\u0441\\\\u0442\\\\u0440. 26<\\\\\\/li>\\\\r\\\\n<\\\\\\/ul>\\\"}\"}'),(29,'9879b7ae-e36d-414e-a2e9-a036a19c9a0d',1,'Create','App\\Models\\ServiceType',1,'App\\Models\\ServiceType',1,'App\\Models\\ServiceType',1,'','finished','','2023-02-15 18:23:21','2023-02-15 18:23:21',NULL,'{\"title\":\"\\u0423\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u043a\\u0430 \\u0432\\u0438\\u0434\\u0435\\u043e\",\"active\":true,\"order\":1,\"updated_at\":\"2023-02-15T18:23:21.000000Z\",\"created_at\":\"2023-02-15T18:23:21.000000Z\",\"id\":1}'),(30,'9879b7c1-4c40-4fb3-8181-3aad1f40c7d4',1,'Create','App\\Models\\ObjectType',1,'App\\Models\\ObjectType',1,'App\\Models\\ObjectType',1,'','finished','','2023-02-15 18:23:33','2023-02-15 18:23:33',NULL,'{\"title\":\"\\u0423\\u043d\\u0438\\u0442\\u0430\\u0437\",\"active\":true,\"order\":1,\"updated_at\":\"2023-02-15T18:23:33.000000Z\",\"created_at\":\"2023-02-15T18:23:33.000000Z\",\"id\":1}');
/*!40000 ALTER TABLE `action_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `activity_log` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `log_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_id` bigint unsigned DEFAULT NULL,
  `causer_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint unsigned DEFAULT NULL,
  `properties` json DEFAULT NULL,
  `batch_uuid` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subject` (`subject_type`,`subject_id`),
  KEY `causer` (`causer_type`,`causer_id`),
  KEY `activity_log_log_name_index` (`log_name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_log`
--

LOCK TABLES `activity_log` WRITE;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_type`, `event`, `subject_id`, `causer_type`, `causer_id`, `properties`, `batch_uuid`, `created_at`, `updated_at`) VALUES (1,'default','created','App\\Models\\User','created',1,NULL,NULL,'{\"ip\": \"127.0.0.1\", \"attributes\": {\"name\": \"Admin\", \"email\": \"admin@gmail.com\", \"password\": \"$2y$10$YkVVHTEwbuRoep1R6xrxIeeGGRuo/lbuzxM7DmRZyb8Oj5I7cIYF.\"}}',NULL,'2023-02-13 17:10:26','2023-02-13 17:10:26'),(2,'default','updated','App\\Models\\User','updated',1,'App\\Models\\User',1,'{\"ip\": \"192.168.48.1\", \"old\": [], \"attributes\": []}',NULL,'2023-02-15 16:07:07','2023-02-15 16:07:07'),(3,'default','created','Stepanenko3\\NovaSettings\\Models\\Settings','created',1,'App\\Models\\User',1,'{\"ip\": \"192.168.48.1\", \"attributes\": {\"env\": \"*\", \"slug\": \"site\", \"type\": \"App\\\\Nova\\\\NovaSettings\\\\General\", \"fields\": null, \"settings\": null}}',NULL,'2023-02-15 16:39:53','2023-02-15 16:39:53'),(4,'default','updated','Stepanenko3\\NovaSettings\\Models\\Settings','updated',1,'App\\Models\\User',1,'{\"ip\": \"192.168.48.1\", \"old\": {\"settings\": null}, \"attributes\": {\"settings\": {\"footer_map_iframe\": null, \"contact_us_form_content\": \"<div class=\\\"textwidget\\\">\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<div class=\\\"dt-sc-contact-info\\\">71 Pilgrim Avenue, Chevy Chase,MD 20815</div>\\r\\n\\r\\n<div class=\\\"dt-sc-contact-info\\\">+413-2619-789</div>\\r\\n\\r\\n<div class=\\\"dt-sc-contact-info\\\">news@cctv.com</div>\\r\\n</div>\", \"footer_contact_description\": \"<p>&lt;div class=&quot;textwidget&quot;&gt;<br />\\r\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;p&gt;<br />\\r\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/p&gt;<br />\\r\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=&quot;dt-sc-contact-info &nbsp;&quot;&gt;&lt;span class=&quot;fa fa-home&quot;&gt; &lt;/span&gt;71 Pilgrim Avenue,<br />\\r\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Chevy Chase,MD 20815<br />\\r\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt;<br />\\r\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=&quot;dt-sc-contact-info &nbsp;&quot;&gt;&lt;span class=&quot;fa fa-phone&quot;&gt; &lt;/span&gt;+413-2619-789&lt;/div&gt;<br />\\r\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=&quot;dt-sc-contact-info &nbsp;&quot;&gt;&lt;span class=&quot;fa fa-envelope&quot;&gt; &lt;/span&gt;news@cctv.com<br />\\r\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt;</p>\\r\\n\\r\\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt;</p>\"}}}',NULL,'2023-02-15 16:44:07','2023-02-15 16:44:07'),(5,'default','updated','Stepanenko3\\NovaSettings\\Models\\Settings','updated',1,'App\\Models\\User',1,'{\"ip\": \"192.168.48.1\", \"old\": {\"settings\": {\"footer_map_iframe\": null, \"contact_us_form_content\": \"<div class=\\\"textwidget\\\">\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<div class=\\\"dt-sc-contact-info\\\">71 Pilgrim Avenue, Chevy Chase,MD 20815</div>\\r\\n\\r\\n<div class=\\\"dt-sc-contact-info\\\">+413-2619-789</div>\\r\\n\\r\\n<div class=\\\"dt-sc-contact-info\\\">news@cctv.com</div>\\r\\n</div>\", \"footer_contact_description\": \"<p>&lt;div class=&quot;textwidget&quot;&gt;<br />\\r\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;p&gt;<br />\\r\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/p&gt;<br />\\r\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=&quot;dt-sc-contact-info &nbsp;&quot;&gt;&lt;span class=&quot;fa fa-home&quot;&gt; &lt;/span&gt;71 Pilgrim Avenue,<br />\\r\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Chevy Chase,MD 20815<br />\\r\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt;<br />\\r\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=&quot;dt-sc-contact-info &nbsp;&quot;&gt;&lt;span class=&quot;fa fa-phone&quot;&gt; &lt;/span&gt;+413-2619-789&lt;/div&gt;<br />\\r\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=&quot;dt-sc-contact-info &nbsp;&quot;&gt;&lt;span class=&quot;fa fa-envelope&quot;&gt; &lt;/span&gt;news@cctv.com<br />\\r\\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt;</p>\\r\\n\\r\\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt;</p>\"}}, \"attributes\": {\"settings\": {\"footer_map_iframe\": \"https://yandex.ru/map-widget/v1/?ll=37.714054%2C55.796333&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fdata%3DCgk3NzE5NTk4NDUSQ9Cg0L7RgdGB0LjRjywg0JzQvtGB0LrQstCwLCAxLdGPINGD0LvQuNGG0LAg0JHRg9GF0LLQvtGB0YLQvtCy0LAsIDEiCg041hZCFdYwX0I%3D&z=14.52\", \"contact_us_form_content\": \"<div class=\\\"textwidget\\\">\\r\\n<ul>\\r\\n\\t<li>Телефон: +7</li>\\r\\n\\t<li>Наш адрес:&nbsp;107076, город Москва, ул 1-Я Бухвостова, д. 12/11 к. 10, помещ. 9/18</li>\\r\\n\\t<li>Название: ООО &quot;Балан&quot;</li>\\r\\n\\t<li>ИНН:&nbsp;9718218176</li>\\r\\n\\t<li>Юридический адрес банка:&nbsp;Москва, 127287, ул. 2-я Хуторская, д. 38А, стр. 26</li>\\r\\n</ul>\\r\\n</div>\", \"footer_contact_description\": \"<ul>\\r\\n\\t<li>Телефон: +7</li>\\r\\n\\t<li>Наш адрес:&nbsp;107076, город Москва, ул 1-Я Бухвостова, д. 12/11 к. 10, помещ. 9/18</li>\\r\\n\\t<li>Название: ООО &quot;Балан&quot;</li>\\r\\n\\t<li>ИНН:&nbsp;9718218176</li>\\r\\n\\t<li>Юридический адрес банка:&nbsp;Москва, 127287, ул. 2-я Хуторская, д. 38А, стр. 26</li>\\r\\n</ul>\"}}}',NULL,'2023-02-15 17:00:52','2023-02-15 17:00:52'),(6,'default','updated','Stepanenko3\\NovaSettings\\Models\\Settings','updated',1,'App\\Models\\User',1,'{\"ip\": \"192.168.48.1\", \"old\": {\"settings\": {\"footer_map_iframe\": \"https://yandex.ru/map-widget/v1/?ll=37.714054%2C55.796333&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fdata%3DCgk3NzE5NTk4NDUSQ9Cg0L7RgdGB0LjRjywg0JzQvtGB0LrQstCwLCAxLdGPINGD0LvQuNGG0LAg0JHRg9GF0LLQvtGB0YLQvtCy0LAsIDEiCg041hZCFdYwX0I%3D&z=14.52\", \"contact_us_form_content\": \"<div class=\\\"textwidget\\\">\\r\\n<ul>\\r\\n\\t<li>Телефон: +7</li>\\r\\n\\t<li>Наш адрес:&nbsp;107076, город Москва, ул 1-Я Бухвостова, д. 12/11 к. 10, помещ. 9/18</li>\\r\\n\\t<li>Название: ООО &quot;Балан&quot;</li>\\r\\n\\t<li>ИНН:&nbsp;9718218176</li>\\r\\n\\t<li>Юридический адрес банка:&nbsp;Москва, 127287, ул. 2-я Хуторская, д. 38А, стр. 26</li>\\r\\n</ul>\\r\\n</div>\", \"footer_contact_description\": \"<ul>\\r\\n\\t<li>Телефон: +7</li>\\r\\n\\t<li>Наш адрес:&nbsp;107076, город Москва, ул 1-Я Бухвостова, д. 12/11 к. 10, помещ. 9/18</li>\\r\\n\\t<li>Название: ООО &quot;Балан&quot;</li>\\r\\n\\t<li>ИНН:&nbsp;9718218176</li>\\r\\n\\t<li>Юридический адрес банка:&nbsp;Москва, 127287, ул. 2-я Хуторская, д. 38А, стр. 26</li>\\r\\n</ul>\"}}, \"attributes\": {\"settings\": {\"logo\": \"2v3RazqEVjNqaa24S36Y9jtzRQb9lYEypjMbcuuF.png\", \"footer_map_iframe\": \"https://yandex.ru/map-widget/v1/?ll=37.714054%2C55.796333&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fdata%3DCgk3NzE5NTk4NDUSQ9Cg0L7RgdGB0LjRjywg0JzQvtGB0LrQstCwLCAxLdGPINGD0LvQuNGG0LAg0JHRg9GF0LLQvtGB0YLQvtCy0LAsIDEiCg041hZCFdYwX0I%3D&z=14.52\", \"contact_us_form_content\": \"<div class=\\\"textwidget\\\">\\r\\n<ul>\\r\\n\\t<li>Телефон: +7</li>\\r\\n\\t<li>Наш адрес:&nbsp;107076, город Москва, ул 1-Я Бухвостова, д. 12/11 к. 10, помещ. 9/18</li>\\r\\n\\t<li>Название: ООО &quot;Балан&quot;</li>\\r\\n\\t<li>ИНН:&nbsp;9718218176</li>\\r\\n\\t<li>Юридический адрес банка:&nbsp;Москва, 127287, ул. 2-я Хуторская, д. 38А, стр. 26</li>\\r\\n</ul>\\r\\n</div>\", \"footer_contact_description\": \"<ul>\\r\\n\\t<li>Телефон: +7</li>\\r\\n\\t<li>Наш адрес:&nbsp;107076, город Москва, ул 1-Я Бухвостова, д. 12/11 к. 10, помещ. 9/18</li>\\r\\n\\t<li>Название: ООО &quot;Балан&quot;</li>\\r\\n\\t<li>ИНН:&nbsp;9718218176</li>\\r\\n\\t<li>Юридический адрес банка:&nbsp;Москва, 127287, ул. 2-я Хуторская, д. 38А, стр. 26</li>\\r\\n</ul>\"}}}',NULL,'2023-02-15 17:18:18','2023-02-15 17:18:18'),(7,'default','updated','Stepanenko3\\NovaSettings\\Models\\Settings','updated',1,'App\\Models\\User',1,'{\"ip\": \"192.168.48.1\", \"old\": {\"settings\": {\"logo\": \"2v3RazqEVjNqaa24S36Y9jtzRQb9lYEypjMbcuuF.png\", \"footer_map_iframe\": \"https://yandex.ru/map-widget/v1/?ll=37.714054%2C55.796333&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fdata%3DCgk3NzE5NTk4NDUSQ9Cg0L7RgdGB0LjRjywg0JzQvtGB0LrQstCwLCAxLdGPINGD0LvQuNGG0LAg0JHRg9GF0LLQvtGB0YLQvtCy0LAsIDEiCg041hZCFdYwX0I%3D&z=14.52\", \"contact_us_form_content\": \"<div class=\\\"textwidget\\\">\\r\\n<ul>\\r\\n\\t<li>Телефон: +7</li>\\r\\n\\t<li>Наш адрес:&nbsp;107076, город Москва, ул 1-Я Бухвостова, д. 12/11 к. 10, помещ. 9/18</li>\\r\\n\\t<li>Название: ООО &quot;Балан&quot;</li>\\r\\n\\t<li>ИНН:&nbsp;9718218176</li>\\r\\n\\t<li>Юридический адрес банка:&nbsp;Москва, 127287, ул. 2-я Хуторская, д. 38А, стр. 26</li>\\r\\n</ul>\\r\\n</div>\", \"footer_contact_description\": \"<ul>\\r\\n\\t<li>Телефон: +7</li>\\r\\n\\t<li>Наш адрес:&nbsp;107076, город Москва, ул 1-Я Бухвостова, д. 12/11 к. 10, помещ. 9/18</li>\\r\\n\\t<li>Название: ООО &quot;Балан&quot;</li>\\r\\n\\t<li>ИНН:&nbsp;9718218176</li>\\r\\n\\t<li>Юридический адрес банка:&nbsp;Москва, 127287, ул. 2-я Хуторская, д. 38А, стр. 26</li>\\r\\n</ul>\"}}, \"attributes\": {\"settings\": {\"logo\": null, \"footer_map_iframe\": \"https://yandex.ru/map-widget/v1/?ll=37.714054%2C55.796333&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fdata%3DCgk3NzE5NTk4NDUSQ9Cg0L7RgdGB0LjRjywg0JzQvtGB0LrQstCwLCAxLdGPINGD0LvQuNGG0LAg0JHRg9GF0LLQvtGB0YLQvtCy0LAsIDEiCg041hZCFdYwX0I%3D&z=14.52\", \"contact_us_form_content\": \"<div class=\\\"textwidget\\\">\\r\\n<ul>\\r\\n\\t<li>Телефон: +7</li>\\r\\n\\t<li>Наш адрес:&nbsp;107076, город Москва, ул 1-Я Бухвостова, д. 12/11 к. 10, помещ. 9/18</li>\\r\\n\\t<li>Название: ООО &quot;Балан&quot;</li>\\r\\n\\t<li>ИНН:&nbsp;9718218176</li>\\r\\n\\t<li>Юридический адрес банка:&nbsp;Москва, 127287, ул. 2-я Хуторская, д. 38А, стр. 26</li>\\r\\n</ul>\\r\\n</div>\", \"footer_contact_description\": \"<ul>\\r\\n\\t<li>Телефон: +7</li>\\r\\n\\t<li>Наш адрес:&nbsp;107076, город Москва, ул 1-Я Бухвостова, д. 12/11 к. 10, помещ. 9/18</li>\\r\\n\\t<li>Название: ООО &quot;Балан&quot;</li>\\r\\n\\t<li>ИНН:&nbsp;9718218176</li>\\r\\n\\t<li>Юридический адрес банка:&nbsp;Москва, 127287, ул. 2-я Хуторская, д. 38А, стр. 26</li>\\r\\n</ul>\"}}}',NULL,'2023-02-15 17:19:51','2023-02-15 17:19:51'),(8,'default','updated','Stepanenko3\\NovaSettings\\Models\\Settings','updated',1,'App\\Models\\User',1,'{\"ip\": \"192.168.48.1\", \"old\": {\"settings\": {\"logo\": null, \"footer_map_iframe\": \"https://yandex.ru/map-widget/v1/?ll=37.714054%2C55.796333&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fdata%3DCgk3NzE5NTk4NDUSQ9Cg0L7RgdGB0LjRjywg0JzQvtGB0LrQstCwLCAxLdGPINGD0LvQuNGG0LAg0JHRg9GF0LLQvtGB0YLQvtCy0LAsIDEiCg041hZCFdYwX0I%3D&z=14.52\", \"contact_us_form_content\": \"<div class=\\\"textwidget\\\">\\r\\n<ul>\\r\\n\\t<li>Телефон: +7</li>\\r\\n\\t<li>Наш адрес:&nbsp;107076, город Москва, ул 1-Я Бухвостова, д. 12/11 к. 10, помещ. 9/18</li>\\r\\n\\t<li>Название: ООО &quot;Балан&quot;</li>\\r\\n\\t<li>ИНН:&nbsp;9718218176</li>\\r\\n\\t<li>Юридический адрес банка:&nbsp;Москва, 127287, ул. 2-я Хуторская, д. 38А, стр. 26</li>\\r\\n</ul>\\r\\n</div>\", \"footer_contact_description\": \"<ul>\\r\\n\\t<li>Телефон: +7</li>\\r\\n\\t<li>Наш адрес:&nbsp;107076, город Москва, ул 1-Я Бухвостова, д. 12/11 к. 10, помещ. 9/18</li>\\r\\n\\t<li>Название: ООО &quot;Балан&quot;</li>\\r\\n\\t<li>ИНН:&nbsp;9718218176</li>\\r\\n\\t<li>Юридический адрес банка:&nbsp;Москва, 127287, ул. 2-я Хуторская, д. 38А, стр. 26</li>\\r\\n</ul>\"}}, \"attributes\": {\"settings\": {\"logo\": \"PWZGg39CBthsABzcIKH66M8jLm4edatYMQL5dtMs.png\", \"footer_map_iframe\": \"https://yandex.ru/map-widget/v1/?ll=37.714054%2C55.796333&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fdata%3DCgk3NzE5NTk4NDUSQ9Cg0L7RgdGB0LjRjywg0JzQvtGB0LrQstCwLCAxLdGPINGD0LvQuNGG0LAg0JHRg9GF0LLQvtGB0YLQvtCy0LAsIDEiCg041hZCFdYwX0I%3D&z=14.52\", \"contact_us_form_content\": \"<div class=\\\"textwidget\\\">\\r\\n<ul>\\r\\n\\t<li>Телефон: +7</li>\\r\\n\\t<li>Наш адрес:&nbsp;107076, город Москва, ул 1-Я Бухвостова, д. 12/11 к. 10, помещ. 9/18</li>\\r\\n\\t<li>Название: ООО &quot;Балан&quot;</li>\\r\\n\\t<li>ИНН:&nbsp;9718218176</li>\\r\\n\\t<li>Юридический адрес банка:&nbsp;Москва, 127287, ул. 2-я Хуторская, д. 38А, стр. 26</li>\\r\\n</ul>\\r\\n</div>\", \"footer_contact_description\": \"<ul>\\r\\n\\t<li>Телефон: +7</li>\\r\\n\\t<li>Наш адрес:&nbsp;107076, город Москва, ул 1-Я Бухвостова, д. 12/11 к. 10, помещ. 9/18</li>\\r\\n\\t<li>Название: ООО &quot;Балан&quot;</li>\\r\\n\\t<li>ИНН:&nbsp;9718218176</li>\\r\\n\\t<li>Юридический адрес банка:&nbsp;Москва, 127287, ул. 2-я Хуторская, д. 38А, стр. 26</li>\\r\\n</ul>\"}}}',NULL,'2023-02-15 17:19:56','2023-02-15 17:19:56');
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `advantages`
--

DROP TABLE IF EXISTS `advantages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `advantages` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT '0',
  `order` int DEFAULT '0',
  `icon` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advantages`
--

LOCK TABLES `advantages` WRITE;
/*!40000 ALTER TABLE `advantages` DISABLE KEYS */;
INSERT INTO `advantages` (`id`, `title`, `excerpt`, `active`, `order`, `icon`, `created_at`, `updated_at`) VALUES (1,'Монтажники в штате','Шесть квалифицированных монтажников с опытом работы более 10 лет',1,2,'pe-tools','2023-02-14 15:45:32','2023-02-14 16:08:01'),(2,'Выполнение проектов в сжатые сроки','Выход на объект и начало работ на следующий день поле подписания договора. При необходимости, возможно выполнение работы в ночное время',1,1,'pe-camera','2023-02-14 15:46:21','2023-02-14 16:08:01'),(3,'Аккуратный монтаж','Сверление отверстий в помещениях с чистовой отделкой ведется с пылесосом. Монтажники уберут после себя мусор.',1,3,'pe-photo','2023-02-14 15:46:49','2023-02-14 15:46:49');
/*!40000 ALTER TABLE `advantages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clients` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `competences`
--

DROP TABLE IF EXISTS `competences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `competences` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT '0',
  `order` int DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competences`
--

LOCK TABLES `competences` WRITE;
/*!40000 ALTER TABLE `competences` DISABLE KEYS */;
INSERT INTO `competences` (`id`, `title`, `excerpt`, `description`, `active`, `order`, `created_at`, `updated_at`) VALUES (3,'Видеонаблюдение','Установка систем видеонаблюдения: от простейших для наблюдения за дачным участком и домом - до систем с аналитикой данных и многофункциональным ПО','<div>Установка систем видеонаблюдения: от простейших для наблюдения за дачным участком и домом - до систем с аналитикой данных и многофункциональным ПО<br><br></div>',1,1,'2023-02-13 18:08:25','2023-02-13 18:16:02'),(4,'СКУД','Установка систем контроля и управления доступом: от установки видеодомофона и замка на тамбурную дверь – до организации системы прохода КПП в аэропорте','<div>Установка систем контроля и управления доступом: от установки видеодомофона и замка на тамбурную дверь – до организации системы прохода КПП в аэропорте<br><br></div>',1,2,'2023-02-13 18:08:58','2023-02-13 18:08:58'),(5,'Системы автопроезда','Установка автоматических распашных, откатных, секционных ворот. Установка электромеханических, гидравлических, антивандальных шлагбаумов. Способы доступа: считывание номеров, RFID метки, пульты, GSM модуль.','<div>Установка автоматических распашных, откатных, секционных ворот. Установка электромеханических, гидравлических, антивандальных шлагбаумов. Способы доступа: считывание номеров, RFID метки, пульты, GSM модуль.<br><br></div>',1,3,'2023-02-13 18:09:14','2023-02-13 18:09:14');
/*!40000 ALTER TABLE `competences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `media` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint unsigned NOT NULL,
  `uuid` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `conversions_disk` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` bigint unsigned NOT NULL,
  `manipulations` json NOT NULL,
  `custom_properties` json NOT NULL,
  `generated_conversions` json NOT NULL,
  `responsive_images` json NOT NULL,
  `order_column` int unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `media_uuid_unique` (`uuid`),
  KEY `media_model_type_model_id_index` (`model_type`,`model_id`),
  KEY `media_order_column_index` (`order_column`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` (`id`, `model_type`, `model_id`, `uuid`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `conversions_disk`, `size`, `manipulations`, `custom_properties`, `generated_conversions`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES (5,'App\\Models\\Competence',4,'4d31cdc5-9eb6-4ffa-87d0-b98f1b5e8dd3','default','img3-2','img3-2.jpg','image/jpeg','public','public',80603,'[]','[]','[]','[]',1,'2023-02-13 18:08:58','2023-02-13 18:08:58'),(6,'App\\Models\\Competence',5,'26d819b7-3193-451d-bb86-db8ecfc2685d','default','img1-6','img1-6.jpg','image/jpeg','public','public',124905,'[]','[]','[]','[]',1,'2023-02-13 18:09:14','2023-02-13 18:09:14'),(7,'App\\Models\\Competence',3,'1c42d987-d3c7-45a0-bc40-9868cf010109','default','img32','img32.jpg','image/jpeg','web','web',620796,'[]','[]','[]','[]',1,'2023-02-13 18:16:02','2023-02-13 18:16:02'),(8,'App\\Models\\Partner',1,'4ff33904-279d-4358-a52c-919ad09eb4d3','default','rit','rit.jpg','image/jpeg','web','web',5800,'[]','[]','[]','[]',1,'2023-02-14 16:38:35','2023-02-14 16:38:35'),(9,'App\\Models\\Partner',2,'2ec38c6c-e169-4f4d-868b-a67edfe6700e','default','panduit (1)','panduit-(1).jpg','image/jpeg','web','web',3960,'[]','[]','[]','[]',1,'2023-02-14 16:38:41','2023-02-14 16:38:41'),(10,'App\\Models\\Partner',3,'5ed4dcc0-edec-4dad-85f7-db6de8dbac39','default','legrand','legrand.jpg','image/jpeg','web','web',6771,'[]','[]','[]','[]',1,'2023-02-14 16:38:47','2023-02-14 16:38:47'),(11,'App\\Models\\Partner',4,'7fe0e0ab-bac6-492d-b24d-ffafbefa4944','default','dkc','dkc.jpg','image/jpeg','web','web',6086,'[]','[]','[]','[]',1,'2023-02-14 16:38:55','2023-02-14 16:38:55'),(12,'App\\Models\\Partner',5,'c439af43-4e15-4fdf-9312-18d86e44a3c1','default','cabeus','cabeus.jpg','image/jpeg','web','web',5328,'[]','[]','[]','[]',1,'2023-02-14 16:39:34','2023-02-14 16:39:34'),(13,'App\\Models\\Portfolio',1,'f4881b6f-9d48-4773-8562-56baf2f1b6a1','default','portfolio6','portfolio6.jpg','image/jpeg','web','web',164738,'[]','[]','[]','[]',1,'2023-02-15 16:10:05','2023-02-15 16:10:05'),(14,'App\\Models\\Portfolio',2,'5feb3c94-116c-40cd-8576-715aa69901a4','default','portfolio5','portfolio5.jpg','image/jpeg','web','web',79152,'[]','[]','[]','[]',1,'2023-02-15 16:10:22','2023-02-15 16:10:22'),(15,'App\\Models\\Portfolio',3,'7258d0b7-ac4e-4292-8f87-2384d21275fd','default','portfolio1','portfolio1.jpg','image/jpeg','web','web',69140,'[]','[]','[]','[]',1,'2023-02-15 16:10:37','2023-02-15 16:10:37');
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_01_01_000000_create_action_events_table',1),(4,'2019_05_10_000000_add_fields_to_action_events_table',1),(5,'2019_08_19_000000_create_failed_jobs_table',1),(6,'2019_12_14_000001_create_personal_access_tokens_table',1),(7,'2021_08_25_193039_create_nova_notifications_table',1),(8,'2022_04_26_000000_add_fields_to_nova_notifications_table',1),(9,'2022_05_10_224818_create_settings_table',1),(10,'2023_02_11_164730_create_permission_tables',1),(11,'2023_02_12_172104_create_activity_log_table',1),(12,'2023_02_12_172105_add_event_column_to_activity_log_table',1),(13,'2023_02_12_172106_add_batch_uuid_column_to_activity_log_table',1),(14,'2023_02_12_182430_create_clients_table',1),(17,'2023_02_13_175950_create_media_table',3),(18,'2023_02_13_174740_create_competences_table',4),(19,'2023_02_14_153745_create_provide_services_table',5),(20,'2023_02_12_223630_create_partners_table',6),(21,'2023_02_15_160319_create_portfolios_table',7),(22,'2023_02_15_181523_create_service_types_table',8),(23,'2023_02_15_181533_create_object_types_table',8);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` bigint unsigned NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model_has_roles` (
  `role_id` bigint unsigned NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES (1,'App\\Models\\User',1);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nova_notifications`
--

DROP TABLE IF EXISTS `nova_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nova_notifications` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint unsigned NOT NULL,
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nova_notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nova_notifications`
--

LOCK TABLES `nova_notifications` WRITE;
/*!40000 ALTER TABLE `nova_notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `nova_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `object_types`
--

DROP TABLE IF EXISTS `object_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `object_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT '0',
  `order` int DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `object_types`
--

LOCK TABLES `object_types` WRITE;
/*!40000 ALTER TABLE `object_types` DISABLE KEYS */;
INSERT INTO `object_types` (`id`, `title`, `active`, `order`, `created_at`, `updated_at`) VALUES (1,'Унитаз',1,1,'2023-02-15 18:23:33','2023-02-15 18:23:33');
/*!40000 ALTER TABLE `object_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `partners` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partners`
--

LOCK TABLES `partners` WRITE;
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;
INSERT INTO `partners` (`id`, `title`, `active`, `created_at`, `updated_at`) VALUES (1,'Rit',1,'2023-02-14 16:38:35','2023-02-14 16:38:35'),(2,'Panduit',1,'2023-02-14 16:38:41','2023-02-14 16:38:41'),(3,'Legrand',1,'2023-02-14 16:38:47','2023-02-14 16:38:47'),(4,'DKC',1,'2023-02-14 16:38:55','2023-02-14 16:38:55'),(5,'Cabeus',1,'2023-02-14 16:39:34','2023-02-14 16:39:34');
/*!40000 ALTER TABLE `partners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES (1,'view users','web','2023-02-13 17:10:25','2023-02-13 17:10:25'),(2,'create users','web','2023-02-13 17:10:25','2023-02-13 17:10:25'),(3,'update users','web','2023-02-13 17:10:25','2023-02-13 17:10:25'),(4,'delete users','web','2023-02-13 17:10:25','2023-02-13 17:10:25'),(5,'view clients','web','2023-02-13 17:10:25','2023-02-13 17:10:25'),(6,'create clients','web','2023-02-13 17:10:25','2023-02-13 17:10:25'),(7,'update clients','web','2023-02-13 17:10:25','2023-02-13 17:10:25'),(8,'delete clients','web','2023-02-13 17:10:25','2023-02-13 17:10:25'),(9,'manage site','web','2023-02-13 17:10:25','2023-02-13 17:10:25');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolios`
--

DROP TABLE IF EXISTS `portfolios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `portfolios` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT '0',
  `order` int DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolios`
--

LOCK TABLES `portfolios` WRITE;
/*!40000 ALTER TABLE `portfolios` DISABLE KEYS */;
INSERT INTO `portfolios` (`id`, `title`, `description`, `active`, `order`, `created_at`, `updated_at`) VALUES (1,'Дом в деревне','<div>Работали в домике в деревне</div>',1,3,'2023-02-15 16:10:05','2023-02-15 16:13:26'),(2,'Второй объект','<div>Работали во втором объекте</div>',1,1,'2023-02-15 16:10:22','2023-02-15 16:13:25'),(3,'Трети й объект','<div>Работали в третьем объекте</div>',1,2,'2023-02-15 16:10:37','2023-02-15 16:13:26');
/*!40000 ALTER TABLE `portfolios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(9,2),(5,3),(6,3),(7,3),(8,3);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES (1,'Главный администратор','web','2023-02-13 17:10:25','2023-02-13 17:10:25'),(2,'Менеджер сайта','web','2023-02-13 17:10:25','2023-02-13 17:10:25'),(3,'Оператор','web','2023-02-13 17:10:25','2023-02-13 17:10:25');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_types`
--

DROP TABLE IF EXISTS `service_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT '0',
  `order` int DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_types`
--

LOCK TABLES `service_types` WRITE;
/*!40000 ALTER TABLE `service_types` DISABLE KEYS */;
INSERT INTO `service_types` (`id`, `title`, `active`, `order`, `created_at`, `updated_at`) VALUES (1,'Установка видео',1,1,'2023-02-15 18:23:21','2023-02-15 18:23:21');
/*!40000 ALTER TABLE `service_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `settings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `env` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` json DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_slug_env_type_unique` (`slug`,`env`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `slug`, `env`, `type`, `settings`, `created_at`, `updated_at`) VALUES (1,'site','*','App\\Nova\\NovaSettings\\General','{\"logo\": \"PWZGg39CBthsABzcIKH66M8jLm4edatYMQL5dtMs.png\", \"footer_map_iframe\": \"https://yandex.ru/map-widget/v1/?ll=37.714054%2C55.796333&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fdata%3DCgk3NzE5NTk4NDUSQ9Cg0L7RgdGB0LjRjywg0JzQvtGB0LrQstCwLCAxLdGPINGD0LvQuNGG0LAg0JHRg9GF0LLQvtGB0YLQvtCy0LAsIDEiCg041hZCFdYwX0I%3D&z=14.52\", \"contact_us_form_content\": \"<div class=\\\"textwidget\\\">\\r\\n<ul>\\r\\n\\t<li>Телефон: +7</li>\\r\\n\\t<li>Наш адрес:&nbsp;107076, город Москва, ул 1-Я Бухвостова, д. 12/11 к. 10, помещ. 9/18</li>\\r\\n\\t<li>Название: ООО &quot;Балан&quot;</li>\\r\\n\\t<li>ИНН:&nbsp;9718218176</li>\\r\\n\\t<li>Юридический адрес банка:&nbsp;Москва, 127287, ул. 2-я Хуторская, д. 38А, стр. 26</li>\\r\\n</ul>\\r\\n</div>\", \"footer_contact_description\": \"<ul>\\r\\n\\t<li>Телефон: +7</li>\\r\\n\\t<li>Наш адрес:&nbsp;107076, город Москва, ул 1-Я Бухвостова, д. 12/11 к. 10, помещ. 9/18</li>\\r\\n\\t<li>Название: ООО &quot;Балан&quot;</li>\\r\\n\\t<li>ИНН:&nbsp;9718218176</li>\\r\\n\\t<li>Юридический адрес банка:&nbsp;Москва, 127287, ул. 2-я Хуторская, д. 38А, стр. 26</li>\\r\\n</ul>\"}','2023-02-15 16:39:53','2023-02-15 17:19:56');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES (1,'Admin','admin@gmail.com',NULL,'$2y$10$YkVVHTEwbuRoep1R6xrxIeeGGRuo/lbuzxM7DmRZyb8Oj5I7cIYF.','F4ENReeIvaHlLXTN7JXH9miL6Q6HqNWrpVEMtlH2oLWbt4CkcvvX9sz6BReL','2023-02-13 17:10:26','2023-02-13 17:10:26');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-02-15 21:17:06
