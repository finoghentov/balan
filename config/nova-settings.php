<?php

return [
    'model' => \Stepanenko3\NovaSettings\Models\Settings::class,

    'resource' => \App\Nova\Settings::class,

    'types' => [
        \App\Nova\NovaSettings\General::class
    ],

    'roles' => [
        'Главный администратор', 'Менеджер сайта'
    ]
];
