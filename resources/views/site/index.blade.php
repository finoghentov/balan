@extends('layouts.app')

@section('content')
    <div id="main">            <!-- ** Container ** -->
        <section id="primary" class="content-full-width">    <!-- #post-9275 -->
                <div id="post-9275" class="post-9275 page type-page status-publish hentry">
                    @include('site.parts._competence')
                    @include('site.parts._our_success')
                    @include('site.parts._advantages')
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="ult-spacer spacer-63e947f473d6a" data-id="63e947f473d6a"
                                         data-height="80" data-height-mobile="80" data-height-tab="80"
                                         data-height-tab-portrait="" data-height-mobile-landscape=""
                                         style="clear:both;display:block;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
{{--                    @include('site.parts._portfolio')--}}
                    @include('site.parts._partners')
                    @include('site.parts._contact-us')
                </div>
            </section>
    </div>
@endsection

@section('script')
    <script>
        jQuery(document).ready(function ($) {
            $('.carousel').slick({
                dots: true,
                autoplay: true,
                autoplaySpeed: 5000,
                speed: 300,
                infinite: true,
                arrows: false,
                slidesToScroll: 1,
                slidesToShow: 4,
                swipe: true,
                draggable: true,
                touchMove: true,
                pauseOnHover: true,
                pauseOnFocus: false,
                responsive: [
                    {
                        breakpoint: 1026,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                        }
                    },
                    {
                        breakpoint: 1025,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 760,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ],
                pauseOnDotsHover: true,
                customPaging: function (slider, i) {
                    return '<i type="button" style= "color:#333333;" class="ultsl-record" data-role="none"></i>';
                },
            });
        });

        function scrollToElem(id) {
            document.getElementById(id).scrollIntoView({
                behavior: 'smooth'
            });
        }

        function toggleMenu() {
            const menu = document.querySelector("#main-menu > ul");
            console.log(menu);
            if (menu.style.display === 'none' || !menu.style.display) {
                menu.style.display = 'block';
            } else {
                menu.style.display = 'none';
            }
        }
    </script>
    <script>
        $("#counter-1").countMe(40,15);
        $("#counter-2").countMe(40,20);
        $("#counter-3").countMe(40,10);
        $("#counter-4").countMe(40,25);
    </script>
    <script>
        IMask(document.querySelector('input[name="phone"]'), {
            mask: '+{7}(000)000-00-00'
        });
    </script>
@endsection
