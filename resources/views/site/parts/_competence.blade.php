@if($competences->isNotEmpty())
    <div class="container">
        <div class="vc_row wpb_row vc_row-fluid vc_custom_1505975565958">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div id="about-us-block" class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-2">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper"></div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-8">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1505823797609">

                                            <figure class="wpb_wrapper vc_figure">
                                                <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                    <img width="122" height="26"
                                                         src="{{config('app.url')}}/wp-content/uploads/2017/09/img1-1.png"
                                                         class="vc_single_image-img attachment-full"
                                                         alt="" decoding="async" loading="lazy"/></div>
                                            </figure>
                                        </div>
                                        <div class='dt-sc-title script-with-sub-title aligncenter'>
                                            <h4>
                                                {{__('site.about_us_subtitle')}}
                                            </h4>
                                            <h2>
                                                {{__('site.about_us_title')}}
                                            </h2>
                                        </div>
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <p style="text-align: center;">{{__('site.about_us_description')}}</p>

                                            </div>
                                        </div>
                                        <div class="ult-spacer spacer-63e947f46f105"
                                             data-id="63e947f46f105" data-height="10"
                                             data-height-mobile="10" data-height-tab="10"
                                             data-height-tab-portrait="" data-height-mobile-landscape=""
                                             style="clear:both;display:block;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-2">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper"></div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            @foreach($competences as $competence)
                                <div class="modal fade bd-example-modal-lg" id="modal-competence-{{$competence->id}}"
                                     tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title"
                                                    id="exampleModalLongTitle">{{$competence->title}}</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                {!! $competence->description !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-4" data-toggle="modal"
                                     data-target="#modal-competence-{{$competence->id}}">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <h4 style="text-align: center" class="vc_custom_heading">
                                                {{$competence->title}}
                                            </h4>
                                            <div class="ult-new-ib ult-ib-effect-style6  ult-ib-resp   vc_custom_1505913847098"
                                                 data-min-width="768" data-max-width="900"
                                                 style="background:#000000;" data-opacity="1"
                                                 data-hover-opacity="0.8">
                                                <img class="ult-new-ib-img"
                                                     style="opacity:1;" alt="null"
                                                     src="{{$competence->getFirstMedia()->getUrl()}}"/>
                                                <div id="interactive-banner-wrap-1566"
                                                     class="ult-new-ib-desc" style=""><h2
                                                            class="ult-new-ib-title ult-responsive"
                                                            data-ultimate-target='#interactive-banner-wrap-1566 .ult-new-ib-title'
                                                            data-responsive-json-new='{"font-size":"","line-height":""}'
                                                            style="font-weight:normal;"></h2></div>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p style="text-align: center;">{{$competence->excerpt}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
