<div id="partners-block" class="vc_row wpb_row vc_row-fluid  gray-bg" style="padding-bottom: 20px;">
    <div class="container">
        <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner ">
                <div class="wpb_wrapper">
                    <div class="ult-spacer spacer-63e947f47ddd2" data-id="63e947f47ddd2"
                         data-height="50" data-height-mobile="50" data-height-tab="50"
                         data-height-tab-portrait="" data-height-mobile-landscape=""
                         style="clear:both;display:block;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="vc_row wpb_row vc_inner vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-2">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper"></div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-8">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1505823797609">

                                    <figure class="wpb_wrapper vc_figure">
                                        <div class="vc_single_image-wrapper   vc_box_border_grey">
                                            <img width="122" height="26"
                                                 src="{{config('app.url')}}/wp-content/uploads/2017/09/img1-1.png"
                                                 class="vc_single_image-img attachment-full"
                                                 alt="" decoding="async" loading="lazy"/></div>
                                    </figure>
                                </div>
                                <div class='dt-sc-title script-with-sub-title aligncenter'>
                                    <h4>
                                        {{__('site.partners_subtitle')}}
                                    </h4>
                                    <h2>
                                        {{__('site.partners_title')}}
                                    </h2>
                                </div>
                                <div class="ult-spacer spacer-63e947f47ea6d"
                                     data-id="63e947f47ea6d" data-height="10"
                                     data-height-mobile="10" data-height-tab="10"
                                     data-height-tab-portrait="" data-height-mobile-landscape=""
                                     style="clear:both;display:block;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-2">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<div class="vc_row wpb_row vc_row-fluid gray-bg">
    <div class="container">
            <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div id="ult-carousel-359210950563e947f47f05f"
                     class="ult-carousel-wrapper  vcr_top-align ult_horizontal" data-gutter="15"
                     data-rtl="false">
                    <div class="carousel">
                        @foreach($partners as $partner)
                            <div class="ult-item-wrap" data-animation="animated no-animation">
                                <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                    <div class="ult-item-wrap"
                                         data-animation="animated no-animation">
                                        <div class="wpb_column vc_column_container vc_col-sm-6" style="width: 100%">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="ult-item-wrap"
                                                         data-animation="animated no-animation">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">

                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                    <img
                                                                         src="{{$partner->getFirstMediaUrl()}}"
                                                                         class="vc_single_image-img attachment-large"
                                                                         alt="{{$partner->title}}" title="{{$partner->title}}" decoding="async"
                                                                         loading="lazy"/>
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="ult-spacer spacer-63e947f482165" data-id="63e947f482165"
                     data-height="40" data-height-mobile="40" data-height-tab="40"
                     data-height-tab-portrait="" data-height-mobile-landscape=""
                     style="clear:both;display:block;"></div>
</div>

