<div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true"
     class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
    <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner ">
                <div class="wpb_wrapper">
                    <div class="ult-spacer spacer-63e947f473d6a1" data-id="63e947f473d6a1"
                         data-height="80" data-height-mobile="80" data-height-tab="80"
                         data-height-tab-portrait="" data-height-mobile-landscape=""
                         style="clear:both;display:block;"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1505823797609">

                    <figure class="wpb_wrapper vc_figure">
                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                    width="122" height="26"
                                    src="{{config('app.url')}}/wp-content/uploads/2017/09/img1-1.png"
                                    class="vc_single_image-img attachment-full" alt=""
                                    decoding="async" loading="lazy"/></div>
                    </figure>
                </div>
                <div class='dt-sc-title script-with-sub-title aligncenter'>
                    <h4>Портфолио</h4>
                    <h2>Примеры наших работ</h2></div>
                <div class='dt-sc-clear '></div>
                <div class="ult-spacer spacer-63e947f47659c" data-id="63e947f47659c"
                     data-height="30" data-height-mobile="30" data-height-tab="30"
                     data-height-tab-portrait="" data-height-mobile-landscape=""
                     style="clear:both;display:block;"></div>

                <div class="dt-sc-portfolio-container no-space">
                    <div class="grid-sizer  portfolio column dt-sc-one-third all-sort"></div>
                    <div id="dt_portfolios-7425"
                         class="type5 no-space portfolio column dt-sc-one-third all-sort first alarm-lock-sort">
                        <figure><img
                                    src="{{config('app.url')}}/wp-content/uploads/2014/01/portfolio1.jpg"
                                    alt="Cras tristique purus non lacus"
                                    title="Cras tristique purus non lacus"/>
                            <div class="image-overlay">
                                <div class="links"><a title="Cras tristique purus non lacus"
                                                      href="{{config('app.url')}}/dt_portfolios/cras-tristique-purus-non-lacus/"><span
                                                class="icon icon-linked"> </span></a><a
                                            title="Cras tristique purus non lacus"
                                            data-gal="prettyPhoto[gallery]"
                                            href="{{config('app.url')}}/wp-content/uploads/2014/01/portfolio1.jpg">
                                        <span class="icon icon-search"> </span> </a></div>
                                <div class="image-overlay-details"><h2><a
                                                title="Cras tristique purus non lacus"
                                                href="{{config('app.url')}}/dt_portfolios/cras-tristique-purus-non-lacus/">Cras
                                            tristique purus non lacus</a></h2>
                                    <p class='categories'><a
                                                href="{{config('app.url')}}/portfolio_entries/alarm-lock/"
                                                rel="tag">Alarm &amp; lock</a></p></div>
                            </div>
                        </figure>
                    </div>
                    <div id="dt_portfolios-7424"
                         class="type5 no-space portfolio column dt-sc-one-third all-sort day-night-cams-sort">
                        <figure><img
                                    src="{{config('app.url')}}/wp-content/uploads/2014/01/portfolio2.jpg"
                                    alt="Duis fermentum felis" title="Duis fermentum felis"/>
                            <div class="image-overlay">
                                <div class="links"><a title="Duis fermentum felis"
                                                      href="{{config('app.url')}}/dt_portfolios/duis-fermentum-felis/"><span
                                                class="icon icon-linked"> </span></a><a
                                            title="Duis fermentum felis"
                                            data-gal="prettyPhoto[gallery]"
                                            href="{{config('app.url')}}/wp-content/uploads/2014/01/portfolio2.jpg">
                                        <span class="icon icon-search"> </span> </a></div>
                                <div class="image-overlay-details"><h2><a
                                                title="Duis fermentum felis"
                                                href="{{config('app.url')}}/dt_portfolios/duis-fermentum-felis/">Duis
                                            fermentum felis</a></h2>
                                    <p class='categories'><a
                                                href="{{config('app.url')}}/portfolio_entries/day-night-cams/"
                                                rel="tag">Day &amp; night cams</a></p></div>
                            </div>
                        </figure>
                    </div>
                    <div id="dt_portfolios-7423"
                         class="type5 no-space portfolio column dt-sc-one-third all-sort closed-circuit-sort">
                        <figure><img
                                    src="{{config('app.url')}}/wp-content/uploads/2014/01/portfolio3.jpg"
                                    alt="Quisque id maximus leo" title="Quisque id maximus leo"/>
                            <div class="image-overlay">
                                <div class="links"><a title="Quisque id maximus leo"
                                                      href="{{config('app.url')}}/dt_portfolios/quisque-id-maximus-leo/"><span
                                                class="icon icon-linked"> </span></a><a
                                            title="Quisque id maximus leo"
                                            data-gal="prettyPhoto[gallery]"
                                            href="{{config('app.url')}}/wp-content/uploads/2014/01/portfolio3.jpg">
                                        <span class="icon icon-search"> </span> </a></div>
                                <div class="image-overlay-details"><h2><a
                                                title="Quisque id maximus leo"
                                                href="{{config('app.url')}}/dt_portfolios/quisque-id-maximus-leo/">Quisque
                                            id maximus leo</a></h2>
                                    <p class='categories'><a
                                                href="{{config('app.url')}}/portfolio_entries/closed-circuit/"
                                                rel="tag">Closed Circuit</a></p></div>
                            </div>
                        </figure>
                    </div>
                    <div id="dt_portfolios-7422"
                         class="type5 no-space portfolio column dt-sc-one-third all-sort first wireless-cams-sort">
                        <figure><img
                                    src="{{config('app.url')}}/wp-content/uploads/2014/01/portfolio4.jpg"
                                    alt="Proin venenatis felis" title="Proin venenatis felis"/>
                            <div class="image-overlay">
                                <div class="links"><a title="Proin venenatis felis"
                                                      href="{{config('app.url')}}/dt_portfolios/proin-venenatis-felis/"><span
                                                class="icon icon-linked"> </span></a><a
                                            title="Proin venenatis felis"
                                            data-gal="prettyPhoto[gallery]"
                                            href="{{config('app.url')}}/wp-content/uploads/2014/01/portfolio4.jpg">
                                        <span class="icon icon-search"> </span> </a></div>
                                <div class="image-overlay-details"><h2><a
                                                title="Proin venenatis felis"
                                                href="{{config('app.url')}}/dt_portfolios/proin-venenatis-felis/">Proin
                                            venenatis felis</a></h2>
                                    <p class='categories'><a
                                                href="{{config('app.url')}}/portfolio_entries/wireless-cams/"
                                                rel="tag">Wireless cams</a></p></div>
                            </div>
                        </figure>
                    </div>
                    <div id="dt_portfolios-7421"
                         class="type5 no-space portfolio column dt-sc-one-third all-sort day-night-cams-sort">
                        <figure><img
                                    src="{{config('app.url')}}/wp-content/uploads/2014/01/portfolio5.jpg"
                                    alt="Aenean vitae enim rhoncus"
                                    title="Aenean vitae enim rhoncus"/>
                            <div class="image-overlay">
                                <div class="links"><a title="Aenean vitae enim rhoncus"
                                                      href="{{config('app.url')}}/dt_portfolios/aenean-vitae-enim-rhoncus/"><span
                                                class="icon icon-linked"> </span></a><a
                                            title="Aenean vitae enim rhoncus"
                                            data-gal="prettyPhoto[gallery]"
                                            href="{{config('app.url')}}/wp-content/uploads/2014/01/portfolio5.jpg">
                                        <span class="icon icon-search"> </span> </a></div>
                                <div class="image-overlay-details"><h2><a
                                                title="Aenean vitae enim rhoncus"
                                                href="{{config('app.url')}}/dt_portfolios/aenean-vitae-enim-rhoncus/">Aenean
                                            vitae enim rhoncus</a></h2>
                                    <p class='categories'><a
                                                href="{{config('app.url')}}/portfolio_entries/day-night-cams/"
                                                rel="tag">Day &amp; night cams</a></p></div>
                            </div>
                        </figure>
                    </div>
                    <div id="dt_portfolios-7420"
                         class="type5 no-space portfolio column dt-sc-one-third all-sort wireless-cams-sort">
                        <figure><img
                                    src="{{config('app.url')}}/wp-content/uploads/2014/01/portfolio6.jpg"
                                    alt="Donec in maximus augue" title="Donec in maximus augue"/>
                            <div class="image-overlay">
                                <div class="links"><a title="Donec in maximus augue"
                                                      href="{{config('app.url')}}/dt_portfolios/donec-in-maximus-augue/"><span
                                                class="icon icon-linked"> </span></a><a
                                            title="Donec in maximus augue"
                                            data-gal="prettyPhoto[gallery]"
                                            href="{{config('app.url')}}/wp-content/uploads/2014/01/portfolio6.jpg">
                                        <span class="icon icon-search"> </span> </a></div>
                                <div class="image-overlay-details"><h2><a
                                                title="Donec in maximus augue"
                                                href="{{config('app.url')}}/dt_portfolios/donec-in-maximus-augue/">Donec
                                            in maximus augue</a></h2>
                                    <p class='categories'><a
                                                href="{{config('app.url')}}/portfolio_entries/wireless-cams/"
                                                rel="tag">Wireless cams</a></p></div>
                            </div>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
