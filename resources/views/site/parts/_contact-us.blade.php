<div class="vc_row wpb_row vc_row-fluid">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="ult-spacer spacer-63e947f482165" data-id="63e947f482165"
                     data-height="40" data-height-mobile="40" data-height-tab="40"
                     data-height-tab-portrait="" data-height-mobile-landscape=""
                     style="clear:both;display:block;"></div>
            </div>
        </div>
    </div>
</div>
<div class="container" id="contact-us">
    <div class="vc_row wpb_row vc_row-fluid contact-section1 vc_custom_1471527830284">
        <div class="wpb_column vc_column_container vc_col-sm-6">
            <div class="vc_column-inner ">
                <div class="wpb_wrapper"><h2 style="text-align: left"
                                             class="vc_custom_heading">{{__('site.contact_us_title')}}</h2>
                    <div class="ult-spacer spacer-63eb4bf7b41cd" data-id="63eb4bf7b41cd" data-height="20"
                         data-height-mobile="20" data-height-tab="20" data-height-tab-portrait=""
                         data-height-mobile-landscape="" style="clear:both;display:block;"></div>
                    <div class="vc_row wpb_row vc_inner vc_row-fluid details">
                        {!! settings('site', 'footer_contact_description', '', '*') !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="wpb_column vc_column_container vc_col-sm-6">
            <div class="vc_column-inner ">
                <div class="wpb_wrapper"><h2 style="text-align: left"
                                             class="vc_custom_heading">{{__('site.contact_us_form_title')}}</h2>
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <p>Отправьте свои данные и мы позвоним вам в близжайшее время!</p>
                        </div>
                    </div>
                    <div class="ult-spacer spacer-63eb4bf7b570e" data-id="63eb4bf7b570e" data-height="13"
                         data-height-mobile="13" data-height-tab="13" data-height-tab-portrait=""
                         data-height-mobile-landscape="" style="clear:both;display:block;"></div>
                    <div role="form" class="wpcf7" id="wpcf7-f7816-p7815-o1" lang="en-US" dir="ltr">
                        <div class="screen-reader-response">
                            <ul></ul>
                        </div>
                        @if (session()->has('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        <form action="{{action([\App\Http\Controllers\SiteController::class, 'contact'])}}" method="post" class="wpcf7-form init cctv-form">
                            @csrf

                            <p>
                                <span class="wpcf7-form-control-wrap text-414">
                                    <input type="text"
                                           name="name"
                                           class="wpcf7-form-control wpcf7-text"
                                           placeholder="{{__('site.contact_us_name')}}*">
                                </span>
                            </p>

                            <p>
                                <span class="wpcf7-form-control-wrap text-414">
                                     @error('name')
                                    <div class="alert alert-danger" style="float: left" role="alert">
                                        Пожалуйста, введите имя
                                    </div>
                                    @enderror
                                    <input
                                            type="text"
                                            name="phone"
                                            class="wpcf7-form-control wpcf7-text"
                                            placeholder="{{__('site.contact_us_phone')}}*">
                                </span>
                            </p>
                            <p>
                                <span class="wpcf7-form-control-wrap text-414">
                                     @error('phone')
                                    <div style="float: left" class="alert alert-danger" role="alert">
                                        Пожалуйста, введите телефон
                                    </div>
                                    @enderror
                                    <select style="padding-left: 10px;" name="object_type">
                                        <option value="">{{__('site.contact_us_object_type')}}</option>
                                        @foreach($objectTypes as $object)
                                            <option value="{{$object->id}}">
                                                {{$object->title}}
                                            </option>
                                        @endforeach
                                    </select>
                                </span>
                            </p>
                            <p>
                                <span class="wpcf7-form-control-wrap text-414">
                                    <select style="padding-left: 10px;" name="service_type">
                                        <option value="">{{__('site.contact_us_service_type')}}</option>
                                        @foreach($serviceTypes as $service)
                                            <option value="{{$service->id}}">
                                                {{$service->title}}
                                            </option>
                                        @endforeach
                                    </select>
                                </span>
                            </p>

                            <p class="form-col-main1">
                                <input type="submit" value="{{__("site.contact_us_submit")}}"
                                       class="wpcf7-form-control wpcf7-submit">
                            </p>
                            <div class="wpcf7-response-output" aria-hidden="true"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="vc_row wpb_row vc_row-fluid">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="ult-spacer spacer-63e947f482165" data-id="63e947f482165"
                     data-height="40" data-height-mobile="40" data-height-tab="40"
                     data-height-tab-portrait="" data-height-mobile-landscape=""
                     style="clear:both;display:block;"></div>
            </div>
        </div>
    </div>
</div>
