<div class="container" id="advantages-block">
    <div data-vc-full-width="true" data-vc-full-width-init="false"
         class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner ">
                <div class="wpb_wrapper">
                    <div class="vc_row wpb_row vc_inner vc_row-fluid vc_column-gap-15">
                        <div class="wpb_column vc_column_container vc_col-sm-6">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class='dt-sc-image-caption  '>
                                        <div class='dt-sc-image-wrapper'>
                                            <img width="768"
                                                 height="592"
                                                 src="{{config('app.url')}}/wp-content/uploads/2015/08/05-image-with-caption-img8.jpg"
                                                 class="attachment-full"
                                                 alt=""
                                                 decoding="async"
                                                 loading="lazy"
                                                 srcset="{{config('app.url')}}/wp-content/uploads/2015/08/05-image-with-caption-img8.jpg 768w, {{config('app.url')}}/wp-content/uploads/2015/08/05-image-with-caption-img8-500x385.jpg 500w, {{config('app.url')}}/wp-content/uploads/2015/08/05-image-with-caption-img8-300x231.jpg 300w"
                                                 sizes="(max-width: 768px) 100vw, 768px"/>
                                        </div>
                                        <div class='dt-sc-image-content'>
                                            <div class='dt-sc-image-title'>
                                                <h3>{{__('site.advantages_title')}}</h3>
                                            </div>
                                            <p>{{__('site.advantages_description')}}</p>
                                            <p>
                                                <a href="javascript:void(0)" onclick="scrollToElem('contact-us')"
                                                   class='dt-sc-button   medium   bordered'>
                                                    {{__('site.contact_us')}}
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-with-bg skin-overlay wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill">
                            <div class="vc_column-inner vc_custom_1507266410979">
                                <div class="wpb_wrapper">
                                    <div class='dt-sc-title script-with-sub-title '>
                                        <h4>{{__('site.advantages_subtitle')}}</h4>
                                        <h2>{{__('site.advantages_excerpt')}}</h2></div>

                                    @foreach($advantages as $advantage)
                                        <div class="ult-spacer spacer-{{$advantage->id}}adv"
                                         data-id="{{$advantage->id}}adv" data-height="20"
                                         data-height-mobile="20" data-height-tab="20"
                                         data-height-tab-portrait="" data-height-mobile-landscape=""
                                         style="clear:both;display:block;"></div>
                                    <div class='dt-sc-icon-box type5'>
                                        <div class="icon-wrapper"><span
                                                    class='pe-icon {{$advantage->icon}}'> </span></div>
                                        <div class="icon-content"><h4>{{$advantage->title}}</h4>
                                            <p>{{$advantage->excerpt}}</p></div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
