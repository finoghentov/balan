<div class="wrapper" style="position:relative" id="our-success-block">
    <div class="bg"
         style="opacity:0.3;position:absolute;top:0;left:0;background: url('{{config('app.url')}}/wp-content/uploads/2017/09/img32.jpg'); width: 100%; height: 100%"></div>
    <div class="container">
        <div data-vc-full-width="true" data-vc-full-width-init="false"
             class="vc_row wpb_row vc_row-fluid vc_custom_1505912071786">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="rs_col-sm-6 rs_col-bottom-space wpb_column vc_column_container vc_col-sm-3">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class='dt-sc-counter type5  '>
                                            <div class='dt-sc-couter-icon-holder'>
                                                <div class='icon-wrapper'><span
                                                            class='icon icon-pointer'> </span></div>
                                                <div class='dt-sc-counter-number' id="counter-1">
                                                    {{__('site.counter_value_1')}}
                                                </div>
                                            </div>
                                            <h4>{{__('site.counter_text_1')}}</h4></div>
                                    </div>
                                </div>
                            </div>
                            <div class="rs_col-sm-6 rs_col-bottom-space wpb_column vc_column_container vc_col-sm-3">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class='dt-sc-counter type5  '>
                                            <div class='dt-sc-couter-icon-holder'>
                                                <div class='icon-wrapper'><span
                                                            class='icon icon-pointer'> </span></div>
                                                <div class='dt-sc-counter-number' id="counter-2">
                                                    {{__('site.counter_value_2')}}
                                                </div>
                                            </div>
                                            <h4>{{__('site.counter_text_2')}}</h4></div>
                                    </div>
                                </div>
                            </div>
                            <div class="rs_col-sm-6 wpb_column vc_column_container vc_col-sm-3">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class='dt-sc-counter type5  '>
                                            <div class='dt-sc-couter-icon-holder'>
                                                <div class='icon-wrapper'><span
                                                            class='icon icon-pointer'> </span></div>
                                                <div class='dt-sc-counter-number' id="counter-3">
                                                    {{__('site.counter_value_3')}}
                                                </div>
                                            </div>
                                            <h4>{{__('site.counter_text_3')}}</h4></div>
                                    </div>
                                </div>
                            </div>
                            <div class="rs_col-sm-6 wpb_column vc_column_container vc_col-sm-3">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class='dt-sc-counter type5  '>
                                            <div class='dt-sc-couter-icon-holder'>
                                                <div class='icon-wrapper'><span
                                                            class='icon icon-pointer'> </span></div>
                                                <div class='dt-sc-counter-number' id="counter-4">
                                                    {{__('site.counter_value_4')}}
                                                </div>
                                            </div>
                                            <h4>{{__('site.counter_text_4')}}</h4></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

