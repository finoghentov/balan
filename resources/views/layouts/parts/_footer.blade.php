<div class="dt-sc-skin-highlight vcr_after-content">
    <div class="container">
        <div class="column dt-sc-two-third first">
            <h4>{{__('site.footer_blue_title')}}</h4>
        </div>
        <div class="column dt-sc-one-third aligncenter">
            <a href="javascript:void(0)" onclick="scrollToElem('contact-us')" class="dt-sc-button medium filled">{{__('site.contact_us')}}</a>
        </div>
    </div>
</div>
<footer id="footer">
    <div class="footer-widgets ">
        <div class="container">
            <div class="column dt-sc-one-fourth first">
                <aside id="text-2" class="widget widget_text">
                    <div class="textwidget">
                        <p>
                            <img decoding="async" loading="lazy"
                                 src="{{ storage_path('web/' . settings('site', 'logo', '', '*')) }}"
                                 alt=""
                                width="120" height="54">
                            <br>
                            <p>
                            {{__('site.footer_description')}}
                        </p>
                    </div>
                </aside>
            </div>
            <div class="column dt-sc-one-half">
                <h3>{{__('site.footer_map_title')}}</h3>
               <div style="position:relative;overflow:hidden;"><a href="https://yandex.ru/maps/213/moscow/?utm_medium=mapframe&utm_source=maps" style="color:#eee;font-size:12px;position:absolute;top:0px;">Москва</a><a href="https://yandex.ru/maps/213/moscow/house/1_ya_ulitsa_bukhvostova_1/Z04YcQdpS0UGQFtvfXt4dnpqZA==/?ll=37.714054%2C55.796333&utm_medium=mapframe&utm_source=maps&z=14.52" style="color:#eee;font-size:12px;position:absolute;top:14px;">1-я улица Бухвостова, 1 — Яндекс Карты</a><iframe src="https://yandex.ru/map-widget/v1/?ll=37.714054%2C55.796333&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fdata%3DCgk3NzE5NTk4NDUSQ9Cg0L7RgdGB0LjRjywg0JzQvtGB0LrQstCwLCAxLdGPINGD0LvQuNGG0LAg0JHRg9GF0LLQvtGB0YLQvtCy0LAsIDEiCg041hZCFdYwX0I%3D&z=14.52" width="560" height="400" frameborder="1" allowfullscreen="true" style="position:relative;"></iframe></div>
            </div>
            <div class="column dt-sc-one-fourth ">
                <aside id="text-7" class="widget_text">
                    <h3 class="">{{__('site.footer_contact_title')}}</h3>
                    <div class="textwidget">
                        {!! settings('site', 'footer_contact_description', '', '*') !!}
                    </div>
                </aside>
            </div>
        </div>
    </div>
    <div class="footer-copyright ">
        <div class="container">
            <div class="column dt-sc-one-half first ">
                <p>
                    {{__('site.copyright')}}
                </p>
            </div>
            <div class="column dt-sc-one-half sociable">
                <ul class="dt-sc-sociable"></ul>
            </div>
        </div>
    </div>
</footer>
