    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <script>
        document.documentElement.className = document.documentElement.className + ' yes-js js_active js'
    </script>
    <title>CCTV &#8211; Just another WordPress site</title>
    <meta name='robots' content='max-image-preview:large' />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link href='https://fonts.gstatic.com' crossorigin rel='preconnect' />
    <link rel="alternate" type="application/rss+xml" title="CCTV &raquo; Feed" href="{{ config('app.url') }}/feed/" />
    <link rel="alternate" type="application/rss+xml" title="CCTV &raquo; Comments Feed" href="{{ config('app.url') }}/comments/feed/" />
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 0.07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='wp-block-library-css' href='{{ config('app.url') }}/wp-includes/css/dist/block-library/style.min.css' type='text/css' media='all' />
    <style id='wp-block-library-theme-inline-css' type='text/css'>
        .wp-block-audio figcaption {
            color: #555;
            font-size: 13px;
            text-align: center
        }

        .is-dark-theme .wp-block-audio figcaption {
            color: hsla(0, 0%, 100%, .65)
        }

        .wp-block-audio {
            margin: 0 0 1em
        }

        .wp-block-code {
            border: 1px solid #ccc;
            border-radius: 4px;
            font-family: Menlo, Consolas, monaco, monospace;
            padding: .8em 1em
        }

        .wp-block-embed figcaption {
            color: #555;
            font-size: 13px;
            text-align: center
        }

        .is-dark-theme .wp-block-embed figcaption {
            color: hsla(0, 0%, 100%, .65)
        }

        .wp-block-embed {
            margin: 0 0 1em
        }

        .blocks-gallery-caption {
            color: #555;
            font-size: 13px;
            text-align: center
        }

        .is-dark-theme .blocks-gallery-caption {
            color: hsla(0, 0%, 100%, .65)
        }

        .wp-block-image figcaption {
            color: #555;
            font-size: 13px;
            text-align: center
        }

        .is-dark-theme .wp-block-image figcaption {
            color: hsla(0, 0%, 100%, .65)
        }

        .wp-block-image {
            margin: 0 0 1em
        }

        .wp-block-pullquote {
            border-top: 4px solid;
            border-bottom: 4px solid;
            margin-bottom: 1.75em;
            color: currentColor
        }

        .wp-block-pullquote__citation,
        .wp-block-pullquote cite,
        .wp-block-pullquote footer {
            color: currentColor;
            text-transform: uppercase;
            font-size: .8125em;
            font-style: normal
        }

        .wp-block-quote {
            border-left: .25em solid;
            margin: 0 0 1.75em;
            padding-left: 1em
        }

        .wp-block-quote cite,
        .wp-block-quote footer {
            color: currentColor;
            font-size: .8125em;
            position: relative;
            font-style: normal
        }

        .wp-block-quote.has-text-align-right {
            border-left: none;
            border-right: .25em solid;
            padding-left: 0;
            padding-right: 1em
        }

        .wp-block-quote.has-text-align-center {
            border: none;
            padding-left: 0
        }

        .wp-block-quote.is-large,
        .wp-block-quote.is-style-large,
        .wp-block-quote.is-style-plain {
            border: none
        }

        .wp-block-search .wp-block-search__label {
            font-weight: 700
        }

        .wp-block-search__button {
            border: 1px solid #ccc;
            padding: .375em .625em
        }

        :where(.wp-block-group.has-background) {
            padding: 1.25em 2.375em
        }

        .wp-block-separator.has-css-opacity {
            opacity: .4
        }

        .wp-block-separator {
            border: none;
            border-bottom: 2px solid;
            margin-left: auto;
            margin-right: auto
        }

        .wp-block-separator.has-alpha-channel-opacity {
            opacity: 1
        }

        .wp-block-separator:not(.is-style-wide):not(.is-style-dots) {
            width: 100px
        }

        .wp-block-separator.has-background:not(.is-style-dots) {
            border-bottom: none;
            height: 1px
        }

        .wp-block-separator.has-background:not(.is-style-wide):not(.is-style-dots) {
            height: 2px
        }

        .wp-block-table {
            margin: "0 0 1em 0"
        }

        .wp-block-table thead {
            border-bottom: 3px solid
        }

        .wp-block-table tfoot {
            border-top: 3px solid
        }

        .wp-block-table td,
        .wp-block-table th {
            word-break: normal
        }

        .wp-block-table figcaption {
            color: #555;
            font-size: 13px;
            text-align: center
        }

        .is-dark-theme .wp-block-table figcaption {
            color: hsla(0, 0%, 100%, .65)
        }

        .wp-block-video figcaption {
            color: #555;
            font-size: 13px;
            text-align: center
        }

        .is-dark-theme .wp-block-video figcaption {
            color: hsla(0, 0%, 100%, .65)
        }

        .wp-block-video {
            margin: 0 0 1em
        }

        .wp-block-template-part.has-background {
            padding: 1.25em 2.375em;
            margin-top: 0;
            margin-bottom: 0
        }
    </style>
    <link rel='stylesheet' id='wc-block-vendors-style-css' href='{{ config('app.url') }}/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/vendors-style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wc-block-style-css' href='{{ config('app.url') }}/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='jquery-selectBox-css' href='{{ config('app.url') }}/wp-content/plugins/yith-woocommerce-wishlist/assets/css/jquery.selectBox.css' type='text/css' media='all' />
    <link rel='stylesheet' id='yith-wcwl-font-awesome-css' href='{{ config('app.url') }}/wp-content/plugins/yith-woocommerce-wishlist/assets/css/font-awesome.css' type='text/css' media='all' />
    <link rel='stylesheet' id='yith-wcwl-main-css' href='{{ config('app.url') }}/wp-content/plugins/yith-woocommerce-wishlist/assets/css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='classic-theme-styles-css' href='{{ config('app.url') }}/wp-includes/css/classic-themes.min.css' type='text/css' media='all' />
    <style id='global-styles-inline-css' type='text/css'>
        .gray-bg {
            background-color: #f0f3f6;
        }

        body {
            --wp--preset--color--black: #000000;
            --wp--preset--color--cyan-bluish-gray: #abb8c3;
            --wp--preset--color--white: #ffffff;
            --wp--preset--color--pale-pink: #f78da7;
            --wp--preset--color--vivid-red: #cf2e2e;
            --wp--preset--color--luminous-vivid-orange: #ff6900;
            --wp--preset--color--luminous-vivid-amber: #fcb900;
            --wp--preset--color--light-green-cyan: #7bdcb5;
            --wp--preset--color--vivid-green-cyan: #00d084;
            --wp--preset--color--pale-cyan-blue: #8ed1fc;
            --wp--preset--color--vivid-cyan-blue: #0693e3;
            --wp--preset--color--vivid-purple: #9b51e0;
            --wp--preset--color--primary: #108bea;
            --wp--preset--color--secondary: #58aff2;
            --wp--preset--color--tertiary: #0561aa;
            --wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg, rgba(6, 147, 227, 1) 0%, rgb(155, 81, 224) 100%);
            --wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg, rgb(122, 220, 180) 0%, rgb(0, 208, 130) 100%);
            --wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg, rgba(252, 185, 0, 1) 0%, rgba(255, 105, 0, 1) 100%);
            --wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg, rgba(255, 105, 0, 1) 0%, rgb(207, 46, 46) 100%);
            --wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg, rgb(238, 238, 238) 0%, rgb(169, 184, 195) 100%);
            --wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg, rgb(74, 234, 220) 0%, rgb(151, 120, 209) 20%, rgb(207, 42, 186) 40%, rgb(238, 44, 130) 60%, rgb(251, 105, 98) 80%, rgb(254, 248, 76) 100%);
            --wp--preset--gradient--blush-light-purple: linear-gradient(135deg, rgb(255, 206, 236) 0%, rgb(152, 150, 240) 100%);
            --wp--preset--gradient--blush-bordeaux: linear-gradient(135deg, rgb(254, 205, 165) 0%, rgb(254, 45, 45) 50%, rgb(107, 0, 62) 100%);
            --wp--preset--gradient--luminous-dusk: linear-gradient(135deg, rgb(255, 203, 112) 0%, rgb(199, 81, 192) 50%, rgb(65, 88, 208) 100%);
            --wp--preset--gradient--pale-ocean: linear-gradient(135deg, rgb(255, 245, 203) 0%, rgb(182, 227, 212) 50%, rgb(51, 167, 181) 100%);
            --wp--preset--gradient--electric-grass: linear-gradient(135deg, rgb(202, 248, 128) 0%, rgb(113, 206, 126) 100%);
            --wp--preset--gradient--midnight: linear-gradient(135deg, rgb(2, 3, 129) 0%, rgb(40, 116, 252) 100%);
            --wp--preset--duotone--dark-grayscale: url('#wp-duotone-dark-grayscale');
            --wp--preset--duotone--grayscale: url('#wp-duotone-grayscale');
            --wp--preset--duotone--purple-yellow: url('#wp-duotone-purple-yellow');
            --wp--preset--duotone--blue-red: url('#wp-duotone-blue-red');
            --wp--preset--duotone--midnight: url('#wp-duotone-midnight');
            --wp--preset--duotone--magenta-yellow: url('#wp-duotone-magenta-yellow');
            --wp--preset--duotone--purple-green: url('#wp-duotone-purple-green');
            --wp--preset--duotone--blue-orange: url('#wp-duotone-blue-orange');
            --wp--preset--font-size--small: 13px;
            --wp--preset--font-size--medium: 20px;
            --wp--preset--font-size--large: 36px;
            --wp--preset--font-size--x-large: 42px;
            --wp--preset--spacing--20: 0.44rem;
            --wp--preset--spacing--30: 0.67rem;
            --wp--preset--spacing--40: 1rem;
            --wp--preset--spacing--50: 1.5rem;
            --wp--preset--spacing--60: 2.25rem;
            --wp--preset--spacing--70: 3.38rem;
            --wp--preset--spacing--80: 5.06rem;
        }

        :where(.is-layout-flex) {
            gap: 0.5em;
        }

        body .is-layout-flow>.alignleft {
            float: left;
            margin-inline-start: 0;
            margin-inline-end: 2em;
        }

        body .is-layout-flow>.alignright {
            float: right;
            margin-inline-start: 2em;
            margin-inline-end: 0;
        }

        body .is-layout-flow>.aligncenter {
            margin-left: auto !important;
            margin-right: auto !important;
        }

        body .is-layout-constrained>.alignleft {
            float: left;
            margin-inline-start: 0;
            margin-inline-end: 2em;
        }

        body .is-layout-constrained>.alignright {
            float: right;
            margin-inline-start: 2em;
            margin-inline-end: 0;
        }

        body .is-layout-constrained>.aligncenter {
            margin-left: auto !important;
            margin-right: auto !important;
        }

        body .is-layout-constrained> :where(:not(.alignleft):not(.alignright):not(.alignfull)) {
            max-width: var(--wp--style--global--content-size);
            margin-left: auto !important;
            margin-right: auto !important;
        }

        body .is-layout-constrained>.alignwide {
            max-width: var(--wp--style--global--wide-size);
        }

        body .is-layout-flex {
            display: flex;
        }

        body .is-layout-flex {
            flex-wrap: wrap;
            align-items: center;
        }

        body .is-layout-flex>* {
            margin: 0;
        }

        :where(.wp-block-columns.is-layout-flex) {
            gap: 2em;
        }

        .has-black-color {
            color: var(--wp--preset--color--black) !important;
        }

        .has-cyan-bluish-gray-color {
            color: var(--wp--preset--color--cyan-bluish-gray) !important;
        }

        .has-white-color {
            color: var(--wp--preset--color--white) !important;
        }

        .has-pale-pink-color {
            color: var(--wp--preset--color--pale-pink) !important;
        }

        .has-vivid-red-color {
            color: var(--wp--preset--color--vivid-red) !important;
        }

        .has-luminous-vivid-orange-color {
            color: var(--wp--preset--color--luminous-vivid-orange) !important;
        }

        .has-luminous-vivid-amber-color {
            color: var(--wp--preset--color--luminous-vivid-amber) !important;
        }

        .has-light-green-cyan-color {
            color: var(--wp--preset--color--light-green-cyan) !important;
        }

        .has-vivid-green-cyan-color {
            color: var(--wp--preset--color--vivid-green-cyan) !important;
        }

        .has-pale-cyan-blue-color {
            color: var(--wp--preset--color--pale-cyan-blue) !important;
        }

        .has-vivid-cyan-blue-color {
            color: var(--wp--preset--color--vivid-cyan-blue) !important;
        }

        .has-vivid-purple-color {
            color: var(--wp--preset--color--vivid-purple) !important;
        }

        .has-black-background-color {
            background-color: var(--wp--preset--color--black) !important;
        }

        .has-cyan-bluish-gray-background-color {
            background-color: var(--wp--preset--color--cyan-bluish-gray) !important;
        }

        .has-white-background-color {
            background-color: var(--wp--preset--color--white) !important;
        }

        .has-pale-pink-background-color {
            background-color: var(--wp--preset--color--pale-pink) !important;
        }

        .has-vivid-red-background-color {
            background-color: var(--wp--preset--color--vivid-red) !important;
        }

        .has-luminous-vivid-orange-background-color {
            background-color: var(--wp--preset--color--luminous-vivid-orange) !important;
        }

        .has-luminous-vivid-amber-background-color {
            background-color: var(--wp--preset--color--luminous-vivid-amber) !important;
        }

        .has-light-green-cyan-background-color {
            background-color: var(--wp--preset--color--light-green-cyan) !important;
        }

        .has-vivid-green-cyan-background-color {
            background-color: var(--wp--preset--color--vivid-green-cyan) !important;
        }

        .has-pale-cyan-blue-background-color {
            background-color: var(--wp--preset--color--pale-cyan-blue) !important;
        }

        .has-vivid-cyan-blue-background-color {
            background-color: var(--wp--preset--color--vivid-cyan-blue) !important;
        }

        .has-vivid-purple-background-color {
            background-color: var(--wp--preset--color--vivid-purple) !important;
        }

        .has-black-border-color {
            border-color: var(--wp--preset--color--black) !important;
        }

        .has-cyan-bluish-gray-border-color {
            border-color: var(--wp--preset--color--cyan-bluish-gray) !important;
        }

        .has-white-border-color {
            border-color: var(--wp--preset--color--white) !important;
        }

        .has-pale-pink-border-color {
            border-color: var(--wp--preset--color--pale-pink) !important;
        }

        .has-vivid-red-border-color {
            border-color: var(--wp--preset--color--vivid-red) !important;
        }

        .has-luminous-vivid-orange-border-color {
            border-color: var(--wp--preset--color--luminous-vivid-orange) !important;
        }

        .has-luminous-vivid-amber-border-color {
            border-color: var(--wp--preset--color--luminous-vivid-amber) !important;
        }

        .has-light-green-cyan-border-color {
            border-color: var(--wp--preset--color--light-green-cyan) !important;
        }

        .has-vivid-green-cyan-border-color {
            border-color: var(--wp--preset--color--vivid-green-cyan) !important;
        }

        .has-pale-cyan-blue-border-color {
            border-color: var(--wp--preset--color--pale-cyan-blue) !important;
        }

        .has-vivid-cyan-blue-border-color {
            border-color: var(--wp--preset--color--vivid-cyan-blue) !important;
        }

        .has-vivid-purple-border-color {
            border-color: var(--wp--preset--color--vivid-purple) !important;
        }

        .has-vivid-cyan-blue-to-vivid-purple-gradient-background {
            background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;
        }

        .has-light-green-cyan-to-vivid-green-cyan-gradient-background {
            background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;
        }

        .has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background {
            background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;
        }

        .has-luminous-vivid-orange-to-vivid-red-gradient-background {
            background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;
        }

        .has-very-light-gray-to-cyan-bluish-gray-gradient-background {
            background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;
        }

        .has-cool-to-warm-spectrum-gradient-background {
            background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;
        }

        .has-blush-light-purple-gradient-background {
            background: var(--wp--preset--gradient--blush-light-purple) !important;
        }

        .has-blush-bordeaux-gradient-background {
            background: var(--wp--preset--gradient--blush-bordeaux) !important;
        }

        .has-luminous-dusk-gradient-background {
            background: var(--wp--preset--gradient--luminous-dusk) !important;
        }

        .has-pale-ocean-gradient-background {
            background: var(--wp--preset--gradient--pale-ocean) !important;
        }

        .has-electric-grass-gradient-background {
            background: var(--wp--preset--gradient--electric-grass) !important;
        }

        .has-midnight-gradient-background {
            background: var(--wp--preset--gradient--midnight) !important;
        }

        .has-small-font-size {
            font-size: var(--wp--preset--font-size--small) !important;
        }

        .has-medium-font-size {
            font-size: var(--wp--preset--font-size--medium) !important;
        }

        .has-large-font-size {
            font-size: var(--wp--preset--font-size--large) !important;
        }

        .has-x-large-font-size {
            font-size: var(--wp--preset--font-size--x-large) !important;
        }

        .wp-block-navigation a:where(:not(.wp-element-button)) {
            color: inherit;
        }

        :where(.wp-block-columns.is-layout-flex) {
            gap: 2em;
        }

        .wp-block-pullquote {
            font-size: 1.5em;
            line-height: 1.6;
        }
    </style>
    <link rel='stylesheet' id='contact-form-7-css' href='{{ config('app.url') }}/wp-content/plugins/contact-form-7/includes/css/styles.css' type='text/css' media='all' />
    <link rel='stylesheet' id='dt-animation-css-css' href='{{ config('app.url') }}/wp-content/plugins/designthemes-core-features/shortcodes/css/animations.css' type='text/css' media='all' />
    <link rel='stylesheet' id='dt-slick-css-css' href='{{ config('app.url') }}/wp-content/plugins/designthemes-core-features/shortcodes/css/slick.css' type='text/css' media='all' />
    <link rel='stylesheet' id='dt-sc-css-css' href='{{ config('app.url') }}/wp-content/plugins/designthemes-core-features/shortcodes/css/shortcodes.css' type='text/css' media='all' />
    <link rel='stylesheet' id='rs-plugin-settings-css' href='{{ config('app.url') }}/wp-content/plugins/revslider/public/assets/css/rs6.css' type='text/css' media='all' />
    <style id='rs-plugin-settings-inline-css' type='text/css'>
        #rs-demo-id {}
    </style>
    <style id='woocommerce-inline-inline-css' type='text/css'>
        .woocommerce form .form-row .required {
            visibility: visible;
        }
    </style>
    <link rel='stylesheet' id='wpsl-styles-css' href='{{ config('app.url') }}/wp-content/plugins/wp-store-locator/css/styles.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='woocommerce_prettyPhoto_css-css' href='{{ config('app.url') }}/wp-content/plugins/woocommerce/assets/css/prettyPhoto.css' type='text/css' media='all' />
    <link rel='stylesheet' id='js_composer_front-css' href='{{ config('app.url') }}/wp-content/plugins/js_composer/assets/css/js_composer.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='bsf-Defaults-css' href='{{ config('app.url') }}/wp-content/uploads/smile_fonts/Defaults/Defaults.css' type='text/css' media='all' />
    <link rel='stylesheet' id='ultimate-style-css' href='{{ config('app.url') }}/wp-content/plugins/Ultimate_VC_Addons/assets/min-css/style.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='ult-slick-css' href='{{ config('app.url') }}/wp-content/plugins/Ultimate_VC_Addons/assets/min-css/slick.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='ult-icons-css' href='{{ config('app.url') }}/wp-content/plugins/Ultimate_VC_Addons/assets/css/icons.css' type='text/css' media='all' />
    <link rel='stylesheet' id='ultimate-animate-css' href='{{ config('app.url') }}/wp-content/plugins/Ultimate_VC_Addons/assets/min-css/animate.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='ult-ib2-style-css' href='{{ config('app.url') }}/wp-content/plugins/Ultimate_VC_Addons/assets/min-css/ib2-style.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-css' href='{{ config('app.url') }}/wp-content/themes/vigil/style.css' type='text/css' media='all' />
    <style id='vigil-inline-css' type='text/css'>
        .dt-sc-menu-sorting a {
            color: rgba(16, 139, 234, 0.6)
        }

        .dt-sc-team.type2 .dt-sc-team-thumb .dt-sc-team-thumb-overlay,
        .dt-sc-hexagon-image span:before,
        .dt-sc-keynote-speakers .dt-sc-speakers-thumb .dt-sc-speakers-thumb-overlay {
            background: rgba(16, 139, 234, 0.9)
        }

        .portfolio .image-overlay,
        .recent-portfolio-widget ul li a:before,
        .dt-sc-image-caption.type2:hover .dt-sc-image-content,
        .dt-sc-fitness-program-short-details-wrapper .dt-sc-fitness-program-short-details {
            background: rgba(16, 139, 234, 0.9)
        }

        .dt-sc-icon-box.type10 .icon-wrapper:before,
        .dt-sc-contact-info.type4 span:after,
        .dt-sc-pr-tb-col.type2 .dt-sc-tb-header:before {
            box-shadow: 5px 0px 0px 0px #108bea
        }

        .dt-sc-icon-box.type10:hover .icon-wrapper:before {
            box-shadow: 7px 0px 0px 0px #108bea
        }

        .dt-sc-counter.type6 .dt-sc-couter-icon-holder:before {
            box-shadow: 5px 1px 0px 0px #108bea
        }

        .dt-sc-button.with-shadow.white,
        .dt-sc-pr-tb-col.type2 .dt-sc-buy-now a {
            box-shadow: 3px 3px 0px 0px #108bea
        }

        .dt-sc-restaurant-events-list .dt-sc-restaurant-event-details h6:before {
            border-bottom-color: rgba(16, 139, 234, 0.6)
        }

        .portfolio.type4 .image-overlay,
        .dt-sc-timeline-section.type4 .dt-sc-timeline-thumb-overlay,
        .dt-sc-yoga-classes .dt-sc-yoga-classes-image-wrapper:before,
        .dt-sc-yoga-course .dt-sc-yoga-course-thumb-overlay,
        .dt-sc-yoga-program .dt-sc-yoga-program-thumb-overlay,
        .dt-sc-yoga-pose .dt-sc-yoga-pose-thumb:before,
        .dt-sc-yoga-teacher .dt-sc-yoga-teacher-thumb:before,
        .dt-sc-doctors .dt-sc-doctors-thumb-overlay,
        .dt-sc-event-addon>.dt-sc-event-addon-date,
        .dt-sc-course .dt-sc-course-overlay,
        .dt-sc-process-steps .dt-sc-process-thumb-overlay {
            background: rgba(16, 139, 234, 0.85)
        }

        .woo-type9.woocommerce ul.products li.product .product-wrapper,
        .woo-type9 .woocommerce ul.products li.product .product-wrapper {
            border-color: rgba(16, 139, 234, 0.6)
        }

        .woo-type1 .woocommerce ul.products li.product .star-rating:before,
        .woo-type1 .woocommerce ul.products li.product .star-rating span:before,
        .woo-type1.woocommerce ul.products li.product .star-rating:before,
        .woo-type1.woocommerce ul.products li.product .star-rating span:before,
        .woo-type1.woocommerce .star-rating:before,
        .woo-type1.woocommerce .star-rating span:before,
        .woo-type1 .woocommerce .star-rating:before,
        .woo-type1 .woocommerce .star-rating span:before,
        .woo-type2 ul.products li.product .product-details h5 a:hover,
        .woo-type2 ul.products li.product-category:hover .product-details h5,
        .woo-type2 ul.products li.product-category:hover .product-details h5 .count {
            color: rgba(16, 139, 234, 0.75)
        }

        .woo-type2 ul.products li.product .product-thumb a.add_to_cart_button:hover,
        .woo-type2 ul.products li.product .product-thumb a.button.product_type_simple:hover,
        .woo-type2 ul.products li.product .product-thumb a.button.product_type_variable:hover,
        .woo-type2 ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover,
        .woo-type2 ul.products li.product .product-thumb a.add_to_wishlist:hover,
        .woo-type2 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover,
        .woo-type2 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover,
        .woo-type6.woocommerce ul.products li.product:hover .product-content,
        .woo-type6 .woocommerce ul.products li.product:hover .product-content,
        .woo-type6.woocommerce ul.products li.product.instock:hover .on-sale-product .product-content,
        .woo-type6 .woocommerce ul.products li.product.instock:hover .on-sale-product .product-content,
        .woo-type6.woocommerce ul.products li.product.outofstock:hover .out-of-stock-product .product-content,
        .woo-type6 .woocommerce ul.products li.product.outofstock:hover .out-of-stock-product .product-content,
        .woo-type6.woocommerce ul.products li.product-category:hover .product-thumb .image:after,
        .woo-type6 .woocommerce ul.products li.product-category:hover .product-thumb .image:after,
        .woo-type8.woocommerce ul.products li.product:hover .product-content,
        .woo-type8.woocommerce ul.products li.product-category:hover .product-thumb .image:after,
        .woo-type8 .woocommerce ul.products li.product:hover .product-content,
        .woo-type8 .woocommerce ul.products li.product-category:hover .product-thumb .image:after,
        .woo-type13.woocommerce ul.products li.product:hover .product-content,
        .woo-type13 .woocommerce ul.products li.product:hover .product-content,
        .woo-type13.woocommerce ul.products li.product.instock:hover .on-sale-product .product-content,
        .woo-type13 .woocommerce ul.products li.product.instock:hover .on-sale-product .product-content,
        .woo-type13.woocommerce ul.products li.product.outofstock:hover .out-of-stock-product .product-content,
        .woo-type13 .woocommerce ul.products li.product.outofstock:hover .out-of-stock-product .product-content,
        .woo-type13.woocommerce ul.products li.product-category:hover .product-thumb .image:after,
        .woo-type13 .woocommerce ul.products li.product-category:hover .product-thumb .image:after {
            background-color: rgba(16, 139, 234, 0.75)
        }

        .woo-type8.woocommerce ul.products li.product:hover .product-content:after,
        .woo-type8 .woocommerce ul.products li.product:hover .product-content:after {
            border-color: rgba( 16, 139, 234, 0.75) rgba(16, 139, 234, 0.75) rgba(255, 255, 255, 0.35) rgba(255, 255, 255, 0.35)
        }

        .woo-type11 ul.products li.product:hover .product-wrapper {
            -webkit-box-shadow: 0 0 0 3px #108bea;
            -moz-box-shadow: 0 0 0 3px #108bea;
            -ms-box-shadow: 0 0 0 3px #108bea;
            -o-box-shadow: 0 0 0 3px #108bea;
            box-shadow: 0 0 0 3px #108bea;
        }

        .woo-type12 ul.products li.product .product-details {
            -webkit-box-shadow: 0 -3px 0 0 #108bea inset;
            -moz-box-shadow: 0 -3px 0 0 #108bea inset;
            -ms-box-shadow: 0 -3px 0 0 #108bea inset;
            -o-box-shadow: 0 -3px 0 0 #108bea inset;
            box-shadow: 0 -3px 0 0 #108bea inset;
        }

        .woo-type14 ul.products li.product .product-details,
        .woo-type14 ul.products li.product .product-details h5:after {
            -webkit-box-shadow: 0 0 0 2px #108bea inset;
            -moz-box-shadow: 0 0 0 2px #108bea inset;
            -ms-box-shadow: 0 0 0 2px #108bea inset;
            -o-box-shadow: 0 0 0 2px #108bea inset;
            box-shadow: 0 0 0 2px #108bea inset;
        }

        .woo-type15 ul.products li.product .product-thumb a.add_to_cart_button:after,
        .woo-type15 ul.products li.product .product-thumb a.button.product_type_simple:after,
        .woo-type15 ul.products li.product .product-thumb a.button.product_type_variable:after,
        .woo-type15 ul.products li.product .product-thumb a.added_to_cart.wc-forward:after,
        .woo-type15 ul.products li.product .product-thumb a.add_to_wishlist:after,
        .woo-type15 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:after,
        .woo-type15 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:after {
            -webkit-box-shadow: 0 0 0 2px #108bea;
            -moz-box-shadow: 0 0 0 2px #108bea;
            -ms-box-shadow: 0 0 0 2px #108bea;
            -o-box-shadow: 0 0 0 2px #108bea;
            box-shadow: 0 0 0 2px #108bea;
        }

        @media only screen and (max-width: 767px) {
            .dt-sc-contact-info.type4:after,
            .dt-sc-icon-box.type10 .icon-content h4:after,
            .dt-sc-counter.type6.last h4::before,
            .dt-sc-counter.type6 h4::after {
                background-color: #108bea
            }
        }

        @media only screen and (max-width: 767px) {
            .dt-sc-timeline-section.type2,
            .dt-sc-timeline-section.type2::before {
                border-color: #108bea
            }
        }

        .dt-sc-event-month-thumb .dt-sc-event-read-more,
        .dt-sc-training-thumb-overlay {
            background: rgba(88, 175, 242, 0.85)
        }

        @media only screen and (max-width: 767px) {
            .dt-sc-highlight .dt-sc-testimonial.type6 .dt-sc-testimonial-author:after,
            .dt-sc-highlight .dt-sc-testimonial.type6 .dt-sc-testimonial-author:after,
            .skin-highlight .dt-sc-testimonial.type6 .dt-sc-testimonial-author:after {
                background-color: #58aff2
            }
        }

        .woo-type8 ul.products li.product:hover .product-details h5:after {
            border-color: rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) #58aff2 rgba(0, 0, 0, 0);
        }

        .woo-type9 ul.products li.product:hover .product-wrapper {
            border-color: rgba(88, 175, 242, 0.75)
        }

        .woo-type20 ul.products li.product .product-thumb a.add_to_cart_button:hover,
        .woo-type20 ul.products li.product .product-thumb a.button.product_type_simple:hover,
        .woo-type20 ul.products li.product .product-thumb a.button.product_type_variable:hover,
        .woo-type20 ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover,
        .woo-type20 ul.products li.product .product-thumb a.add_to_wishlist:hover,
        .woo-type20 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover,
        .woo-type20 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover,
        .woo-type20 ul.products li.product:hover .product-wrapper {
            background-color: rgba(88, 175, 242, 0.5)
        }

        .woo-type7 ul.products li.product:hover .product-details {
            -webkit-box-shadow: 0 -3px 0 0 #58aff2 inset;
            -moz-box-shadow: 0 -3px 0 0 #58aff2 inset;
            -ms-box-shadow: 0 -3px 0 0 #58aff2 inset;
            -o-box-shadow: 0 -3px 0 0 #58aff2 inset;
            box-shadow: 0 -3px 0 0 #58aff2 inset;
        }

        .woo-type19 ul.products li.product:hover .product-thumb .image {
            -webkit-box-shadow: 0 0 1px 4px #58aff2;
            -moz-box-shadow: 0 0 1px 4px #58aff2;
            -ms-box-shadow: 0 0 1px 4px #58aff2;
            -o-box-shadow: 0 0 1px 4px #58aff2;
            box-shadow: 0 0 1px 4px #58aff2;
        }

        .dt-sc-faculty .dt-sc-faculty-thumb-overlay {
            background: rgba(5, 97, 170, 0.9)
        }

        .woo-type18 ul.products li.product:hover .product-content,
        .woo-type18 ul.products li.product.instock:hover .on-sale-product .product-content,
        .woo-type18 ul.products li.product.outofstock:hover .out-of-stock-product .product-content,
        .woo-type18.woocommerce ul.products li.product:hover .product-content,
        .woo-type18.woocommerce ul.products li.product.instock:hover .on-sale-product .product-content,
        .woo-type18.woocommerce ul.products li.product.outofstock:hover .out-of-stock-product .product-content,
        .woo-type18.woocommerce-page ul.products li.product:hover .product-content,
        .woo-type18.woocommerce-page ul.products li.product.instock:hover .on-sale-product .product-content,
        .woo-type18.woocommerce-page ul.products li.product.outofstock:hover .out-of-stock-product .product-content {
            background-color: rgba(5, 97, 170, 0.6)
        }

        .woo-type19.woocommerce ul.products li.product:hover .product-content,
        .woo-type19 .woocommerce ul.products li.product:hover .product-content,
        .woo-type19.woocommerce ul.products li.product.instock:hover .on-sale-product .product-content,
        .woo-type19 .woocommerce ul.products li.product.instock:hover .on-sale-product .product-content,
        .woo-type19.woocommerce ul.products li.product.outofstock:hover .out-of-stock-product .product-content,
        .woo-type19 .woocommerce ul.products li.product.outofstock:hover .out-of-stock-product .product-content,
        .woo-type20.woocommerce ul.products li.product:hover .product-content,
        .woo-type20 .woocommerce ul.products li.product:hover .product-content,
        .woo-type20.woocommerce ul.products li.product.instock:hover .on-sale-product .product-content,
        .woo-type20 .woocommerce ul.products li.product.instock:hover .on-sale-product .product-content,
        .woo-type20.woocommerce ul.products li.product.outofstock:hover .out-of-stock-product .product-content,
        .woo-type20 .woocommerce ul.products li.product.outofstock:hover .out-of-stock-product .product-content {
            background-color: rgba(5, 97, 170, 0.4)
        }

        .woo-type1 ul.products li.product:hover .product-thumb:after {
            -webkit-box-shadow: 0 0 0 10px rgba(5, 97, 170, 0.35) inset;
            -moz-box-shadow: 0 0 0 10px rgba(5, 97, 170, 0.35) inset;
            -ms-box-shadow: 0 0 0 10px rgba(5, 97, 170, 0.35) inset;
            -o-box-shadow: 0 0 0 10px rgba(5, 97, 170, 0.35) inset;
            box-shadow: 0 0 0 10px rgba(5, 97, 170, 0.35) inset;
        }

        .woo-type7 ul.products li.product .product-details {
            -webkit-box-shadow: 0 -2px 0 0 #0561aa;
            -moz-box-shadow: 0 -2px 0 0 #0561aa;
            -ms-box-shadow: 0 -2px 0 0 #0561aa;
            -o-box-shadow: 0 -2px 0 0 #0561aa;
            box-shadow: 0 -2px 0 0 #0561aa;
        }

        .woo-type20 ul.products li.product .product-wrapper {
            -webkit-box-shadow: 0 0 0 5px rgba(5, 97, 170, 0.75) inset;
            -moz-box-shadow: 0 0 0 5px rgba(5, 97, 170, 0.75) inset;
            -ms-box-shadow: 0 0 0 5px rgba(5, 97, 170, 0.75) inset;
            -o-box-shadow: 0 0 0 5px rgba(5, 97, 170, 0.75) inset;
            box-shadow: 0 0 0 5px rgba(5, 97, 170, 0.75) inset;
        }
    </style>
    <link rel='stylesheet' id='vigil-base-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/base.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-grid-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/grid.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-widget-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/widget.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-layout-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/layout.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-blog-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/blog.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-portfolio-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/portfolio.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-contact-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/contact.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-custom-class-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/custom-class.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-browsers-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/browsers.css' type='text/css' media='all' />
    <link rel='stylesheet' id='prettyphoto-css' href='{{ config('app.url') }}/wp-content/plugins/js_composer/assets/lib/prettyphoto/css/prettyPhoto.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-gutenberg-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/gutenberg.css' type='text/css' media='all' />
    <link rel='stylesheet' id='custom-font-awesome-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='pe-icon-7-stroke-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/pe-icon-7-stroke.css' type='text/css' media='all' />
    <link rel='stylesheet' id='stroke-gap-icons-style-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/stroke-gap-icons-style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='icon-moon-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/icon-moon.css' type='text/css' media='all' />
    <link rel='stylesheet' id='material-design-iconic-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/material-design-iconic-font.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type-default-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/woocommerce-default.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type1-fashion-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type1-fashion.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type2-jewel-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type2-jewel.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type3-business-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type3-business.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type4-hosting-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type4-hosting.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type5-spa-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type5-spa.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type6-wedding-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type6-wedding.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type7-university-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type7-university.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type8-insurance-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type8-insurance.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type9-plumber-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type9-plumber.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type10-medical-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type10-medical.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type11-model-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type11-model.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type12-attorney-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type12-attorney.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type13-architecture-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type13-architecture.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type14-fitness-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type14-fitness.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type15-hotel-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type15-hotel.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type16-photography-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type16-photography.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type17-restaurant-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type17-restaurant.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type18-event-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type18-event.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type19-nightclub-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type19-nightclub.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type20-yoga-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type20-yoga.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-woo-type21-styleshop-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/woocommerce/type21-styleshop.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-customevent-css' href='{{ config('app.url') }}/wp-content/themes/vigil/tribe-events/custom.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-popup-css-css' href='{{ config('app.url') }}/wp-content/themes/vigil/framework/js/magnific/magnific-popup.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vigil-custom-css' href='{{ config('app.url') }}/wp-content/themes/vigil/css/custom.css' type='text/css' media='all' />

    <link rel="https://api.w.org/" href="{{ config('app.url') }}/wp-json/" />
    <link rel="alternate" type="application/json" href="{{ config('app.url') }}/wp-json/wp/v2/pages/9275" />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="{{ config('app.url') }}/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="{{ config('app.url') }}/wp-includes/wlwmanifest.xml" />
    <link rel="canonical" href="{{ config('app.url') }}/" />
    <link rel='shortlink' href='{{ config('app.url') }}/' />
    <link rel="alternate" type="application/json+oembed" href="{{ config('app.url') }}/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fvigil.wpengine.com%2F" />
    <link rel="alternate" type="text/xml+oembed" href="{{ config('app.url') }}/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fvigil.wpengine.com%2F&#038;format=xml" />
    <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." />
    <meta name="generator" content="Powered by Slider Revolution 6.3.4 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
    <link rel="icon" href="{{ config('app.url') }}/wp-content/uploads/2017/09/cropped-favicon-2-32x32.png" sizes="32x32" />
    <link rel="icon" href="{{ config('app.url') }}/wp-content/uploads/2017/09/cropped-favicon-2-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon" href="{{ config('app.url') }}/wp-content/uploads/2017/09/cropped-favicon-2-180x180.png" />
    <meta name="msapplication-TileImage" content="{{ config('app.url') }}/wp-content/uploads/2017/09/cropped-favicon-2-270x270.png" />
    <style id="kirki-inline-styles">
        .has-primary-background-color {
            background-color: #108bea;
        }

        .has-primary-color {
            color: #108bea;
        }

        a,
        h1 a:hover,
        h2 a:hover,
        h3 a:hover,
        h4 a:hover,
        h5 a:hover,
        h6 a:hover,
        #footer a:hover {
            color: #108bea;
        }

        .top-bar a:hover,
        .menu-icons-wrapper .search a:hover,
        .menu-icons-wrapper .cart a:hover,
        .main-header .menu-icons-wrapper .overlay-search #searchform:before,
        .breadcrumb a:hover {
            color: #108bea;
        }

        #main-menu ul.menu li a:hover,
        #main-menu ul.menu li.current_page_item>a,
        #main-menu ul.menu li.current_page_parent>a,
        #main-menu>ul.menu>li.current_page_item>a,
        #main-menu>ul.menu>li.current_page_ancestor>a,
        #main-menu>ul.menu>li.current-menu-item>a,
        #main-menu ul.menu>li.current-menu-ancestor>a,
        #main-menu ul.menu li.menu-item-simple-parent ul>li.current_page_item>a,
        #main-menu ul.menu li.menu-item-simple-parent ul>li.current_page_ancestor>a,
        #main-menu ul.menu li.menu-item-simple-parent ul>li.current-menu-item>a,
        #main-menu ul.menu li.menu-item-simple-parent ul>li.current-menu-ancestor>a,
        #main-menu ul.menu li.menu-item-simple-parent ul li a:hover,
        #main-menu ul.menu>li.menu-item-simple-parent:hover>a,
        #main-menu ul.menu li.menu-item-megamenu-parent:hover>a,
        #main-menu ul.menu li.menu-item-simple-parent ul li:hover>a,
        #main-menu .megamenu-child-container ul.sub-menu>li>ul li a:hover,
        #main-menu .megamenu-child-container ul.sub-menu>li.current_page_item>a,
        #main-menu .megamenu-child-container ul.sub-menu>li.current_page_ancestor>a,
        #main-menu .megamenu-child-container ul.sub-menu>li.current-menu-item>a,
        #main-menu .megamenu-child-container ul.sub-menu>li.current-menu-ancestor>a,
        #main-menu .megamenu-child-container ul.sub-menu>li.current_page_item>span,
        #main-menu .megamenu-child-container ul.sub-menu>li.current_page_ancestor>span,
        #main-menu .megamenu-child-container ul.sub-menu>li.current-menu-item>span,
        #main-menu .megamenu-child-container ul.sub-menu>li.current-menu-ancestor>span,
        #main-menu .megamenu-child-container.dt-sc-dark-bg>ul.sub-menu>li>a:hover,
        #main-menu .megamenu-child-container.dt-sc-dark-bg ul.sub-menu>li>ul li a:hover,
        #main-menu .megamenu-child-container.dt-sc-dark-bg ul.sub-menu>li>ul li a:hover .fa {
            color: #108bea;
        }

        .left-header .menu-active-highlight-grey #main-menu>ul.menu>li.current_page_item>a,
        .left-header .menu-active-highlight-grey #main-menu>ul.menu>li.current_page_ancestor>a,
        .left-header .menu-active-highlight-grey #main-menu>ul.menu>li.current-menu-item>a,
        .left-header .menu-active-highlight-grey #main-menu>ul.menu>li.current-menu-ancestor>a {
            color: #108bea;
        }

        .blog-entry .entry-meta a:hover,
        .blog-entry.entry-date-left .entry-date a:hover,
        .blog-entry.entry-date-author-left .entry-date-author .comments:hover,
        .blog-entry.entry-date-author-left .entry-date-author .comments:hover i,
        .entry-meta-data p a:hover,
        .blog-entry.entry-date-author-left .entry-date-author .entry-author a:hover,
        .blog-entry.entry-date-author-left .entry-date-author .comments a:hover,
        .dt-sc-dark-bg .blog-medium-style.white-highlight .dt-sc-button.fully-rounded-border,
        .blog-entry.blog-thumb-style .entry-title h4 a:hover,
        .blog-entry.blog-thumb-style a.read-more:hover {
            color: #108bea;
        }

        .widget #wp-calendar td a:hover,
        .dt-sc-dark-bg .widget #wp-calendar td a:hover,
        .secondary-sidebar .widget ul li>a:hover,
        .dt-sc-practices-list li:before,
        .secondary-sidebar .type15 .widget.widget_recent_reviews ul li .reviewer,
        .secondary-sidebar .type15 .widget.widget_top_rated_products ul li .amount.amount,
        #main-menu .menu-item-widget-area-container .widget ul li>a:hover,
        #main-menu .dt-sc-dark-bg .menu-item-widget-area-container .widget ul li>a:hover,
        #main-menu .dt-sc-dark-bg .menu-item-widget-area-container .widget_recent_posts .entry-title h4 a:hover,
        #main-menu ul li.menu-item-simple-parent.dt-sc-dark-bg ul li a:hover,
        #main-menu .menu-item-widget-area-container .widget li:hover:before {
            color: #108bea;
        }

        #footer .footer-copyright .menu-links li a:hover,
        #footer .footer-copyright .copyright-left a:hover,
        #footer .dt-sc-dark-bg .recent-posts-widget li .entry-meta a:hover,
        #footer .dt-sc-dark-bg .entry-title h4 a:hover,
        #footer .dt-sc-dark-bg a:hover,
        .left-header-footer .dt-sc-sociable.filled li a {
            color: #108bea;
        }

        .portfolio .image-overlay .links a:hover,
        .portfolio.type7 .image-overlay .links a,
        .project-details li a:hover,
        .portfolio-categories a:hover,
        .portfolio-tags a:hover,
        .dt-portfolio-single-slider-wrapper #bx-pager a.active:hover:before,
        .dt-portfolio-single-slider-wrapper #bx-pager a,
        .portfolio.type8 .image-overlay .links a {
            color: #108bea;
        }

        .available-domains li span,
        .dt-sc-popular-procedures .details .duration,
        .dt-sc-popular-procedures .details .price,
        .dt-sc-text-with-icon span,
        blockquote.type4>cite,
        .dt-sc-contact-info.type3 span,
        .dt-sc-pr-tb-col.type2 .dt-sc-buy-now a,
        .dt-sc-events-list .dt-sc-event-title h5 a,
        .woocommerce-MyAccount-navigation ul>li.is-active>a {
            color: #108bea;
        }

        .dt-sc-button.fully-rounded-border,
        .dt-sc-button.rounded-border,
        .dt-sc-button.bordered,
        .dt-sc-button.with-shadow.white,
        .dt-sc-skin-highlight .dt-sc-button.rounded-border:hover,
        .dt-sc-skin-highlight .dt-sc-button.bordered:hover,
        .dt-sc-dark-bg.skin-color .dt-sc-button.fully-rounded-border:hover {
            color: #108bea;
        }

        .dt-sc-icon-box.type1 .icon-wrapper .icon,
        .dt-sc-icon-box.type2 .icon-wrapper .icon,
        .dt-sc-icon-box.type4 .icon-wrapper span,
        .dt-sc-icon-box.type5:hover .icon-content h4 a,
        .dt-sc-icon-box.type5.no-icon-bg .icon-wrapper span,
        .dt-sc-icon-box.type5.no-icon-bg:hover .icon-wrapper span,
        .dt-sc-icon-box.type10 .icon-wrapper span,
        .dt-sc-icon-box.type10:hover .icon-content h4,
        .dt-sc-icon-box.type13 .icon-content h4,
        .dt-sc-icon-box.type14 .icon-content h4 {
            color: #108bea;
        }

        .dt-sc-testimonial.type4 .dt-sc-testimonial-author cite,
        .dt-sc-testimonial.type5 .dt-sc-testimonial-author cite,
        .dt-sc-testimonial.type7 .dt-sc-testimonial-quote blockquote cite,
        .dt-sc-testimonial.type8 .dt-sc-testimonial-quote blockquote q:before,
        .dt-sc-testimonial.type8 .dt-sc-testimonial-quote blockquote q:after,
        .dt-sc-testimonial-special-wrapper:after,
        .dt-sc-special-testimonial-images-holder .dt-sc-testimonial-image.slick-current .dt-sc-testimonial-author cite,
        .dt-sc-team-carousel-wrapper .dt-sc-team-details .dt-sc-team-social li a:hover,
        .dt-sc-tabs-horizontal-frame-container.type3 ul.dt-sc-tabs-horizontal-frame>li>a.current {
            color: #108bea;
        }

        ul.dt-sc-tabs-horizontal-frame>li>a.current,
        ul.dt-sc-tabs-horizontal>li>a.current,
        ul.dt-sc-tabs-horizontal>li>a:hover,
        ul.dt-sc-tabs-horizontal-frame>li>a:hover,
        .type7 ul.dt-sc-tabs-horizontal-frame>li>a.current {
            color: #108bea;
        }

        ul.dt-sc-tabs-vertical-frame>li>a:hover,
        ul.dt-sc-tabs-vertical-frame>li.current a,
        ul.dt-sc-tabs-vertical>li>a.current,
        .dt-sc-tabs-vertical-frame-container.type2 ul.dt-sc-tabs-vertical-frame>li>a.current:before,
        ul.dt-sc-tabs-vertical>li>a:hover {
            color: #108bea;
        }

        .dt-sc-toggle-frame-set>.dt-sc-toggle-accordion.active>a,
        .dt-sc-toggle-group-set .dt-sc-toggle.active>a,
        .dt-sc-toggle-frame h5.dt-sc-toggle-accordion.active a,
        .dt-sc-toggle-frame h5.dt-sc-toggle.active a,
        .dt-sc-toggle-panel h2 span {
            color: #108bea;
        }

        .dt-sc-title.with-sub-title h3,
        .dt-sc-title.script-with-sub-title h2,
        .dt-sc-title.with-two-color-stripe h2,
        .dt-sc-hexagon-title h2 span {
            color: #108bea;
        }

        .dt-sc-image-with-caption h3 a,
        .dt-sc-image-caption.type3 .dt-sc-image-content h3,
        .dt-sc-event-image-caption .dt-sc-image-content h3,
        .dt-sc-image-caption.type8:hover .dt-sc-image-content h3 a:hover,
        .dt-sc-image-caption.type3 .dt-sc-image-wrapper .icon-wrapper span {
            color: #108bea;
        }

        .dt-sc-team.hide-social-role-show-on-hover .dt-sc-team-social.rounded-square li a,
        .dt-sc-team.rounded .dt-sc-team-details .dt-sc-team-social li a:hover,
        .dt-sc-team.rounded.team_rounded_border:hover .dt-sc-team-details h4,
        .dt-sc-team.type2 .dt-sc-team-social.rounded-border li a:hover,
        .dt-sc-team.type2 .dt-sc-team-social.rounded-square li a:hover,
        .dt-sc-team.type2 .dt-sc-team-social.square-border li a:hover,
        .dt-sc-team.type2 .dt-sc-team-social.hexagon-border li a:hover,
        .dt-sc-team.type2 .dt-sc-team-social.diamond-square-border li a:hover {
            color: #108bea;
        }

        .dt-sc-timeline .dt-sc-timeline-content h2 span,
        .dt-sc-hr-timeline-section.type2 .dt-sc-hr-timeline-content:hover h3,
        .dt-sc-timeline-section.type4 .dt-sc-timeline:hover .dt-sc-timeline-content h2 {
            color: #108bea;
        }

        .dt-sc-sociable.diamond-square-border li:hover a,
        .dt-sc-sociable.hexagon-border li:hover a,
        .dt-sc-sociable.hexagon-with-border li:hover a,
        .dt-sc-sociable.no-margin li a {
            color: #108bea;
        }

        .dt-sc-counter.type3.diamond-square h4,
        .dt-sc-counter.type6:hover h4 {
            color: #108bea;
        }

        .dt-sc-menu-sorting a:hover,
        .dt-sc-menu-sorting a.active-sort,
        .dt-sc-training-details h6,
        .dt-sc-fitness-diet:hover .dt-sc-fitness-diet-details h5,
        table.fit-Diet-table th strong,
        .dt-sc-hotel-room-single-metadata ul li,
        .dt-sc-hotel-room .dt-sc-hotel-room-details ul li,
        ul.dt-sc-vertical-nav>li>a:hover,
        .university-contact-info p a:hover,
        .vcr_hover-arrow .slick-arrow {
            color: #108bea;
        }

        th,
        input[type="submit"],
        button,
        input[type="reset"],
        .loader {
            background-color: #108bea;
        }

        .left-header #toggle-sidebar,
        .left-header-footer,
        .overlay-header #trigger-overlay,
        .overlay .overlay-close,
        .menu-icons-wrapper.rounded-icons .search a span:hover,
        .menu-icons-wrapper.rounded-icons .cart a span:hover,
        .menu-icons-wrapper .cart sup {
            background-color: #108bea;
        }

        .dt-menu-toggle,
        .two-color-header .main-header-wrapper:before {
            background-color: #108bea;
        }

        .menu-active-highlight #main-menu>ul.menu>li.current_page_item,
        .menu-active-highlight #main-menu>ul.menu>li.current_page_ancestor,
        .menu-active-highlight #main-menu>ul.menu>li.current-menu-item,
        .menu-active-highlight #main-menu>ul.menu>li.current-menu-ancestor {
            background-color: #108bea;
        }

        .menu-active-highlight-grey #main-menu>ul.menu>li.current_page_item>a:before,
        .menu-active-highlight-grey #main-menu>ul.menu>li.current_page_ancestor>a:before,
        .menu-active-highlight-grey #main-menu>ul.menu>li.current-menu-item>a:before,
        .menu-active-highlight-grey #main-menu>ul.menu>li.current-menu-ancestor>a:before {
            background-color: #108bea;
        }

        .menu-active-highlight-with-arrow #main-menu>ul.menu>li.current_page_item>a,
        .menu-active-highlight-with-arrow #main-menu>ul.menu>li.current_page_ancestor>a,
        .menu-active-highlight-with-arrow #main-menu>ul.menu>li.current-menu-item>a,
        .menu-active-highlight-with-arrow #main-menu>ul.menu>li.current-menu-ancestor>a {
            background-color: #108bea;
        }

        .menu-active-with-icon #main-menu>ul.menu>li.current_page_item>a:before,
        .menu-active-with-icon #main-menu>ul.menu>li.current_page_ancestor>a:before,
        .menu-active-with-icon #main-menu>ul.menu>li.current-menu-item>a:before,
        .menu-active-with-icon #main-menu>ul.menu>li.current-menu-ancestor>a:before,
        .menu-active-with-icon #main-menu>ul.menu>li.current_page_item>a:after,
        .menu-active-with-icon #main-menu>ul.menu>li.current_page_ancestor>a:after,
        .menu-active-with-icon #main-menu>ul.menu>li.current-menu-item>a:after,
        .menu-active-with-icon #main-menu>ul.menu>li.current-menu-ancestor>a:after {
            background-color: #108bea;
        }

        .menu-active-border-with-arrow #main-menu>ul.menu>li.current_page_item>a:after,
        .menu-active-border-with-arrow #main-menu>ul.menu>li.current_page_ancestor>a:after,
        .menu-active-border-with-arrow #main-menu>ul.menu>li.current-menu-item>a:after,
        .menu-active-border-with-arrow #main-menu>ul.menu>li.current-menu-ancestor>a:after {
            background-color: #108bea;
        }

        .menu-active-with-two-border #main-menu>ul.menu>li.current_page_item>a:before,
        .menu-active-with-two-border #main-menu>ul.menu>li.current_page_ancestor>a:before,
        .menu-active-with-two-border #main-menu>ul.menu>li.current-menu-item>a:before,
        .menu-active-with-two-border #main-menu>ul.menu>li.current-menu-ancestor>a:before,
        .menu-active-with-two-border #main-menu>ul.menu>li.current_page_item>a:after,
        .menu-active-with-two-border #main-menu>ul.menu>li.current_page_ancestor>a:after,
        .menu-active-with-two-border #main-menu>ul.menu>li.current-menu-item>a:after,
        .menu-active-with-two-border #main-menu>ul.menu>li.current-menu-ancestor>a:after {
            background-color: #108bea;
        }

        .left-header .menu-active-highlight #main-menu>ul.menu>li.current_page_item>a,
        .left-header .menu-active-highlight #main-menu>ul.menu>li.current_page_ancestor>a,
        .left-header .menu-active-highlight #main-menu>ul.menu>li.current-menu-item>a,
        .left-header .menu-active-highlight #main-menu>ul.menu>li.current-menu-ancestor>a {
            background-color: #108bea;
        }

        .entry-format a,
        .blog-entry.blog-medium-style:hover .entry-format a,
        .blog-entry.blog-medium-style.dt-blog-medium-highlight.dt-sc-skin-highlight,
        .blog-entry.blog-medium-style.dt-blog-medium-highlight.dt-sc-skin-highlight .entry-format a,
        ul.commentlist li .reply a:hover,
        .dt-sc-dark-bg .blog-medium-style.white-highlight .dt-sc-button.fully-rounded-border:hover,
        .post-nav-container .post-next-link a:hover,
        .post-nav-container .post-prev-link a:hover,
        .page-link>span,
        .page-link a:hover,
        .post-edit-link:hover,
        .vc_inline-link:hover,
        .pagination ul li a:hover,
        .pagination ul li span {
            background-color: #108bea;
        }

        .widget .dt-sc-newsletter-section.boxed .dt-sc-subscribe-frm input[type="submit"]:hover,
        .tagcloud a:hover,
        .widgettitle:before,
        .widget.widget_categories ul li>a:hover span,
        .widget.widget_archive ul li>a:hover span,
        .dt-sc-dark-bg .tagcloud a:hover,
        .dt-sc-dark-bg .widget.widget_categories ul li>a:hover span,
        #footer .dt-sc-dark-bg .widget.widget_categories ul li>a:hover span,
        #footer .dt-sc-dark-bg .widget.widget_archive ul li>a:hover span {
            background-color: #108bea;
        }

        .dt-sc-portfolio-sorting a.active-sort,
        .dt-sc-portfolio-sorting a:hover,
        .dt-sc-portfolio-sorting a:hover:before,
        .dt-sc-portfolio-sorting a:hover:after,
        .dt-sc-portfolio-sorting a.active-sort:before,
        .dt-sc-portfolio-sorting a.active-sort:after,
        .portfolio.type2 .image-overlay-details,
        .portfolio.type2 .image-overlay .links a:hover,
        .dt-sc-portfolio-sorting.type2,
        .dt-sc-portfolio-sorting.type2:before,
        .portfolio.type6 .image-overlay .links a:hover,
        .portfolio.type7 .image-overlay-details .categories a:before,
        .portfolio.type7 .image-overlay .links a:hover:before {
            background-color: #108bea;
        }

        ul.side-nav li a:hover:before,
        ul.side-nav>li.current_page_item>a:before,
        ul.side-nav>li>ul>li.current_page_item>a:before,
        ul.side-nav>li>ul>li>ul>li.current_page_item>a:before,
        .dt-sc-small-separator,
        .dt-sc-diamond-separator,
        .dt-sc-titled-box h6.dt-sc-titled-box-title,
        .carousel-arrows a:hover,
        .dt-sc-images-wrapper .carousel-arrows a:hover,
        .diamond-narrow-square-border li:hover:before,
        .dt-sc-sociable.hexagon-with-border li,
        .dt-sc-skin-highlight,
        .dt-sc-skin-highlight.extend-bg-fullwidth-left:after,
        .dt-sc-skin-highlight.extend-bg-fullwidth-right:after,
        .two-color-section:before,
        .dt-sc-readmore-plus-icon:hover:before,
        .dt-sc-readmore-plus-icon:hover:after,
        .dt-sc-content-with-hexagon-shape,
        .dt-sc-hexagons li .dt-sc-hexagon-overlay,
        .available-domains li .tdl:before,
        .available-domains li:hover .dt-sc-button,
        .domain-search-container .domain-search-form,
        .dt-sc-newsletter-section.type1 h2:before,
        .dt-sc-newsletter-section.type1 h2:after,
        .top-icon .aio-icon-img {
            background-color: #108bea;
        }

        .dt-sc-button.filled,
        .dt-sc-button:hover,
        .dt-sc-button.rounded-border:hover,
        .dt-sc-button.bordered:hover,
        .dt-sc-button.fully-rounded-border:hover,
        .dt-sc-colored-big-buttons:hover,
        .dt-sc-colored-big-buttons span {
            background-color: #108bea;
        }

        .dt-sc-contact-info.type2:hover span,
        .dt-sc-contact-info.type3,
        .dt-sc-contact-info.type4 span:after,
        .dt-sc-contact-info.type4:before,
        .dt-sc-contact-info.type5 .dt-sc-contact-icon,
        .dt-sc-contact-info.type5:hover,
        .dt-sc-contact-info.type6,
        .dt-sc-contact-info.type7 span:after,
        .dt-sc-contact-info.type4:after,
        .university-contact-form .button-field i {
            background-color: #108bea;
        }

        .dt-sc-counter.type1 .icon-wrapper:before,
        .dt-sc-counter.type2 .dt-sc-couter-icon-holder,
        .dt-sc-counter.type3:hover .icon-wrapper,
        .dt-sc-counter.type3.diamond-square .dt-sc-couter-icon-holder .icon-wrapper:before,
        .dt-sc-counter.type4:hover .dt-sc-couter-icon-holder,
        .dt-sc-counter.type5:hover:after,
        .dt-sc-counter.type6 h4:before,
        .dt-sc-counter.type6:hover .dt-sc-couter-icon-holder:before {
            background-color: #108bea;
        }

        .dt-sc-icon-box.type1 .icon-content h4:before,
        .dt-sc-icon-box.type3.dt-sc-diamond:hover .icon-wrapper:after,
        .dt-sc-icon-box.type5.rounded-skin .icon-wrapper,
        .dt-sc-icon-box.type5.rounded:hover .icon-wrapper,
        .dt-sc-icon-box.type5:hover .icon-wrapper:before,
        .dt-sc-icon-box.type5.alter .icon-wrapper:before,
        .dt-sc-icon-box.type6 .icon-wrapper,
        .dt-sc-icon-box.type7 .icon-wrapper span,
        .dt-sc-contact-info.type8:hover span,
        .dt-sc-icon-box.type10:hover .icon-wrapper:before,
        .dt-sc-icon-box.type10 .icon-content h4:before,
        .dt-sc-icon-box.type11:before,
        .dt-sc-icon-box.type12,
        .dt-sc-icon-box.type13:hover,
        .dt-sc-icon-box.type14:hover,
        .dt-sc-icon-box.type3 .icon-content h4:before,
        .dt-sc-icon-box.type4 .icon-content h4:after,
        .custom-arrow-nav button.slick-arrow:hover i:before,
        .dt-sc-image-caption:hover .dt-sc-image-content a.dt-sc-button,
        .dt-sc-icon-box.type3:hover .icon-wrapper span {
            background-color: #108bea;
        }

        .dt-sc-testimonial-wrapper .dt-sc-testimonial-bullets a:hover,
        .dt-sc-testimonial-wrapper .dt-sc-testimonial-bullets a.active,
        .dt-sc-tabs-horizontal-frame-container.type3 ul.dt-sc-tabs-horizontal-frame>li>a {
            background-color: #108bea;
        }

        .dt-sc-title.with-two-color-bg:after,
        .dt-sc-triangle-title:after,
        .dt-sc-title.with-right-border-decor:after,
        .dt-sc-title.with-right-border-decor:before,
        .dt-sc-title.with-boxed,
        .mz-title .mz-title-content h2,
        .mz-title-content h3.widgettitle,
        .mz-title .mz-title-content:before,
        .mz-blog .comments a,
        .mz-blog div.vc_gitem-post-category-name,
        .mz-blog .ico-format {
            background-color: #108bea;
        }

        .dt-sc-team-social.hexagon-border li:hover,
        .dt-sc-team .dt-sc-team-social.diamond-square-border li:hover,
        .dt-sc-team.hide-social-role-show-on-hover .dt-sc-team-social.rounded-square li:hover a,
        .dt-sc-infinite-portfolio-load-more,
        .dt-sc-single-hexagon .dt-sc-single-hexagon-overlay,
        .dt-sc-team-social.rounded-border li a:hover,
        .dt-sc-team-social.rounded-square li a,
        .dt-sc-team.hide-social-show-on-hover:hover .dt-sc-team-details,
        .dt-sc-team-social.square-border li a:hover,
        .dt-sc-team.rounded:hover .dt-sc-team-thumb:after,
        .dt-sc-team.hide-social-role-show-on-hover:hover .dt-sc-team-details,
        .dt-sc-team.hide-social-role-show-on-hover .dt-sc-team-social li:hover,
        .dt-sc-team.style2 .dt-sc-sociable li a,
        .dt-sc-team.style2 .dt-sc-team-details .view-details:hover {
            background-color: #108bea;
        }

        .dt-sc-pr-tb-col.minimal:hover .dt-sc-price,
        .dt-sc-pr-tb-col.minimal.selected .dt-sc-price,
        .dt-sc-pr-tb-col:hover .dt-sc-buy-now a,
        .dt-sc-pr-tb-col.selected .dt-sc-buy-now a,
        .dt-sc-pr-tb-col.minimal:hover .icon-wrapper:before,
        .dt-sc-pr-tb-col.minimal.selected .icon-wrapper:before,
        .dt-sc-pr-tb-col.type1:hover .dt-sc-tb-header,
        .dt-sc-pr-tb-col.type1.selected .dt-sc-tb-header,
        .dt-sc-pr-tb-col.type2 .dt-sc-tb-header .dt-sc-tb-title:before,
        .dt-sc-pr-tb-col.type2 .dt-sc-tb-content:before,
        .dt-sc-pr-tb-col.type2 .dt-sc-tb-content li .highlight,
        .dt-sc-pr-tb-col.type2:hover .dt-sc-price:before,
        .dt-sc-pr-tb-col.type2.selected .dt-sc-price:before,
        .dt-sc-pr-tb-col.type2:hover .dt-sc-buy-now a {
            background-color: #108bea;
        }

        .dt-sc-hr-timeline-section.type1:before,
        .dt-sc-hr-timeline-section.type1 .dt-sc-hr-timeline .dt-sc-hr-timeline-content:after,
        .dt-sc-hr-timeline-section.type1 .dt-sc-hr-timeline-wrapper:before,
        .dt-sc-hr-timeline-section.type1 .dt-sc-hr-timeline-wrapper:after,
        .dt-sc-hr-timeline-section.type2 .dt-sc-hr-timeline-content h3:before,
        .dt-sc-hr-timeline-section.type2 .dt-sc-hr-timeline:hover .dt-sc-hr-timeline-thumb:before {
            background-color: #108bea;
        }

        .dt-sc-timeline-section.type2:before,
        .dt-sc-timeline-section.type3 .dt-sc-timeline .dt-sc-timeline-content h2:before,
        .dt-sc-timeline-section.type4 .dt-sc-timeline .dt-sc-timeline-content h2:before,
        .dt-sc-timeline-section.type4 .dt-sc-timeline:hover .dt-sc-timeline-thumb:before {
            background-color: #108bea;
        }

        .dt-sc-image-caption.type4:hover .dt-sc-button,
        .dt-sc-image-caption.type8 .dt-sc-image-content:before,
        .dt-sc-event-image-caption:hover {
            background-color: #108bea;
        }

        .dt-sc-tabs-horizontal-frame-container.type4 ul.dt-sc-tabs-horizontal-frame>li>a.current>span:after,
        .dt-sc-tabs-horizontal-frame-container.type5 ul.dt-sc-tabs-horizontal-frame>li>a.current,
        .dt-sc-tabs-horizontal-frame-container.type6 ul.dt-sc-tabs-horizontal-frame>li>a,
        .type8 ul.dt-sc-tabs-horizontal-frame>li>a.current,
        .type8 ul.dt-sc-tabs-horizontal-frame>li>a:hover {
            background-color: #108bea;
        }

        .dt-sc-tabs-vertical-frame-container.type3 ul.dt-sc-tabs-vertical-frame>li>a:hover,
        .dt-sc-tabs-vertical-frame-container.type3 ul.dt-sc-tabs-vertical-frame>li>a.current,
        .dt-sc-tabs-vertical-frame-container.type4 ul.dt-sc-tabs-vertical-frame>li>a:before,
        .dt-sc-tabs-vertical-frame-container.type4 ul.dt-sc-tabs-vertical-frame>li>a:after {
            background-color: #108bea;
        }

        .dt-sc-toggle-frame h5.dt-sc-toggle-accordion.active a:before,
        h5.dt-sc-toggle-accordion.active a:before,
        .dt-sc-toggle-frame h5.dt-sc-toggle.active a:before,
        h5.dt-sc-toggle.active a:before,
        .type2 .dt-sc-toggle-frame h5.dt-sc-toggle-accordion.active,
        .type2 .dt-sc-toggle-frame h5.dt-sc-toggle.active,
        .dt-sc-toggle-frame-set.type2>h5.dt-sc-toggle-accordion.active:after,
        .dt-sc-toggle-icon {
            background-color: #108bea;
        }

        .dt-sc-video-wrapper .video-overlay-inner a,
        .dt-sc-video-item:hover .dt-sc-vitem-detail,
        .dt-sc-video-item.active .dt-sc-vitem-detail,
        .type2 .dt-sc-video-item:hover,
        .type2 .dt-sc-video-item.active,
        .nicescroll-rails.dt-sc-skin {
            background-color: #108bea;
        }

        .live-chat a,
        .dt-bmi-inner-content tbody th,
        .dt-bmi-inner-content tbody tr:nth-child(2n+1) th,
        .dt-sc-menu .menu-categories a:before,
        .hotel-search-container form input[type="submit"]:hover,
        .hotel-search-container .selection-box:after,
        .dt-sc-training-details-overlay,
        .custom-navigation .vc_images_carousel .vc_carousel-indicators li,
        .dt-sc-doctors.style1 .dt-sc-doctors-thumb-wrapper .dt-sc-button,
        .dt-sc-doctors-single .dt-sc-doctors.style1 .dt-sc-doctors-details ul.dt-sc-sociable li a,
        .dt-sc-procedure-item:hover,
        .dt-sc-fitness-procedure-sorting a,
        ul.dt-sc-vertical-nav>li.active>a,
        ul.time-table>li,
        ul.time-slots>li a:hover,
        .dt-sc-available-times ul.time-slots,
        #wpsl-search-btn,
        #wpsl-stores li>p span,
        #wpsl-stores li>p,
        #wpsl-stores li>p~.wpsl-directions,
        .dt-sc-toggle-advanced-options span,
        .dt-inline-modal>h4 {
            background-color: #108bea;
        }

        #main-menu ul li.menu-item-simple-parent ul,
        #main-menu .megamenu-child-container {
            border-botom-color: #108bea;
        }

        .menu-active-with-double-border #main-menu>ul.menu>li.current_page_item>a,
        .menu-active-with-double-border #main-menu>ul.menu>li.current_page_ancestor>a,
        .menu-active-with-double-border #main-menu>ul.menu>li.current-menu-item>a,
        .menu-active-with-double-border #main-menu>ul.menu>li.current-menu-ancestor>a {
            border-color: #108bea;
        }

        .menu-active-border-with-arrow #main-menu>ul.menu>li.current_page_item>a:before,
        .menu-active-border-with-arrow #main-menu>ul.menu>li.current_page_ancestor>a:before,
        .menu-active-border-with-arrow #main-menu>ul.menu>li.current-menu-item>a:before,
        .menu-active-border-with-arrow #main-menu>ul.menu>li.current-menu-ancestor>a:before,
        .main-header .menu-icons-wrapper .overlay-search #searchform {
            border-bottom-color: #108bea;
        }

        .menu-active-highlight-with-arrow #main-menu>ul.menu>li.current_page_item>a:before,
        .menu-active-highlight-with-arrow #main-menu>ul.menu>li.current_page_ancestor>a:before,
        .menu-active-highlight-with-arrow #main-menu>ul.menu>li.current-menu-item>a:before,
        .menu-active-highlight-with-arrow #main-menu>ul.menu>li.current-menu-ancestor>a:before,
        #main-menu .menu-item-megamenu-parent .megamenu-child-container>ul.sub-menu>li:nth-child(odd):hover>a:after,
        #main-menu .menu-item-megamenu-parent .megamenu-child-container>ul.sub-menu>li:nth-child(odd):hover>.nolink-menu:after,
        #main-menu .menu-item-megamenu-parent .megamenu-child-container>ul.sub-menu>li.current-menu-ancestor:nth-child(odd)>a:after,
        #main-menu .menu-item-megamenu-parent .megamenu-child-container>ul.sub-menu>li.current-menu-ancestor:nth-child(odd)>.nolink-menu:after,
        .dt-sc-tabs-horizontal-frame-container.type3 ul.dt-sc-tabs-horizontal-frame>li>a.current {
            border-top-color: #108bea;
        }

        .blog-entry.entry-date-left .entry-date span,
        .blog-entry.blog-medium-style:hover .entry-format a,
        ul.commentlist li .reply a:hover,
        .dt-sc-dark-bg .blog-medium-style.white-highlight .dt-sc-button.fully-rounded-border,
        .pagination ul li a:hover,
        .pagination ul li span,
        .post-nav-container .post-next-link a:hover,
        .post-nav-container .post-prev-link a:hover,
        .page-link>span,
        .page-link a:hover {
            border-color: #108bea;
        }

        .widget .dt-sc-newsletter-section.boxed,
        .widget .dt-sc-newsletter-section.boxed .dt-sc-subscribe-frm input[type="submit"],
        .tagcloud a:hover,
        .dt-sc-dark-bg .tagcloud a:hover,
        .secondary-sidebar .type3 .widgettitle,
        .secondary-sidebar .type6 .widgettitle,
        .secondary-sidebar .type13 .widgettitle:before,
        .secondary-sidebar .type14 .widgettitle,
        .secondary-sidebar .type16 .widgettitle {
            border-color: #108bea;
        }

        .dt-sc-portfolio-sorting a.active-sort,
        .dt-sc-portfolio-sorting a:hover,
        .portfolio.type7 .image-overlay .links a:before {
            border-color: #108bea;
        }

        .dt-sc-colored-big-buttons,
        .dt-sc-button.fully-rounded-border,
        .dt-sc-button.fully-rounded-border:hover,
        .dt-sc-button.rounded-border.black:hover,
        .dt-sc-button.bordered.black:hover,
        .dt-sc-button.bordered:hover,
        .dt-sc-button.rounded-border:hover {
            border-color: #108bea;
        }

        .dt-sc-sociable.rounded-border li a:hover,
        .dt-sc-dark-bg .dt-sc-sociable.rounded-border li a:hover,
        .dt-sc-dark-bg .dt-sc-sociable.square-border li a:hover,
        .dt-sc-sociable.diamond-square-border li:hover,
        .diamond-narrow-square-border li:before {
            border-color: #108bea;
        }

        .dt-sc-team .dt-sc-team-social.diamond-square-border li:hover,
        .dt-sc-team-social.hexagon-border li:hover,
        .dt-sc-team-social.hexagon-border li:hover:before,
        .dt-sc-team-social.hexagon-border li:hover:after,
        .dt-sc-team-social.rounded-border li a:hover,
        .dt-sc-team-social.square-border li a:hover,
        .dt-sc-team.team_rounded_border.rounded:hover .dt-sc-team-thumb:before {
            border-color: #108bea;
        }

        .dt-sc-testimonial.type5 .dt-sc-testimonial-quote,
        .dt-sc-testimonial-images li.selected div,
        .dt-sc-testimonial-wrapper .dt-sc-testimonial-bullets a:hover,
        .dt-sc-testimonial-wrapper .dt-sc-testimonial-bullets a.active,
        .dt-sc-testimonial-wrapper .dt-sc-testimonial-bullets a.active:before,
        .dt-sc-testimonial-wrapper .dt-sc-testimonial-bullets a.active:hover:before,
        .dt-sc-testimonial.type5 .dt-sc-testimonial-author img {
            border-color: #108bea;
        }

        ul.dt-sc-tabs-horizontal>li>a.current,
        ul.dt-sc-tabs-vertical>li>a.current,
        .dt-sc-tabs-vertical-frame-container.type3 ul.dt-sc-tabs-vertical-frame>li>a:hover,
        .dt-sc-tabs-vertical-frame-container.type3 ul.dt-sc-tabs-vertical-frame>li>a.current {
            border-color: #108bea;
        }

        .type2 .dt-sc-toggle-frame h5.dt-sc-toggle-accordion.active,
        .type2 .dt-sc-toggle-frame h5.dt-sc-toggle.active {
            border-color: #108bea;
        }

        .dt-sc-hr-timeline-section.type1 .dt-sc-hr-timeline .dt-sc-hr-timeline-content:before,
        .dt-sc-timeline-section.type2 .dt-sc-timeline-image-wrapper,
        .dt-sc-timeline-section.type2 .dt-sc-timeline .dt-sc-timeline-content:after,
        .dt-sc-timeline-section.type2:after {
            border-color: #108bea;
        }

        .dt-sc-counter.type3 .icon-wrapper:before,
        .dt-sc-counter.type3.diamond-square,
        .dt-sc-counter.type5:hover:before,
        .dt-sc-counter.type5:hover:after,
        .dt-sc-counter.type6,
        .dt-sc-counter.type6 .dt-sc-couter-icon-holder:before {
            border-color: #108bea;
        }

        .dt-sc-contact-info.type2:hover,
        .dt-sc-contact-info.type4,
        .last .dt-sc-contact-info.type4 {
            border-color: #108bea;
        }

        .dt-sc-icon-box.type5.no-icon .icon-content h4,
        .dt-sc-icon-box.type5.no-icon,
        .dt-sc-icon-box.type10,
        .dt-sc-icon-box.type10 .icon-wrapper:before,
        .dt-sc-icon-box.type3.dt-sc-diamond:hover .icon-wrapper:after,
        .dt-sc-icon-box.type11:before {
            border-color: #108bea;
        }

        .dt-sc-image-caption.type2 .dt-sc-image-content,
        .dt-sc-image-caption.type4,
        .dt-sc-image-caption.type4:hover .dt-sc-button,
        .dt-sc-icon-box.type10:hover .icon-wrapper:before {
            border-color: #108bea;
        }

        .dt-sc-title.with-right-border-decor h2:before,
        .dt-sc-pr-tb-col.type2 .dt-sc-tb-header:before,
        .dt-sc-newsletter-section.type2 .dt-sc-subscribe-frm input[type="text"],
        .dt-sc-newsletter-section.type2 .dt-sc-subscribe-frm input[type="email"],
        .dt-sc-text-with-icon.border-bottom,
        .dt-sc-text-with-icon.border-right,
        .dt-sc-hexagons li:hover,
        .dt-sc-hexagons li:hover:before,
        .dt-sc-hexagons li:hover:after,
        .dt-sc-hexagons li,
        .dt-sc-hexagons li:before,
        .dt-sc-hexagons li .dt-sc-hexagon-overlay:before,
        .dt-sc-hexagons li:after,
        .dt-sc-hexagons li .dt-sc-hexagon-overlay:after,
        .dt-sc-single-hexagon,
        .dt-sc-single-hexagon:before,
        .dt-sc-single-hexagon .dt-sc-single-hexagon-overlay:before,
        .dt-sc-single-hexagon:after,
        .dt-sc-single-hexagon .dt-sc-single-hexagon-overlay:after,
        .dt-sc-single-hexagon:hover,
        .dt-sc-single-hexagon:hover:before,
        .dt-sc-single-hexagon:hover:after,
        .carousel-arrows a:hover,
        .vc_custom_carousel .slick-slider .slick-dots,
        .vc_custom_carousel .slick-slider:before,
        .dt-sc-team-navigation .dt-sc-team-pager-prev:before,
        .dt-sc-team-navigation .dt-sc-team-pager-next:before,
        ul.dt-sc-vertical-nav,
        ul.dt-sc-vertical-nav>li:first-child>a,
        .dt-sc-loading:before {
            border-color: #108bea;
        }

        .dt-sc-triangle-wrapper:hover .dt-sc-triangle-content:before,
        .dt-sc-pr-tb-col.type2 .dt-sc-tb-content:after,
        .dt-sc-content-with-hexagon-shape:after,
        .type7 ul.dt-sc-tabs-horizontal-frame>li>a.current:before,
        .type7 ul.dt-sc-tabs-horizontal-frame>li>a.current:after,
        .skin-highlight .dt-sc-tabs-horizontal-frame-container.type6 ul.dt-sc-tabs-horizontal-frame>li>a:before,
        .dt-sc-doctors-filter .selection-box:before {
            border-top-color: #108bea;
        }

        .dt-sc-up-arrow:before,
        .dt-sc-image-caption .dt-sc-image-wrapper .icon-wrapper:before,
        .dt-sc-triangle-wrapper.alter:hover .dt-sc-triangle-content:before,
        .dt-sc-content-with-hexagon-shape:before,
        .dt-sc-tabs-horizontal-frame-container.type4 ul.dt-sc-tabs-horizontal-frame>li>a.current {
            border-bottom-color: #108bea;
        }

        .type3 .dt-sc-toggle-frame .dt-sc-toggle-content,
        .dt-sc-tabs-vertical-frame-container.type3 ul.dt-sc-tabs-vertical-frame>li>a.current:before,
        .dt-sc-event-image-caption:hover .dt-sc-image-content:before {
            border-left-color: #108bea;
        }

        .dt-sc-attorney-sorting,
        .dt-sc-menu-sorting a.active-sort,
        .dt-sc-menu .image-overlay .price,
        .hotel-search-container form input[type="submit"] {
            border-color: #108bea;
        }

        .error404 .type2 a.dt-sc-back,
        .error404 .type4 .error-box,
        .error404 .type4 .dt-sc-newsletter-section input[type="submit"],
        .error404 .type8 .dt-go-back {
            background-color: #108bea;
        }

        .error404 .type2 h2,
        .error404 .type8 h2,
        .error404 .type8 .dt-go-back:hover i {
            color: #108bea;
        }

        .under-construction.type4 .dt-sc-counter-wrapper,
        .under-construction.type1 .dt-sc-newsletter-section form input[type="submit"],
        .under-construction.type1 .dt-sc-counter-wrapper .counter-icon-wrapper:before,
        .under-construction.type2 .dt-sc-sociable>li:hover a,
        .under-construction.type7 .dt-sc-sociable>li:hover a,
        .under-construction.type3 .dt-sc-newsletter-section form input[type="submit"],
        .under-construction.type3 .dt-sc-sociable>li:hover a,
        .under-construction.type7 .dt-sc-counter-wrapper,
        .under-construction.type7 .dt-sc-newsletter-section form input[type="submit"] {
            background-color: #108bea;
        }

        .under-construction.type3 .dt-sc-sociable>li:hover a {
            border-color: #108bea;
        }

        .under-construction.type4 .wpb_wrapper>h2 span,
        .under-construction.type4 .read-more i,
        .under-construction.type4 .wpb_wrapper>h4:after,
        .under-construction.type4 .wpb_wrapper>h4:before,
        .under-construction.type1 .read-more span.fa,
        .under-construction.type1 .read-more a:hover,
        .under-construction.type2 .counter-icon-wrapper .dt-sc-counter-number,
        .under-construction.type2 h2,
        .under-construction.type2 .dt-sc-counter-wrapper h3,
        .under-construction.type2 .mailchimp-newsletter h3,
        .under-construction.type7 h2,
        .under-construction.type7 .mailchimp-newsletter h3,
        .under-construction.type3 p,
        .under-construction.type5 h2 span,
        .under-construction.type5 .dt-sc-counter-number,
        .under-construction.type5 footer .dt-sc-team-social li:hover a,
        .under-construction.type5 input[type="email"],
        .under-construction.type7 .aligncenter .wpb_text_column h2 {
            color: #108bea;
        }

        #buddypress div.pagination .pagination-links span,
        #buddypress div.pagination .pagination-links a:hover,
        #buddypress #group-create-body #group-creation-previous,
        #item-header-content #item-meta>#item-buttons .group-button,
        #buddypress div#subnav.item-list-tabs ul li.feed a:hover,
        #buddypress div.activity-meta a:hover,
        #buddypress div.item-list-tabs ul li.selected a span,
        #buddypress .activity-list li.load-more a,
        #buddypress .activity-list li.load-newest a {
            background-color: #108bea;
        }

        #buddypress div.pagination .pagination-links span,
        #buddypress div.pagination .pagination-links a:hover,
        #buddypress #members-dir-list ul li:hover {
            border-color: #108bea;
        }

        #members-list.item-list.single-line li h5 span.small a.button,
        #buddypress div.item-list-tabs ul li.current a,
        #buddypress #group-create-tabs ul li.current a,
        #buddypress a.bp-primary-action:hover span,
        #buddypress div.item-list-tabs ul li.selected a,
        .widget.buddypress div.item-options a:hover,
        .widget.buddypress div.item-options a.selected,
        #footer .footer-widgets.dt-sc-dark-bg .widget.buddypress div.item-options a.selected,
        .widget.widget_bp_core_members_widget div.item .item-title a:hover,
        .widget.buddypress .bp-login-widget-user-links>div.bp-login-widget-user-link a:hover {
            color: #108bea;
        }

        #bbpress-forums li.bbp-header,
        .bbp-submit-wrapper #bbp_topic_submit,
        .bbp-reply-form #bbp_reply_submit,
        .bbp-pagination-links a:hover,
        .bbp-pagination-links span.current,
        #bbpress-forums #subscription-toggle a.subscription-toggle {
            background-color: #108bea;
        }

        .bbp-pagination-links a:hover,
        .bbp-pagination-links span.current {
            border-color: #108bea;
        }

        .bbp-forums .bbp-body .bbp-forum-info::before {
            color: #108bea;
        }

        #tribe-bar-views .tribe-bar-views-list .tribe-bar-views-option a:hover,
        #tribe-bar-views .tribe-bar-views-list .tribe-bar-views-option.tribe-bar-active a:hover,
        #tribe-bar-form .tribe-bar-submit input[type="submit"],
        #tribe-bar-views .tribe-bar-views-list li.tribe-bar-active a,
        .tribe-events-calendar thead th,
        #tribe-events-content .tribe-events-tooltip h4,
        .tribe-events-calendar td.tribe-events-present div[id*="tribe-events-daynum-"],
        .tribe-events-read-more,
        #tribe-events .tribe-events-button,
        .tribe-events-button,
        .tribe-events-calendar td.tribe-events-present div[id*="tribe-events-daynum-"]>a,
        .tribe-events-back>a,
        #tribe_events_filters_toggle {
            background-color: #108bea;
        }

        .tribe-events-list .tribe-events-event-cost span {
            border-color: #108bea;
        }

        .tribe-grid-header,
        .tribe-grid-allday .tribe-events-week-allday-single,
        .tribe-grid-body .tribe-events-week-hourly-single {
            background-color: #108bea;
        }

        .type1.tribe_events .event-image-wrapper .event-datetime>span,
        .type3.tribe_events .event-date,
        .event-meta-tab ul.dt-sc-tabs-horizontal-frame>li>a {
            background-color: #108bea;
        }

        .type1 .event-schedule,
        .type1.tribe_events .nav-top-links a:hover,
        .type1.tribe_events .event-image-wrapper .event-datetime>i,
        .type1.tribe_events .event-image-wrapper .event-venue>i,
        .type1.tribe_events h4 a,
        .type2.tribe_events .date-wrapper p span,
        .type2.tribe_events h4 a,
        .type3.tribe_events .right-calc a:hover,
        .type3.tribe_events .tribe-events-sub-nav li a:hover,
        .type3.tribe_events .tribe-events-sub-nav li a span,
        .type4.tribe_events .data-wrapper p span,
        .type4.tribe_events .data-wrapper p i,
        .type4.tribe_events .event-organize h4 a,
        .type4.tribe_events .event-venue h4 a,
        .type5.tribe_events .event-details h3,
        .type5.tribe_events .event-organize h3,
        .type5.tribe_events .event-venue h3,
        .type5.tribe_events .data-wrapper p span,
        .data-wrapper p i,
        .type5.tribe_events .event-organize h4 a,
        .type5.tribe_events .event-venue h4 a {
            color: #108bea;
        }

        .dt-sc-event.type1 .dt-sc-event-thumb p,
        .dt-sc-event.type1 .dt-sc-event-meta:before,
        .dt-sc-event.type2:hover .dt-sc-event-meta,
        .dt-sc-event.type3 .dt-sc-event-date,
        .dt-sc-event.type3:hover .dt-sc-event-meta {
            background-color: #108bea;
        }

        .dt-sc-event.type4 .dt-sc-event-date:after {
            border-bottom-color: #108bea;
        }

        .dt-sc-event.type1 .dt-sc-event-meta p span,
        .dt-sc-event.type1:hover h2.entry-title a,
        .dt-sc-event.type3:hover h2.entry-title a,
        .dt-sc-event.type4 .dt-sc-event-date span {
            color: #108bea;
        }

        .widget.tribe_mini_calendar_widget .tribe-mini-calendar thead.tribe-mini-calendar-nav td,
        .widget.tribe_mini_calendar_widget .tribe-mini-calendar .tribe-events-present,
        .widget.tribe_mini_calendar_widget .tribe-mini-calendar .tribe-events-has-events.tribe-mini-calendar-today,
        .tribe-mini-calendar .tribe-events-has-events.tribe-events-present a:hover,
        .widget.tribe_mini_calendar_widget .tribe-mini-calendar td.tribe-events-has-events.tribe-mini-calendar-today a:hover,
        .dt-sc-dark-bg .widget.tribe_mini_calendar_widget .tribe-mini-calendar .tribe-events-present,
        .dt-sc-dark-bg .widget.tribe_mini_calendar_widget .tribe-mini-calendar .tribe-events-has-events.tribe-mini-calendar-today,
        .dt-sc-dark-bg .tribe-mini-calendar .tribe-events-has-events.tribe-events-present a:hover,
        .dt-sc-dark-bg .widget.tribe_mini_calendar_widget .tribe-mini-calendar td.tribe-events-has-events.tribe-mini-calendar-today a:hover {
            background-color: #108bea;
        }

        .widget.tribe_mini_calendar_widget .tribe-mini-calendar thead.tribe-mini-calendar-nav td {
            border-color: #108bea;
        }

        .widget.tribe-events-countdown-widget .tribe-countdown-text a:hover {
            color: #108bea;
        }

        .woocommerce a.button,
        .woocommerce button.button,
        .woocommerce button,
        .woocommerce input.button,
        .woocommerce input[type=button],
        .woocommerce input[type=submit],
        .woocommerce #respond input#submit,
        .woocommerce a.button.alt,
        .woocommerce button.button.alt,
        .woocommerce input.button.alt,
        .woocommerce #respond input#submit.alt,
        .woocommerce .product .summary .add_to_wishlist,
        .woocommerce .wishlist_table .add_to_cart.button,
        .woocommerce .yith-wcwl-add-button a.add_to_wishlist,
        .woocommerce .yith-wcwl-popup-button a.add_to_wishlist,
        .woocommerce .wishlist_table a.ask-an-estimate-button,
        .woocommerce .wishlist-title a.show-title-form,
        .woocommerce .hidden-title-form a.hide-title-form,
        .woocommerce .yith-wcwl-wishlist-new button,
        .woocommerce .wishlist_manage_table a.create-new-wishlist,
        .woocommerce .wishlist_manage_table button.submit-wishlist-changes,
        .woocommerce .yith-wcwl-wishlist-search-form button.wishlist-search-button,
        .woocommerce .cart input.button,
        .woocommerce .shop_table th,
        .woocommerce div.product .woocommerce-tabs ul.tabs li.active a:after,
        .woocommerce-page a.button,
        .woocommerce-page button.button,
        .woocommerce-page button,
        .woocommerce-page input.button,
        .woocommerce-page input[type=button],
        .woocommerce-page input[type=submit],
        .woocommerce-page #respond input#submit,
        .woocommerce-page a.button.alt,
        .woocommerce-page button.button.alt,
        .woocommerce-page input.button.alt,
        .woocommerce-page #respond input#submit.alt,
        .woocommerce-page .product .summary .add_to_wishlist,
        .woocommerce-page .wishlist_table .add_to_cart.button,
        .woocommerce-page .yith-wcwl-add-button a.add_to_wishlist,
        .woocommerce-page .yith-wcwl-popup-button a.add_to_wishlist,
        .woocommerce-page .wishlist_table a.ask-an-estimate-button,
        .woocommerce-page .wishlist-title a.show-title-form,
        .woocommerce-page .hidden-title-form a.hide-title-form,
        .woocommerce-page .yith-wcwl-wishlist-new button,
        .woocommerce-page .wishlist_manage_table a.create-new-wishlist,
        .woocommerce-page .wishlist_manage_table button.submit-wishlist-changes,
        .woocommerce-page .yith-wcwl-wishlist-search-form button.wishlist-search-button,
        .woocommerce-page .cart input.button,
        .woocommerce-page .shop_table th,
        .woocommerce-page div.product .woocommerce-tabs ul.tabs li.active a:after,
        .woocommerce ul.products li.product .featured-tag,
        .woocommerce ul.products li.product:hover .featured-tag,
        .woocommerce.single-product .featured-tag,
        .woocommerce .widget_price_filter .price_slider_wrapper .ui-widget-content {
            background-color: #108bea;
        }

        .woocommerce ul.products li.product .featured-tag:after,
        .woocommerce ul.products li.product:hover .featured-tag:after,
        .woocommerce.single-product .featured-tag:after {
            border-color: #108bea;
        }

        .woocommerce-checkout #payment ul.payment_methods li a:hover {
            color: #108bea;
        }

        .woo-type1 ul.products li.product .product-thumb a.add_to_cart_button:hover,
        .woo-type1 ul.products li.product .product-thumb a.button.product_type_simple:hover,
        .woo-type1 ul.products li.product .product-thumb a.button.product_type_variable:hover,
        .woo-type1 ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover,
        .woo-type1 ul.products li.product .product-thumb a.add_to_wishlist:hover,
        .woo-type1 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover,
        .woo-type1 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover {
            background-color: #108bea;
        }

        .woo-type1 ul.products li.product-category:hover .product-details h5,
        .woo-type1 ul.products li.product-category:hover .product-details h5 .count,
        .woo-type1 ul.products li.product .product-details .product-price .amount,
        .woo-type1 ul.products li.product .product-details span.price,
        .woo-type1 ul.products li.product .product-details span.price del,
        .woo-type1 ul.products li.product .product-details span.price del .amount,
        .woo-type1 ul.products li.product .product-details span.price ins,
        .woo-type1 ul.products li.product .product-details span.price ins .amount,
        .woo-type1.woocommerce.single-product .product .summary .product_meta a:hover,
        .woo-type1.woocommerce div.product .woocommerce-tabs ul.tabs li.active a {
            color: #108bea;
        }

        .woo-type2 ul.products li.product .product-thumb a.add_to_cart_button,
        .woo-type2 ul.products li.product .product-thumb a.button.product_type_simple,
        .woo-type2 ul.products li.product .product-thumb a.button.product_type_variable,
        .woo-type2 ul.products li.product .product-thumb a.added_to_cart.wc-forward,
        .woo-type2 ul.products li.product .product-thumb a.add_to_wishlist,
        .woo-type2 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a,
        .woo-type2 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a,
        .woo-type2.woocommerce ul.products li.product .onsale,
        .woo-type2.woocommerce ul.products li.product:hover .onsale,
        .woo-type2 .woocommerce ul.products li.product .onsale,
        .woo-type2 .woocommerce ul.products li.product:hover .onsale,
        .woo-type2.woocommerce ul.products li.product .out-of-stock,
        .woo-type2.woocommerce ul.products li.product:hover .out-of-stock,
        .woo-type2 .woocommerce ul.products li.product .out-of-stock,
        .woo-type2 .woocommerce ul.products li.product:hover .out-of-stock,
        .woo-type2.woocommerce span.onsale,
        .woo-type2.woocommerce span.out-of-stock,
        .woo-type2 .woocommerce span.onsale,
        .woo-type2 .woocommerce span.out-of-stock,
        .woo-type2 div.product .woocommerce-tabs ul.tabs li.active a {
            background-color: #0561aa;
        }

        .woo-type3 ul.products li.product .product-details h5:after {
            background-color: #108bea;
        }

        .woo-type3 ul.products li.product-category:hover .product-details h5,
        .woo-type3 ul.products li.product-category:hover .product-details h5 .count {
            color: #108bea;
        }

        .woo-type4 ul.products li.product .product-thumb a.add_to_cart_button:after,
        .woo-type4 ul.products li.product .product-thumb a.button.product_type_simple:after,
        .woo-type4 ul.products li.product .product-thumb a.button.product_type_variable:after,
        .woo-type4 ul.products li.product .product-thumb a.added_to_cart.wc-forward:after,
        .woo-type4 ul.products li.product .product-thumb a.add_to_wishlist:after,
        .woo-type4 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:after,
        .woo-type4 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:after,
        .woo-type4 ul.products li.product .product-details h5:after {
            background-color: #108bea;
        }

        .woo-type4 ul.products li.product-category:hover .product-details h5,
        .woo-type4 ul.products li.product-category:hover .product-details h5 .count {
            color: #108bea;
        }

        .woo-type5 ul.products li.product .product-thumb a.add_to_cart_button,
        .woo-type5 ul.products li.product .product-thumb a.button.product_type_simple,
        .woo-type5 ul.products li.product .product-thumb a.button.product_type_variable,
        .woo-type5 ul.products li.product .product-thumb a.added_to_cart.wc-forward,
        .woo-type5 ul.products li.product .product-thumb a.add_to_wishlist,
        .woo-type5 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a,
        .woo-type5 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a,
        .woo-type5 ul.products li.product .product-details h5:after {
            background-color: #108bea;
        }

        .woo-type5 ul.products li.product-category:hover .product-details h5,
        .woo-type5 ul.products li.product-category:hover .product-details h5 .count {
            color: #108bea;
        }

        .woo-type6 ul.products li.product .price {
            background-color: #108bea;
        }

        .woo-type6 ul.products li.product .product-thumb a.add_to_cart_button:hover:before,
        .woo-type6 ul.products li.product .product-thumb a.button.product_type_simple:hover:before,
        .woo-type6 ul.products li.product .product-thumb a.button.product_type_variable:hover:before,
        .woo-type6 ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover:before,
        .woo-type6 ul.products li.product .product-thumb a.add_to_wishlist:hover:before,
        .woo-type6 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover:before,
        .woo-type6 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover:before,
        .woo-type6 ul.products li.product-category:hover .product-details h5,
        .woo-type6 ul.products li.product-category:hover .product-details h5 .count {
            color: #108bea;
        }

        .woo-type7 ul.products li.product .product-thumb a.add_to_cart_button,
        .woo-type7 ul.products li.product .product-thumb a.button.product_type_simple,
        .woo-type7 ul.products li.product .product-thumb a.button.product_type_variable,
        .woo-type7 ul.products li.product .product-thumb a.added_to_cart.wc-forward,
        .woo-type7 ul.products li.product .product-details,
        .woo-type7 ul.products li.product:hover .product-details h5 {
            background-color: #108bea;
        }

        .woo-type8 ul.products li.product .product-details,
        .woo-type8 ul.products li.product:hover .product-details h5:before {
            background-color: #108bea;
        }

        .woo-type8 ul.products li.product .product-thumb a.add_to_cart_button:hover:before,
        .woo-type8 ul.products li.product .product-thumb a.button.product_type_simple:hover:before,
        .woo-type8 ul.products li.product .product-thumb a.button.product_type_variable:hover:before,
        .woo-type8 ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover:before,
        .woo-type8 ul.products li.product .product-thumb a.add_to_wishlist:hover:before,
        .woo-type8 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover:before,
        .woo-type8 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover:before,
        .woo-type8 ul.products li.product:hover .product-details h5 a:hover {
            color: #108bea;
        }

        .woo-type9 ul.products li.product .product-thumb a.add_to_cart_button:hover:after,
        .woo-type9 ul.products li.product .product-thumb a.button.product_type_simple:hover:after,
        .woo-type9 ul.products li.product .product-thumb a.button.product_type_variable:hover:after,
        .woo-type9 ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover:after,
        .woo-type9 ul.products li.product .product-thumb a.add_to_wishlist:hover:after,
        .woo-type9 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover:after,
        .woo-type9 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover:after,
        .woo-type9 ul.products li.product .product-details {
            background-color: #108bea;
        }

        .woo-type10 ul.products li.product .product-thumb a.add_to_cart_button,
        .woo-type10 ul.products li.product .product-thumb a.button.product_type_simple,
        .woo-type10 ul.products li.product .product-thumb a.button.product_type_variable,
        .woo-type10 ul.products li.product .product-thumb a.added_to_cart.wc-forward,
        .woo-type10 ul.products li.product .product-thumb a.add_to_wishlist,
        .woo-type10 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a,
        .woo-type10 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a,
        .woo-type10 ul.products li.product:hover .product-details {
            background-color: #108bea;
        }

        .woo-type10 ul.products li.product:hover .product-wrapper {
            border-color: #108bea;
        }

        .woo-type10 ul.products li.product:hover .product-details:before,
        .woo-type10 ul.products li.product:hover .product-details:after {
            border-bottom-color: #108bea;
        }

        .woo-type11 ul.products li.product .product-thumb a.add_to_cart_button:hover,
        .woo-type11 ul.products li.product .product-thumb a.button.product_type_simple:hover,
        .woo-type11 ul.products li.product .product-thumb a.button.product_type_variable:hover,
        .woo-type11 ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover,
        .woo-type11 ul.products li.product .product-thumb a.add_to_wishlist:hover,
        .woo-type11 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover,
        .woo-type11 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover,
        .woo-type11.woocommerce div.product .woocommerce-tabs ul.tabs li.active a:after,
        .woo-type11 ul.products li.product .product-details {
            background-color: #108bea;
        }

        .woo-type11 ul.products li.product .product-thumb a.add_to_cart_button:before,
        .woo-type11 ul.products li.product .product-thumb a.button.product_type_simple:before,
        .woo-type11 ul.products li.product .product-thumb a.button.product_type_variable:before,
        .woo-type11 ul.products li.product .product-thumb a.added_to_cart.wc-forward:before,
        .woo-type11 ul.products li.product .product-thumb a.add_to_wishlist:before,
        .woo-type11 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:before,
        .woo-type11 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:before {
            color: #108bea;
        }

        .woo-type12 ul.products li.product .product-thumb a.add_to_cart_button,
        .woo-type12 ul.products li.product .product-thumb a.button.product_type_simple,
        .woo-type12 ul.products li.product .product-thumb a.button.product_type_variable,
        .woo-type12 ul.products li.product .product-thumb a.added_to_cart.wc-forward,
        .woo-type12 ul.products li.product .product-thumb a.add_to_wishlist,
        .woo-type12 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a,
        .woo-type12 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a,
        .woo-type12 ul.products li.product:hover .product-details,
        .woo-type12 ul.products li.product .product-details h5:after {
            background-color: #108bea;
        }

        .woo-type13 ul.products li.product .product-details h5:before {
            background-color: #108bea;
        }

        .woo-type13 ul.products li.product .product-thumb a.add_to_cart_button:hover:before,
        .woo-type13 ul.products li.product .product-thumb a.button.product_type_simple:hover:before,
        .woo-type13 ul.products li.product .product-thumb a.button.product_type_variable:hover:before,
        .woo-type13 ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover:before,
        .woo-type13 ul.products li.product .product-thumb a.add_to_wishlist:hover:before,
        .woo-type13 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover:before,
        .woo-type13 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover:before,
        .woo-type13 ul.products li.product:hover .product-details h5 a,
        .woo-type13 ul.products li.product-category:hover .product-details h5,
        .woo-type13 ul.products li.product-category:hover .product-details h5 .count {
            color: #108bea;
        }

        .woo-type14 ul.products li.product:hover .product-details,
        .woo-type14 ul.products li.product .product-details h5:before,
        .woo-type14 ul.products li.product:hover .product-details h5:after {
            background-color: #108bea;
        }

        .woo-type14 ul.products li.product:hover .product-details h5:after {
            border-color: #108bea;
        }

        .woo-type15 ul.products li.product .product-thumb a.add_to_cart_button:after,
        .woo-type15 ul.products li.product .product-thumb a.button.product_type_simple:after,
        .woo-type15 ul.products li.product .product-thumb a.button.product_type_variable:after,
        .woo-type15 ul.products li.product .product-thumb a.added_to_cart.wc-forward:after,
        .woo-type15 ul.products li.product .product-thumb a.add_to_wishlist:after,
        .woo-type15 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:after,
        .woo-type15 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:after,
        .woo-type15 ul.products li.product .price:after {
            background-color: #108bea;
        }

        .woo-type15 ul.products li.product:hover .product-wrapper {
            border-color: #108bea;
        }

        .woo-type16 ul.products li.product .product-wrapper:before,
        .woo-type16 ul.products li.product .product-thumb a.add_to_cart_button:hover,
        .woo-type16 ul.products li.product .product-thumb a.button.product_type_simple:hover,
        .woo-type16 ul.products li.product .product-thumb a.button.product_type_variable:hover,
        .woo-type16 ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover,
        .woo-type16 ul.products li.product .product-thumb a.add_to_wishlist:hover,
        .woo-type16 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover,
        .woo-type16 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover,
        .woo-type16.woocommerce .shop_table th,
        .woo-type16 .woocommerce .shop_table th,
        .woo-type16.woocommerce div.product .woocommerce-tabs ul.tabs li.active a:after {
            background-color: #108bea;
        }

        .woo-type17 ul.products li.product .product-thumb a.add_to_cart_button:hover:after,
        .woo-type17 ul.products li.product .product-thumb a.button.product_type_simple:hover:after,
        .woo-type17 ul.products li.product .product-thumb a.button.product_type_variable:hover:after,
        .woo-type17 ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover:after,
        .woo-type17 ul.products li.product .product-thumb a.add_to_wishlist:hover:after,
        .woo-type17 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover:after,
        .woo-type17 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover:after,
        .woo-type17 ul.products li.product:hover .product-details {
            background-color: #108bea;
        }

        .woo-type17 ul.products li.product:hover .product-wrapper,
        .woo-type17 ul.products li.product:hover .product-thumb a.add_to_cart_button:after,
        .woo-type17 ul.products li.product:hover .product-thumb a.button.product_type_simple:after,
        .woo-type17 ul.products li.product:hover .product-thumb a.button.product_type_variable:after,
        .woo-type17 ul.products li.product:hover .product-thumb a.added_to_cart.wc-forward:after,
        .woo-type17 ul.products li.product:hover .product-thumb a.add_to_wishlist:after,
        .woo-type17 ul.products li.product:hover .product-thumb .yith-wcwl-wishlistaddedbrowse a:after,
        .woo-type17 ul.products li.product:hover .product-thumb .yith-wcwl-wishlistexistsbrowse a:after,
        .woo-type17 ul.products li.product .product-details h5 a:after,
        .woo-type17 ul.products li.product-category .product-details h5:after,
        .woo-type17 ul.products li.product .price {
            border-color: #108bea;
        }

        .woo-type17 ul.products li.product .product-thumb a.add_to_cart_button,
        .woo-type17 ul.products li.product .product-thumb a.button.product_type_simple,
        .woo-type17 ul.products li.product .product-thumb a.button.product_type_variable,
        .woo-type17 ul.products li.product .product-thumb a.added_to_cart.wc-forward,
        .woo-type17 ul.products li.product .product-thumb a.add_to_wishlist,
        .woo-type17 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a,
        .woo-type17 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a,
        .woo-type17 ul.products li.product .product-thumb a.add_to_cart_button:before,
        .woo-type17 ul.products li.product .product-thumb a.button.product_type_simple:before,
        .woo-type17 ul.products li.product .product-thumb a.button.product_type_variable:before,
        .woo-type17 ul.products li.product .product-thumb a.added_to_cart.wc-forward:before,
        .woo-type17 ul.products li.product .product-thumb a.add_to_wishlist:before,
        .woo-type17 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:before,
        .woo-type17 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:before,
        .woo-type17 ul.products li.product .product-details h5 a,
        .woo-type17 ul.products li.product-category .product-details h5,
        .woo-type17 ul.products li.product-category .product-details h5 .count,
        .woo-type17 ul.products li.product .product-details .product-price .amount,
        .woo-type17 ul.products li.product .product-details span.price,
        .woo-type17 ul.products li.product .product-details span.price del,
        .woo-type17 ul.products li.product .product-details span.price del .amount,
        .woo-type17 ul.products li.product .product-details span.price ins,
        .woo-type17 ul.products li.product .product-details span.price ins .amount,
        .woo-type17 .widget.woocommerce ul li:hover:before {
            color: #108bea;
        }

        .woo-type18 ul.products li.product .product-thumb a.add_to_cart_button,
        .woo-type18 ul.products li.product .product-thumb a.button.product_type_simple,
        .woo-type18 ul.products li.product .product-thumb a.button.product_type_variable,
        .woo-type18 ul.products li.product .product-thumb a.added_to_cart.wc-forward,
        .woo-type18 ul.products li.product .product-thumb a.add_to_wishlist,
        .woo-type18 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a,
        .woo-type18 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a {
            background-color: #108bea;
        }

        .woo-type19 ul.products li.product:hover .product-wrapper,
        .woo-type19 ul.products li.product:hover .product-details {
            background-color: #108bea;
        }

        .woo-type19 ul.products li.product .product-thumb a.add_to_cart_button:hover:before,
        .woo-type19 ul.products li.product .product-thumb a.button.product_type_simple:hover:before,
        .woo-type19 ul.products li.product .product-thumb a.button.product_type_variable:hover:before,
        .woo-type19 ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover:before,
        .woo-type19 ul.products li.product .product-thumb a.add_to_wishlist:hover:before,
        .woo-type19 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover:before,
        .woo-type19 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover:before {
            color: #108bea;
        }

        .woo-type20 ul.products li.product .product-thumb a.add_to_cart_button,
        .woo-type20 ul.products li.product .product-thumb a.button.product_type_simple,
        .woo-type20 ul.products li.product .product-thumb a.button.product_type_variable,
        .woo-type20 ul.products li.product .product-thumb a.added_to_cart.wc-forward,
        .woo-type20 ul.products li.product .product-thumb a.add_to_wishlist,
        .woo-type20 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a,
        .woo-type20 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a,
        .woo-type20 ul.products li.product .product-wrapper:after,
        .woo-type20.woocommerce ul.products li.product .product-details h5,
        .woo-type20 .woocommerce ul.products li.product .product-details h5,
        .woo-type20 ul.products li.product-category .product-wrapper h3 {
            border-color: #108bea;
        }

        .woo-type20 ul.products li.product .product-thumb a.add_to_cart_button:before,
        .woo-type20 ul.products li.product .product-thumb a.button.product_type_simple:before,
        .woo-type20 ul.products li.product .product-thumb a.button.product_type_variable:before,
        .woo-type20 ul.products li.product .product-thumb a.added_to_cart.wc-forward:before,
        .woo-type20 ul.products li.product .product-thumb a.add_to_wishlist:before,
        .woo-type20 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:before,
        .woo-type20 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:before,
        .woo-type20 ul.products li.product .product-details h5 a,
        .woo-type20 ul.products li.product-category .product-details h5,
        .woo-type20 ul.products li.product-category .product-details h5 .count,
        .woo-type20 ul.products li.product .product-details .product-price .amount,
        .woo-type20 ul.products li.product .product-details span.price,
        .woo-type20 ul.products li.product .product-details span.price del,
        .woo-type20 ul.products li.product .product-details span.price del .amount,
        .woo-type20 ul.products li.product .product-details span.price ins,
        .woo-type20 ul.products li.product .product-details span.price ins .amount,
        .woo-type20 ul.products li.product .product-details .product-rating-wrapper .star-rating:before,
        .woo-type20 ul.products li.product .product-details .product-rating-wrapper .star-rating span:before {
            color: #108bea;
        }

        .woo-type21 .woocommerce ul.products li.product .product-thumb a.add_to_cart_button:hover,
        .woo-type21 .woocommerce ul.products li.product .product-thumb a.button.product_type_simple:hover,
        .woo-type21 .woocommerce ul.products li.product .product-thumb a.button.product_type_variable:hover,
        .woo-type21 .woocommerce ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover,
        .woo-type21 .woocommerce ul.products li.product .product-thumb a.add_to_wishlist:hover,
        .woo-type21 .woocommerce ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover,
        .woo-type21 .woocommerce ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover,
        .woo-type21.woocommerce ul.products li.product .product-thumb a.add_to_cart_button:hover,
        .woo-type21.woocommerce ul.products li.product .product-thumb a.button.product_type_simple:hover,
        .woo-type21.woocommerce ul.products li.product .product-thumb a.button.product_type_variable:hover,
        .woo-type21.woocommerce ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover,
        .woo-type21.woocommerce ul.products li.product .product-thumb a.add_to_wishlist:hover,
        .woo-type21.woocommerce ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover,
        .woo-type21.woocommerce ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover,
        .woo-type21 .woocommerce a.button:hover,
        .woo-type21 .woocommerce button.button:hover,
        .woo-type21 .woocommerce button:hover,
        .woo-type21 .woocommerce input.button:hover,
        .woo-type21 .woocommerce input[type=button]:hover,
        .woo-type21 .woocommerce input[type=submit]:hover,
        .woo-type21 .woocommerce #respond input#submit:hover,
        .woo-type21 .woocommerce a.button.alt:hover,
        .woo-type21 .woocommerce button.button.alt:hover,
        .woo-type21 .woocommerce input.button.alt:hover,
        .woo-type21 .woocommerce #respond input#submit.alt:hover,
        .woo-type21 .woocommerce .product .summary .add_to_wishlist:hover,
        .woo-type21 .woocommerce .wishlist_table .add_to_cart.button:hover,
        .woo-type21 .woocommerce .yith-wcwl-add-button a.add_to_wishlist:hover,
        .woo-type21 .woocommerce .yith-wcwl-popup-button a.add_to_wishlist:hover,
        .woo-type21 .woocommerce .wishlist_table a.ask-an-estimate-button:hover,
        .woo-type21 .woocommerce .wishlist-title a.show-title-form:hover,
        .woo-type21 .woocommerce .hidden-title-form a.hide-title-form:hover,
        .woo-type21 .woocommerce .yith-wcwl-wishlist-new button:hover,
        .woo-type21 .woocommerce .wishlist_manage_table a.create-new-wishlist:hover,
        .woo-type21 .woocommerce .wishlist_manage_table button.submit-wishlist-changes:hover,
        .woo-type21 .woocommerce .yith-wcwl-wishlist-search-form button.wishlist-search-button:hover,
        .woo-type21 .woocommerce .cart input.button:hover,
        .woo-type21.woocommerce a.button:hover,
        .woo-type21.woocommerce button.button:hover,
        .woo-type21.woocommerce button:hover,
        .woo-type21.woocommerce input.button:hover,
        .woo-type21.woocommerce input[type=button]:hover,
        .woo-type21.woocommerce input[type=submit]:hover,
        .woo-type21.woocommerce #respond input#submit:hover,
        .woo-type21.woocommerce a.button.alt:hover,
        .woo-type21.woocommerce button.button.alt:hover,
        .woo-type21.woocommerce input.button.alt:hover,
        .woo-type21.woocommerce #respond input#submit.alt:hover,
        .woo-type21.woocommerce .product .summary .add_to_wishlist:hover,
        .woo-type21.woocommerce .wishlist_table .add_to_cart.button:hover,
        .woo-type21.woocommerce .yith-wcwl-add-button a.add_to_wishlist:hover,
        .woo-type21.woocommerce .yith-wcwl-popup-button a.add_to_wishlist:hover,
        .woo-type21.woocommerce .wishlist_table a.ask-an-estimate-button:hover,
        .woo-type21.woocommerce .wishlist-title a.show-title-form:hover,
        .woo-type21.woocommerce .hidden-title-form a.hide-title-form:hover,
        .woo-type21.woocommerce .yith-wcwl-wishlist-new button:hover,
        .woo-type21.woocommerce .wishlist_manage_table a.create-new-wishlist:hover,
        .woo-type21.woocommerce .wishlist_manage_table button.submit-wishlist-changes:hover,
        .woo-type21.woocommerce .yith-wcwl-wishlist-search-form button.wishlist-search-button:hover,
        .woo-type21.woocommerce .cart input.button:hover,
        .woo-type21 .woocommerce .product .summary .add_to_wishlist:hover:before,
        .woo-type21.woocommerce .product .summary .add_to_wishlist:hover:before {
            background-color: #108bea;
        }

        .woo-type21 .woocommerce .product .summary .add_to_wishlist:hover,
        .woo-type21.woocommerce .product .summary .add_to_wishlist:hover {
            color: #108bea;
        }

        .dt-sc-attorney-sorting a.active-sort,
        .dt-sc-attorneys-single h5.dt-sc-attorney-role:before,
        .dt-sc-attorneys-single h5.dt-sc-attorney-role:after,
        .dt-sc-attorney .dt-sc-attorney-details h5:before,
        .dt-sc-attorney .dt-sc-attorney-details h5:after,
        .widget.widget_attroney .widgettitle:before,
        .attorney-contact-form-widget input[type="submit"]:hover,
        .dt-sc-practices-list li a:hover,
        .dt-sc-attorney-location-overlay p:last-child a {
            background-color: #108bea;
        }

        .dt-sc-attorney-location-overlay:before {
            border-top-color: #108bea;
        }

        .dt-sc-attorney-sorting a:hover,
        .dt-sc-attorneys-single h6,
        .dt-sc-attorney .dt-sc-attorney-details h6,
        .dt-sc-attorneys-single a.read-more:hover,
        .dt-sc-attorney .dt-sc-attorney-details a.read-more:hover,
        .widget.widget_attroney .widgettitle,
        .widget.widget_attorney_practice .attorney-practice-widget ul li a:hover,
        .attorney-contact-form-widget input[type="submit"],
        .dt-sc-attorney-location-overlay p a:hover,
        .dt-sc-loading:before {
            color: #108bea;
        }

        .dt-sc-chef-single-image-overlay .dt-sc-sociable li a,
        .dt-sc-chef-single-image-overlay .dt-sc-chef-single-special p a:before,
        .dt-sc-menu .menu-categories a:before {
            background-color: #108bea;
        }

        .dt-sc-menu-sorting a.active-sort,
        .dt-sc-menu .image-overlay .price {
            border-color: #108bea;
        }

        .dt-sc-chef .dt-sc-chef-details p,
        .dt-sc-chef .dt-sc-chef-details h6,
        .dt-sc-chef .dt-sc-chef-details .dt-sc-chef-category a:hover,
        .dt-sc-chef .dt-sc-chef-details .dt-sc-button:hover,
        .dt-sc-chef-single-details .dt-sc-chef-single-likes p a:hover,
        .dt-sc-chef-single-image-overlay .dt-sc-chef-single-special h6,
        .dt-sc-chef-single-image-overlay .dt-sc-sociable li a:hover,
        .dt-sc-chef-single-image-overlay .dt-sc-chef-single-special p a:hover,
        .dt-sc-menu .image-overlay .price,
        .dt-sc-menu .image-overlay h6 a:hover,
        .dt-sc-menu .menu-categories a:hover,
        .dt-sc-menu-sorting a:hover,
        .dt-sc-menu-sorting a.active-sort,
        .dt-sc-menu .dt-sc-menu-details .dt-sc-menu-price,
        .dt-sc-restaurant-events-list .dt-sc-restaurant-event-details p {
            color: #108bea;
        }

        .dt-sc-yoga-video .dt-sc-yoga-video-meta,
        .dt-sc-yoga-classes:hover .dt-sc-yoga-classes-details,
        .dt-sc-yoga-classes-sorting a,
        .dt-sc-yoga-pose:hover .dt-sc-yoga-pose-details,
        .dt-sc-yoga-teacher:hover .dt-sc-yoga-teacher-details {
            background-color: #108bea;
        }

        .dt-sc-yoga-people-single .dt-sc-single-line-dashed-separator,
        .dt-sc-yoga-program-single:hover,
        .dt-sc-yoga-pose-single .entry-thumb:hover,
        .dt-sc-yoga-style-single .entry-thumb:hover {
            border-color: #108bea;
        }

        .dt-sc-yoga-classes .dt-sc-yoga-classes-image-overlay a,
        .dt-sc-yoga-pose .dt-sc-yoga-pose-overlay a,
        .dt-sc-yoga-teacher .dt-sc-yoga-teacher-overlay a,
        .dt-sc-yoga-courses-sorting a:hover,
        .dt-sc-yoga-course .dt-sc-yoga-course-details h5 a:hover,
        .dt-sc-yoga-course .dt-sc-yoga-course-details h6 a:hover,
        .dt-sc-yoga-course .dt-sc-yoga-course-details .price,
        .dt-sc-yoga-course .dt-sc-yoga-course-thumb-overlay a,
        .dt-sc-yoga-program .dt-sc-yoga-program-details h5 a:hover,
        .dt-sc-yoga-program .dt-sc-yoga-program-details h6 a:hover,
        .dt-sc-yoga-program .dt-sc-yoga-program-thumb-overlay a,
        .dt-sc-yoga-people-info h4 {
            color: #108bea;
        }

        .dt-sc-doctors-sorting a.active-sort,
        .dt-sc-doctors .dt-sc-doctors-details ul.dt-sc-sociable li a {
            background-color: #108bea;
        }

        .dt-sc-doctors-sorting a.active-sort {
            border-color: #108bea;
        }

        .dt-sc-doctors-sorting a:hover,
        .dt-sc-doctors .dt-sc-doctors-details h5 a,
        .dt-sc-doctors .dt-sc-doctors-details ul.dt-sc-doctors-meta li span,
        .dt-sc-doctors .dt-sc-doctors-details ul.dt-sc-doctors-meta li a:hover,
        .dt-sc-doctors-single-meta li span {
            color: #108bea;
        }

        .dt-sc-event-addon-detail>a.buy-now {
            background-color: #108bea;
        }

        .dt-sc-dj-club h2 {
            border-color: #108bea;
        }

        .dt-sc-event-sorting a.active-sort,
        .dt-sc-event-sorting a:hover,
        .dt-sc-event-addon-detail ul li span,
        .dt-sc-event-month:hover h2 a,
        .dt-sc-dj-profile .dt-sc-dj-profile-details h3,
        .dt-sc-dj-profile .dt-sc-dj-profile-details .dt-sc-dj-profile-meta p,
        .dt-sc-dj-club h4 {
            color: #108bea;
        }

        .model-nav-container .model-next-link a:hover,
        .model-nav-container .model-prev-link a:hover {
            background-color: #108bea;
            border-color: #108bea;
        }

        .dt-sc-model figcaption h3 a:hover,
        .dt-sc-model-sorting a:hover,
        .dt-sc-model-sorting a.active-sort,
        .dt-sc-model-details li span,
        .dt-sc-model-single-slider-wrapper .bx-controls a:hover:before {
            color: #108bea;
        }

        .dt-sc-hotel-room-sorting a:before,
        .dt-sc-hotel-room-sorting a.active-sort:before,
        .dt-sc-hotel-room-single-metadata form input[type="submit"]:hover,
        .dt-sc-hotel-room-single-metadata form p:before {
            background-color: #108bea;
        }

        .dt-sc-hotel-room-single-metadata form,
        .dt-sc-hotel-room-single-metadata form input[type="submit"] {
            border-color: #108bea;
        }

        .dt-sc-hotel-room .dt-sc-hotel-room-thumb {
            border-top-color: #108bea;
        }

        .dt-sc-hotel-room .dt-sc-hotel-room-thumb-overlay p .price,
        .dt-sc-hotel-room .dt-sc-hotel-room-buttons a:hover,
        .dt-sc-hotel-room .dt-sc-hotel-room-details .dt-sc-hotel-room-content h4,
        .dt-sc-hotel-room-single-details h3,
        .dt-sc-hotel-room-single-details ul li:before,
        .dt-sc-hotel-room-single-metadata .price-wrapper p .price,
        .dt-sc-hotel-room-single-metadata ul.dt-sc-sociable li a:hover {
            color: #108bea;
        }

        .dt-sc-training-details-overlay,
        .dt-sc-trainers .dt-sc-sociable,
        .dt-sc-workouts .dt-sc-workouts-details a span,
        .dt-sc-workouts .dt-sc-workouts-details a:hover,
        .dt-sc-fitness-program figure figcaption,
        .dt-sc-fitness-program-nav li a:before,
        .dt-sc-fitness-program-nav li.current_page_item a,
        .dt-sc-fitness-program-nav li a:after,
        .dt-sc-fitness-program-details h4:before,
        .dt-sc-fitness-program-details .dt-sc-pr-tb-col .dt-sc-buy-now a {
            background-color: #108bea;
        }

        .dt-sc-trainers:hover,
        .dt-sc-trainers:hover .dt-sc-trainers-title,
        .dt-sc-process-steps .dt-sc-process-thumb img {
            border-color: #108bea;
        }

        .dt-sc-training-details h6,
        .dt-sc-trainers .dt-sc-trainers-title h6,
        .dt-sc-workouts .dt-sc-workouts-details h4,
        .dt-sc-fitness-program-sorting a:hover,
        .dt-sc-fitness-program-sorting a.active-sort,
        .dt-sc-fitness-program .dt-sc-fitness-program-price sup,
        .dt-sc-fitness-program .dt-sc-fitness-program-price sub,
        .dt-sc-fitness-program-nav li a:hover,
        .dt-sc-fitness-program-details .dt-sc-pr-tb-col .dt-sc-price h6 sup,
        .dt-sc-fitness-diet-result p {
            color: #108bea;
        }

        .has-secondary-background-color {
            background-color: #58aff2;
        }

        .has-secondary-color {
            color: #58aff2;
        }

        input[type="submit"]:hover,
        button:hover,
        input[type="reset"]:hover,
        .mz-blog .comments a:hover,
        .mz-blog div.vc_gitem-post-category-name:hover,
        .dt-sc-infinite-portfolio-load-more:hover,
        .dt-sc-button.filled:hover,
        .dt-sc-button.with-icon.icon-right.type1:hover,
        .dt-sc-counter.type2:hover .dt-sc-couter-icon-holder,
        .dt-sc-newsletter-section.type2 .dt-sc-subscribe-frm input[type="submit"]:hover,
        .skin-highlight .dt-sc-testimonial.type6 .dt-sc-testimonial-author:before,
        .skin-highlight .dt-sc-testimonial.type6:after,
        .dt-sc-team-social.rounded-square li a:hover,
        .dt-sc-video-wrapper .video-overlay-inner a:hover,
        #dt-consent-extra-info.dt-inline-modal .mfp-close:hover {
            background-color: #58aff2;
        }

        .dt-sc-contact-info.type5 .dt-sc-contact-icon,
        .dt-sc-contact-info.type5 .dt-sc-contact-icon:before,
        .dt-sc-contact-info.type5 .dt-sc-contact-icon:after,
        .dt-sc-image-caption.type2:hover .dt-sc-image-content,
        .dt-sc-newsletter-section.type2 .dt-sc-subscribe-frm input[type="email"],
        .dt-sc-sociable.hexagon-with-border li,
        .dt-sc-sociable.hexagon-with-border li:before,
        .dt-sc-sociable.hexagon-with-border li:after {
            border-color: #58aff2;
        }

        .error404 .type2 a.dt-sc-back:hover,
        .error404 .type4 .dt-sc-newsletter-section input[type="submit"]:hover {
            background-color: #58aff2;
        }

        #item-header-content #item-meta>#item-buttons .group-button:hover,
        #buddypress .activity-list li.load-more a:hover,
        #buddypress .activity-list li.load-newest a:hover {
            background-color: #58aff2;
        }

        #bbpress-forums #subscription-toggle a.subscription-toggle:hover,
        .bbp-submit-wrapper #bbp_topic_submit:hover {
            background-color: #58aff2;
        }

        #tribe-bar-form .tribe-bar-submit input[type="submit"]:hover,
        .tribe-events-read-more:hover,
        #tribe-events .tribe-events-button:hover,
        .tribe-events-button:hover,
        .tribe-events-back>a:hover,
        .datepicker thead tr:first-child th:hover,
        .datepicker tfoot tr th:hover,
        #tribe_events_filters_toggle:hover {
            background-color: #58aff2;
        }

        .tribe-grid-header .tribe-week-today {
            background-color: #58aff2;
        }

        .woocommerce a.button:hover,
        .woocommerce button.button:hover,
        .woocommerce button:hover,
        .woocommerce input.button:hover,
        .woocommerce input[type=button]:hover,
        .woocommerce input[type=submit]:hover,
        .woocommerce #respond input#submit:hover,
        .woocommerce a.button.alt:hover,
        .woocommerce button.button.alt:hover,
        .woocommerce input.button.alt:hover,
        .woocommerce #respond input#submit.alt:hover,
        .woocommerce .product .summary .add_to_wishlist:hover,
        .woocommerce .wishlist_table .add_to_cart.button:hover,
        .woocommerce .yith-wcwl-add-button a.add_to_wishlist:hover,
        .woocommerce .yith-wcwl-popup-button a.add_to_wishlist:hover,
        .woocommerce .wishlist_table a.ask-an-estimate-button:hover,
        .woocommerce .wishlist-title a.show-title-form:hover,
        .woocommerce .hidden-title-form a.hide-title-form:hover,
        .woocommerce .yith-wcwl-wishlist-new button:hover,
        .woocommerce .wishlist_manage_table a.create-new-wishlist:hover,
        .woocommerce .wishlist_manage_table button.submit-wishlist-changes:hover,
        .woocommerce .yith-wcwl-wishlist-search-form button.wishlist-search-button:hover,
        .woocommerce .cart input.button:hover,
        .woocommerce-page a.button:hover,
        .woocommerce-page button.button:hover,
        .woocommerce-page button:hover,
        .woocommerce-page input.button:hover,
        .woocommerce-page input[type=button]:hover,
        .woocommerce-page input[type=submit]:hover,
        .woocommerce-page #respond input#submit:hover,
        .woocommerce-page a.button.alt:hover,
        .woocommerce-page button.button.alt:hover,
        .woocommerce-page input.button.alt:hover,
        .woocommerce-page #respond input#submit.alt:hover,
        .woocommerce-page .product .summary .add_to_wishlist:hover,
        .woocommerce-page .wishlist_table .add_to_cart.button:hover,
        .woocommerce-page .yith-wcwl-add-button a.add_to_wishlist:hover,
        .woocommerce-page .yith-wcwl-popup-button a.add_to_wishlist:hover,
        .woocommerce-page .wishlist_table a.ask-an-estimate-button:hover,
        .woocommerce-page .wishlist-title a.show-title-form:hover,
        .woocommerce-page .hidden-title-form a.hide-title-form:hover,
        .woocommerce-page .yith-wcwl-wishlist-new button:hover,
        .woocommerce-page .wishlist_manage_table a.create-new-wishlist:hover,
        .woocommerce-page .wishlist_manage_table button.submit-wishlist-changes:hover,
        .woocommerce-page .yith-wcwl-wishlist-search-form button.wishlist-search-button:hover,
        .woocommerce-page .cart input.button:hover,
        .woocommerce a.button.alt.disabled,
        .woocommerce a.button.alt:disabled,
        .woocommerce a.button.alt[disabled]:disabled,
        .woocommerce button.button.alt.disabled,
        .woocommerce button.button.alt:disabled,
        .woocommerce button.button.alt[disabled]:disabled,
        .woocommerce input.button.alt.disabled,
        .woocommerce input.button.alt:disabled,
        .woocommerce input.button.alt[disabled]:disabled,
        .woocommerce #respond input#submit.alt.disabled,
        .woocommerce #respond input#submit.alt:disabled,
        .woocommerce #respond input#submit.alt[disabled]:disabled,
        .woocommerce a.button.alt.disabled:hover,
        .woocommerce a.button.alt:disabled:hover,
        .woocommerce a.button.alt[disabled]:disabled:hover,
        .woocommerce button.button.alt.disabled:hover,
        .woocommerce button.button.alt:disabled:hover,
        .woocommerce button.button.alt[disabled]:disabled:hover,
        .woocommerce input.button.alt.disabled:hover,
        .woocommerce input.button.alt:disabled:hover,
        .woocommerce input.button.alt[disabled]:disabled:hover,
        .woocommerce #respond input#submit.alt.disabled:hover,
        .woocommerce #respond input#submit.alt:disabled:hover,
        .woocommerce #respond input#submit.alt[disabled]:disabled:hover,
        .woocommerce a.button.disabled:hover,
        .woocommerce a.button:disabled:hover,
        .woocommerce a.button:disabled[disabled]:hover,
        .woocommerce button.button.disabled:hover,
        .woocommerce button.button:disabled:hover,
        .woocommerce button.button:disabled[disabled]:hover,
        .woocommerce input.button.disabled:hover,
        .woocommerce input.button:disabled:hover,
        .woocommerce input.button:disabled[disabled]:hover,
        .woocommerce #respond input#submit.disabled:hover,
        .woocommerce #respond input#submit:disabled:hover,
        .woocommerce #respond input#submit:disabled[disabled]:hover {
            background-color: #58aff2;
        }

        .woo-type3 ul.products li.product .product-thumb a.add_to_cart_button:hover,
        .woo-type3 ul.products li.product .product-thumb a.button.product_type_simple:hover,
        .woo-type3 ul.products li.product .product-thumb a.button.product_type_variable:hover,
        .woo-type3 ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover,
        .woo-type3 ul.products li.product .product-thumb a.add_to_wishlist:hover,
        .woo-type3 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover,
        .woo-type3 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover {
            background-color: #58aff2;
        }

        .woo-type4 ul.products li.product .product-thumb a.add_to_cart_button:hover:after,
        .woo-type4 ul.products li.product .product-thumb a.button.product_type_simple:hover:after,
        .woo-type4 ul.products li.product .product-thumb a.button.product_type_variable:hover:after,
        .woo-type4 ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover:after,
        .woo-type4 ul.products li.product .product-thumb a.add_to_wishlist:hover:after,
        .woo-type4 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover:after,
        .woo-type4 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover:after {
            background-color: #58aff2;
        }

        .woo-type5 ul.products li.product .product-thumb a.add_to_cart_button:hover,
        .woo-type5 ul.products li.product .product-thumb a.button.product_type_simple:hover,
        .woo-type5 ul.products li.product .product-thumb a.button.product_type_variable:hover,
        .woo-type5 ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover,
        .woo-type5 ul.products li.product .product-thumb a.add_to_wishlist:hover,
        .woo-type5 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover,
        .woo-type5 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover {
            background-color: #58aff2;
        }

        .woo-type7 ul.products li.product .product-details h5 {
            background-color: #58aff2;
        }

        .woo-type8 ul.products li.product:hover .product-wrapper {
            border-color: #58aff2;
        }

        .woo-type9 ul.products li.product .product-thumb a.add_to_cart_button:after,
        .woo-type9 ul.products li.product .product-thumb a.button.product_type_simple:after,
        .woo-type9 ul.products li.product .product-thumb a.button.product_type_variable:after,
        .woo-type9 ul.products li.product .product-thumb a.added_to_cart.wc-forward:after,
        .woo-type9 ul.products li.product .product-thumb a.add_to_wishlist:after,
        .woo-type9 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:after,
        .woo-type9 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:after,
        .woo-type9 ul.products li.product:hover .product-details {
            background-color: #58aff2;
        }

        .woo-type9 ul.products li.product:hover .product-details h5:after {
            border-color: #58aff2;
        }

        .woo-type10 ul.products li.product .product-thumb a.add_to_cart_button:hover,
        .woo-type10 ul.products li.product .product-thumb a.button.product_type_simple:hover,
        .woo-type10 ul.products li.product .product-thumb a.button.product_type_variable:hover,
        .woo-type10 ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover,
        .woo-type10 ul.products li.product .product-thumb a.add_to_wishlist:hover,
        .woo-type10 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover,
        .woo-type10 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover,
        .woo-type10.woocommerce ul.products li.product .featured-tag,
        .woo-type10 .woocommerce ul.products li.product .featured-tag,
        .woo-type10.woocommerce.single-product .featured-tag {
            background-color: #58aff2;
        }

        .woo-type10 ul.products li.product .featured-tag:after,
        .woo-type10 ul.products li.product:hover .featured-tag:after,
        .woo-type10.woocommerce.single-product .featured-tag:after {
            border-color: #58aff2;
        }

        .woo-type11 ul.products li.product .product-thumb a.add_to_cart_button,
        .woo-type11 ul.products li.product .product-thumb a.button.product_type_simple,
        .woo-type11 ul.products li.product .product-thumb a.button.product_type_variable,
        .woo-type11 ul.products li.product .product-thumb a.added_to_cart.wc-forward,
        .woo-type11 ul.products li.product .product-thumb a.add_to_wishlist,
        .woo-type11 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a,
        .woo-type11 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a,
        .woo-type11 ul.products li.product:hover .product-wrapper:before,
        .woo-type11 ul.products li.product:hover .product-wrapper:after,
        .woo-type11.woocommerce ul.products li.product .product-thumb,
        .woo-type11 .woocommerce ul.products li.product .product-thumb,
        .woo-type11 ul.products li.product-category a img {
            border-color: #58aff2;
        }

        .woo-type12 ul.products li.product .product-thumb a.add_to_cart_button:hover,
        .woo-type12 ul.products li.product .product-thumb a.button.product_type_simple:hover,
        .woo-type12 ul.products li.product .product-thumb a.button.product_type_variable:hover,
        .woo-type12 ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover,
        .woo-type12 ul.products li.product .product-thumb a.add_to_wishlist:hover,
        .woo-type12 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover,
        .woo-type12 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover,
        .woo-type12 ul.products li.product:hover .product-details h5:after {
            background-color: #58aff2;
        }

        .woo-type12 ul.products li.product:hover .product-wrapper {
            border-color: #58aff2;
        }

        .woo-type14 ul.products li.product .product-thumb a.add_to_cart_button:hover,
        .woo-type14 ul.products li.product .product-thumb a.button.product_type_simple:hover,
        .woo-type14 ul.products li.product .product-thumb a.button.product_type_variable:hover,
        .woo-type14 ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover,
        .woo-type14 ul.products li.product .product-thumb a.add_to_wishlist:hover,
        .woo-type14 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover,
        .woo-type14 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover {
            background-color: #58aff2;
        }

        .woo-type15 ul.products li.product .product-thumb a.add_to_cart_button:hover:after,
        .woo-type15 ul.products li.product .product-thumb a.button.product_type_simple:hover:after,
        .woo-type15 ul.products li.product .product-thumb a.button.product_type_variable:hover:after,
        .woo-type15 ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover:after,
        .woo-type15 ul.products li.product .product-thumb a.add_to_wishlist:hover:after,
        .woo-type15 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover:after,
        .woo-type15 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover:after,
        .woo-type15.woocommerce ul.products li.product .featured-tag,
        .woo-type15 .woocommerce ul.products li.product .featured-tag,
        .woo-type15.woocommerce.single-product .featured-tag {
            background-color: #58aff2;
        }

        .woo-type15.woocommerce ul.products li.product .featured-tag:after,
        .woo-type15.woocommerce ul.products li.product:hover .featured-tag:after,
        .woo-type15 .woocommerce ul.products li.product .featured-tag:after,
        .woo-type15 .woocommerce ul.products li.product:hover .featured-tag:after,
        .woo-type15.woocommerce.single-product .featured-tag:after {
            border-color: #58aff2;
        }

        .woo-type17.woocommerce ul.products li.product .featured-tag,
        .woo-type17 .woocommerce ul.products li.product .featured-tag,
        .woo-type17.woocommerce.single-product .featured-tag {
            background-color: #58aff2;
        }

        .woo-type17 ul.products li.product .featured-tag:after,
        .woo-type17 ul.products li.product:hover .featured-tag:after,
        .woo-type17.woocommerce.single-product .featured-tag:after {
            border-color: #58aff2;
        }

        .woo-type18 ul.products li.product .product-thumb a.add_to_cart_button:hover,
        .woo-type18 ul.products li.product .product-thumb a.button.product_type_simple:hover,
        .woo-type18 ul.products li.product .product-thumb a.button.product_type_variable:hover,
        .woo-type18 ul.products li.product .product-thumb a.added_to_cart.wc-forward:hover,
        .woo-type18 ul.products li.product .product-thumb a.add_to_wishlist:hover,
        .woo-type18 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a:hover,
        .woo-type18 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a:hover {
            background-color: #58aff2;
        }

        .woo-type18 ul.products li.product:hover .product-details h5 {
            border-color: #58aff2;
        }

        .woo-type18 ul.products li.product:hover .product-details h5 a,
        .woo-type18 ul.products li.product-category:hover .product-details h5,
        .woo-type18 ul.products li.product-category:hover .product-details h5 .count,
        .woo-type18 ul.products li.product:hover .product-details .product-price .amount,
        .woo-type18 ul.products li.product:hover .product-details span.price,
        .woo-type18 ul.products li.product:hover .product-details span.price del,
        .woo-type18 ul.products li.product:hover .product-details span.price del .amount,
        .woo-type18 ul.products li.product:hover .product-details span.price ins,
        .woo-type18 ul.products li.product:hover .product-details span.price ins .amount,
        .woo-type18 ul.products li.product .product-details .product-rating-wrapper .star-rating:before,
        .woo-type18 ul.products li.product .product-details .product-rating-wrapper .star-rating span:before {
            color: #58aff2;
        }

        .woo-type20.woocommerce .shop_table th,
        .woo-type20 .woocommerce .shop_table th,
        .woo-type20.woocommerce div.product .woocommerce-tabs ul.tabs li.active a:after {
            background-color: #58aff2;
        }

        .woo-type20 ul.products li.product:hover .product-wrapper:after,
        .woo-type20 div.product div.images img {
            border-color: #58aff2;
        }

        .woo-type20.woocommerce-checkout #payment ul.payment_methods li a:hover {
            color: #58aff2;
        }

        .dt-sc-yoga-classes-sorting a:hover,
        .dt-sc-yoga-classes-sorting a.active-sort {
            background-color: #58aff2;
        }

        .yoga-single-meta li span {
            color: #58aff2;
        }

        .dt-sc-doctors .dt-sc-doctors-details ul.dt-sc-sociable li a:hover {
            background-color: #58aff2;
        }

        .dt-sc-event-sorting a,
        .dt-sc-event-month-meta li {
            color: #58aff2;
        }

        .dt-sc-model-sorting a:before {
            background-color: #58aff2;
        }

        .dt-sc-model-sorting a {
            color: #58aff2;
        }

        .has-tertiary-background-color {
            background-color: #0561aa;
        }

        .has-tertiary-color {
            color: #0561aa;
        }

        .dt-sc-triangle-title:before,
        .dt-sc-icon-box.type10 .icon-wrapper:after {
            background-color: #0561aa;
        }

        .woo-type2 div.product .summary a,
        .woo-type2 .shipping-calculator-button,
        .woo-type2.woocommerce-checkout #payment ul.payment_methods li a:hover {
            color: #0561aa;
        }

        .woo-type7 ul.products li.product .product-thumb a.add_to_wishlist,
        .woo-type7 ul.products li.product .product-thumb .yith-wcwl-wishlistaddedbrowse a,
        .woo-type7 ul.products li.product .product-thumb .yith-wcwl-wishlistexistsbrowse a,
        .woo-type7 ul.products li.product:hover .product-details {
            background-color: #0561aa;
        }

        .woo-type18 ul.products li.product:hover .product-wrapper:after {
            border-color: #0561aa;
        }

        .woo-type18 ul.products li.product:hover .product-details h5 a:hover,
        .woo-type18.woocommerce-checkout #payment ul.payment_methods li a:hover {
            color: #0561aa;
        }

        .yoga-video-sorting input[type="submit"],
        .dt-sc-yoga-classes .dt-sc-yoga-classes-image-overlay a:hover,
        .dt-sc-yoga-courses-sorting a.active-sort,
        .dt-sc-yoga-courses-sorting a:before,
        .dt-sc-yoga-course .dt-sc-yoga-course-thumb-overlay a:hover,
        .dt-sc-yoga-pose .dt-sc-yoga-pose-overlay a:hover,
        .dt-sc-yoga-teacher .dt-sc-yoga-teacher-overlay a:hover,
        .dt-sc-yoga-program .dt-sc-yoga-program-thumb-overlay a:hover {
            background-color: #0561aa;
        }

        .dt-sc-yoga-video .dt-sc-yoga-video-overlay p a {
            border-color: #0561aa;
        }

        .dt-sc-yoga-video .dt-sc-yoga-video-overlay p a,
        .dt-sc-yoga-video .dt-sc-yoga-video-overlay h6 a:hover,
        .dt-sc-yoga-courses-sorting a,
        .dt-sc-yoga-course .dt-sc-yoga-course-meta p,
        .dt-sc-yoga-course .dt-sc-yoga-course-details h6 a,
        .dt-sc-yoga-program .dt-sc-yoga-program-meta p,
        .dt-sc-yoga-program .dt-sc-yoga-program-details h6 a {
            color: #0561aa;
        }

        .dt-sc-event-month-thumb .dt-sc-event-month-date-wrapper {
            background-color: #0561aa;
        }

        .dt-sc-course .dt-sc-course-overlay a {
            background-color: #0561aa;
        }

        .dt-sc-faculty-sorting a:hover,
        .dt-sc-faculty-sorting a.active-sort,
        .dt-sc-course .dt-sc-course-details .dt-sc-course-meta p span,
        .dt-sc-course.no-course-thumb .dt-sc-course-details h5 a {
            color: #0561aa;
        }

        .dt-sc-training-details-overlay h6,
        .dt-sc-training-details-overlay .price,
        .dt-sc-training-details .dt-sc-training-details-overlay h6 {
            color: #0561aa;
        }

        body,
        .layout-boxed .inner-wrapper,
        .secondary-sidebar .type8 .widgettitle,
        .secondary-sidebar .type10 .widgettitle:after,
        .dt-sc-contact-info.type3::after,
        .dt-sc-image-caption .dt-sc-image-wrapper .icon-wrapper::after,
        ul.products li .product-wrapper,
        .woocommerce-tabs .panel,
        .select2-results,
        .woocommerce .woocommerce-message,
        .woocommerce .woocommerce-info,
        .woocommerce .woocommerce-error,
        .woocommerce div.product .woocommerce-tabs ul.tabs li.active,
        .woo-type13 ul.products li.product:hover .product-details h5 a,
        .tribe-events-list-separator-month span {
            background-color: #ffffff;
        }

        .dt-sc-image-caption.type8 .dt-sc-image-content::before {
            border-color: #ffffff;
        }

        .secondary-sidebar .type14 .widgettitle:before,
        .widget.buddypress div.item-options a.selected {
            border-bottom-color: #ffffff;
        }

        .dt-sc-testimonial.type2 blockquote::before {
            border-top-color: #ffffff;
        }

        body,
        .wp-block-pullquote {
            color: #000000;
        }

        a {
            color: #108bea;
        }

        a:hover {
            color: #333333;
        }

        .main-title-section h1,
        h1.simple-title {
            font-family: 'Open sans';
            font-size: 20px;
            font-weight: 400;
            letter-spacing: 0.5px;
            text-transform: none;
            color: #000000;
        }

        div.footer-widgets h3.widgettitle {
            font-family: 'Open sans';
            font-size: 20px;
            font-weight: 700;
            line-height: 36px;
            text-align: left;
            text-transform: none;
            color: #2b2b2b;
        }

        div.footer-widgets .widget {
            font-family: 'Open sans';
            font-size: 14px;
            font-weight: 400;
            line-height: 24px;
            text-align: left;
            text-transform: none;
            color: #333333;
        }

        #main-menu>ul.menu>li>a {
            font-size: 14px;
            font-weight: 400;
            letter-spacing: 0.5px;
            text-transform: none;
            color: #000000;
        }

        body {
            font-family: Open Sans;
            font-size: 14px;
            font-weight: normal;
            line-height: 24px;
            text-transform: none;
            color: #000000;
        }

        h1 {
            font-family: 'Open sans';
            font-size: 42px;
            font-weight: 400;
            letter-spacing: 0.5px;
            text-transform: none;
            color: #000000;
        }

        h2 {
            font-family: 'Open sans';
            font-size: 36px;
            font-weight: 400;
            letter-spacing: 0.5px;
            text-transform: none;
            color: #000000;
        }

        h3 {
            font-family: 'Open sans';
            font-size: 30px;
            font-weight: 400;
            letter-spacing: 0.5px;
            text-transform: none;
            color: #000000;
        }

        h4 {
            font-family: 'Open sans';
            font-size: 24px;
            font-weight: 400;
            letter-spacing: 0.5px;
            text-transform: none;
            color: #000000;
        }

        h5 {
            font-family: 'Open sans';
            font-size: 18px;
            font-weight: 400;
            letter-spacing: 0.5px;
            text-transform: none;
            color: #000000;
        }

        h6 {
            font-family: 'Open sans';
            font-size: 14px;
            font-weight: 700;
            letter-spacing: 0.5px;
            text-transform: none;
            color: #000000;
        }

        /* devanagari */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 100;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiAyp8kv8JHgFVrJJLmE0tDMPShSkFEkm8.woff) format('woff');
            unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 100;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiAyp8kv8JHgFVrJJLmE0tMMPShSkFEkm8.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 100;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiAyp8kv8JHgFVrJJLmE0tCMPShSkFE.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* devanagari */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 200;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmv1pVFteIYktMqlap.woff) format('woff');
            unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 200;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmv1pVGdeIYktMqlap.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 200;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmv1pVF9eIYktMqg.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* devanagari */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 300;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLm21lVFteIYktMqlap.woff) format('woff');
            unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 300;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLm21lVGdeIYktMqlap.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 300;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLm21lVF9eIYktMqg.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* devanagari */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 400;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiGyp8kv8JHgFVrJJLucXtGOvWDSHFF.woff) format('woff');
            unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 400;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiGyp8kv8JHgFVrJJLufntGOvWDSHFF.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 400;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiGyp8kv8JHgFVrJJLucHtGOvWDSA.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* devanagari */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 500;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmg1hVFteIYktMqlap.woff) format('woff');
            unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 500;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmg1hVGdeIYktMqlap.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 500;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmg1hVF9eIYktMqg.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* devanagari */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 600;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmr19VFteIYktMqlap.woff) format('woff');
            unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 600;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmr19VGdeIYktMqlap.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 600;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmr19VF9eIYktMqg.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* devanagari */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 700;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmy15VFteIYktMqlap.woff) format('woff');
            unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 700;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmy15VGdeIYktMqlap.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 700;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmy15VF9eIYktMqg.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* devanagari */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 800;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLm111VFteIYktMqlap.woff) format('woff');
            unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 800;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLm111VGdeIYktMqlap.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 800;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLm111VF9eIYktMqg.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* devanagari */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 900;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLm81xVFteIYktMqlap.woff) format('woff');
            unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 900;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLm81xVGdeIYktMqlap.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 900;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLm81xVF9eIYktMqg.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* devanagari */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 100;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiGyp8kv8JHgFVrLPTucXtGOvWDSHFF.woff) format('woff');
            unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 100;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiGyp8kv8JHgFVrLPTufntGOvWDSHFF.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 100;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiGyp8kv8JHgFVrLPTucHtGOvWDSA.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* devanagari */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 200;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLFj_Z11lE92JQEl8qw.woff) format('woff');
            unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 200;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLFj_Z1JlE92JQEl8qw.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 200;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLFj_Z1xlE92JQEk.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* devanagari */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 300;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLDz8Z11lE92JQEl8qw.woff) format('woff');
            unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 300;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLDz8Z1JlE92JQEl8qw.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 300;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLDz8Z1xlE92JQEk.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* devanagari */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiEyp8kv8JHgFVrJJbedHFHGPezSQ.woff) format('woff');
            unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiEyp8kv8JHgFVrJJnedHFHGPezSQ.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiEyp8kv8JHgFVrJJfedHFHGPc.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* devanagari */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 500;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLGT9Z11lE92JQEl8qw.woff) format('woff');
            unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 500;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLGT9Z1JlE92JQEl8qw.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 500;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLGT9Z1xlE92JQEk.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* devanagari */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 600;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLEj6Z11lE92JQEl8qw.woff) format('woff');
            unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 600;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLEj6Z1JlE92JQEl8qw.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 600;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLEj6Z1xlE92JQEk.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* devanagari */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 700;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLCz7Z11lE92JQEl8qw.woff) format('woff');
            unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 700;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLCz7Z1JlE92JQEl8qw.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 700;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLCz7Z1xlE92JQEk.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* devanagari */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 800;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLDD4Z11lE92JQEl8qw.woff) format('woff');
            unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 800;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLDD4Z1JlE92JQEl8qw.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 800;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLDD4Z1xlE92JQEk.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* devanagari */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 900;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLBT5Z11lE92JQEl8qw.woff) format('woff');
            unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 900;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLBT5Z1JlE92JQEl8qw.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 900;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLBT5Z1xlE92JQEk.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* cyrillic-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 300;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk5hkWV0exoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }

        /* cyrillic */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 300;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk5hkWVQexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }

        /* greek-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 300;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk5hkWVwexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+1F00-1FFF;
        }

        /* greek */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 300;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk5hkWVMexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0370-03FF;
        }

        /* hebrew */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 300;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk5hkWVIexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0590-05FF, U+200C-2010, U+20AA, U+25CC, U+FB1D-FB4F;
        }

        /* vietnamese */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 300;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk5hkWV8exoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 300;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk5hkWV4exoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 300;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk5hkWVAexoMUdjFnmg.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* cyrillic-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 400;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk8ZkWV0exoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }

        /* cyrillic */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 400;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk8ZkWVQexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }

        /* greek-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 400;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk8ZkWVwexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+1F00-1FFF;
        }

        /* greek */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 400;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk8ZkWVMexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0370-03FF;
        }

        /* hebrew */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 400;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk8ZkWVIexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0590-05FF, U+200C-2010, U+20AA, U+25CC, U+FB1D-FB4F;
        }

        /* vietnamese */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 400;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk8ZkWV8exoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 400;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk8ZkWV4exoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 400;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk8ZkWVAexoMUdjFnmg.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* cyrillic-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 600;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0RkxhjWV0exoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }

        /* cyrillic */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 600;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0RkxhjWVQexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }

        /* greek-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 600;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0RkxhjWVwexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+1F00-1FFF;
        }

        /* greek */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 600;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0RkxhjWVMexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0370-03FF;
        }

        /* hebrew */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 600;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0RkxhjWVIexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0590-05FF, U+200C-2010, U+20AA, U+25CC, U+FB1D-FB4F;
        }

        /* vietnamese */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 600;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0RkxhjWV8exoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 600;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0RkxhjWV4exoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 600;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0RkxhjWVAexoMUdjFnmg.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* cyrillic-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 700;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0RkyFjWV0exoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }

        /* cyrillic */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 700;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0RkyFjWVQexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }

        /* greek-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 700;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0RkyFjWVwexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+1F00-1FFF;
        }

        /* greek */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 700;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0RkyFjWVMexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0370-03FF;
        }

        /* hebrew */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 700;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0RkyFjWVIexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0590-05FF, U+200C-2010, U+20AA, U+25CC, U+FB1D-FB4F;
        }

        /* vietnamese */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 700;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0RkyFjWV8exoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 700;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0RkyFjWV4exoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 700;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0RkyFjWVAexoMUdjFnmg.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* cyrillic-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 800;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk0ZjWV0exoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }

        /* cyrillic */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 800;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk0ZjWVQexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }

        /* greek-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 800;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk0ZjWVwexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+1F00-1FFF;
        }

        /* greek */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 800;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk0ZjWVMexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0370-03FF;
        }

        /* hebrew */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 800;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk0ZjWVIexoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0590-05FF, U+200C-2010, U+20AA, U+25CC, U+FB1D-FB4F;
        }

        /* vietnamese */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 800;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk0ZjWV8exoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 800;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk0ZjWV4exoMUdjFnmiU_.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 800;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk0ZjWVAexoMUdjFnmg.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* cyrillic-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 300;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsiH0B4taVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }

        /* cyrillic */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 300;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsiH0B4kaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }

        /* greek-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 300;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsiH0B4saVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+1F00-1FFF;
        }

        /* greek */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 300;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsiH0B4jaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0370-03FF;
        }

        /* hebrew */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 300;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsiH0B4iaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0590-05FF, U+200C-2010, U+20AA, U+25CC, U+FB1D-FB4F;
        }

        /* vietnamese */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 300;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsiH0B4vaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 300;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsiH0B4uaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 300;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsiH0B4gaVQUwaEQXjM.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* cyrillic-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 400;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsjZ0B4taVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }

        /* cyrillic */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 400;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsjZ0B4kaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }

        /* greek-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 400;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsjZ0B4saVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+1F00-1FFF;
        }

        /* greek */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 400;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsjZ0B4jaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0370-03FF;
        }

        /* hebrew */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 400;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsjZ0B4iaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0590-05FF, U+200C-2010, U+20AA, U+25CC, U+FB1D-FB4F;
        }

        /* vietnamese */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 400;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsjZ0B4vaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 400;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsjZ0B4uaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 400;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsjZ0B4gaVQUwaEQXjM.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* cyrillic-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 600;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsgH1x4taVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }

        /* cyrillic */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 600;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsgH1x4kaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }

        /* greek-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 600;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsgH1x4saVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+1F00-1FFF;
        }

        /* greek */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 600;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsgH1x4jaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0370-03FF;
        }

        /* hebrew */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 600;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsgH1x4iaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0590-05FF, U+200C-2010, U+20AA, U+25CC, U+FB1D-FB4F;
        }

        /* vietnamese */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 600;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsgH1x4vaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 600;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsgH1x4uaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 600;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsgH1x4gaVQUwaEQXjM.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* cyrillic-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 700;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsg-1x4taVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }

        /* cyrillic */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 700;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsg-1x4kaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }

        /* greek-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 700;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsg-1x4saVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+1F00-1FFF;
        }

        /* greek */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 700;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsg-1x4jaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0370-03FF;
        }

        /* hebrew */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 700;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsg-1x4iaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0590-05FF, U+200C-2010, U+20AA, U+25CC, U+FB1D-FB4F;
        }

        /* vietnamese */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 700;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsg-1x4vaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 700;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsg-1x4uaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 700;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsg-1x4gaVQUwaEQXjM.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* cyrillic-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 800;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgshZ1x4taVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }

        /* cyrillic */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 800;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgshZ1x4kaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }

        /* greek-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 800;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgshZ1x4saVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+1F00-1FFF;
        }

        /* greek */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 800;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgshZ1x4jaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0370-03FF;
        }

        /* hebrew */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 800;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgshZ1x4iaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0590-05FF, U+200C-2010, U+20AA, U+25CC, U+FB1D-FB4F;
        }

        /* vietnamese */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 800;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgshZ1x4vaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }

        /* latin-ext */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 800;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgshZ1x4uaVQUwaEQXjN_mQ.woff) format('woff');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 800;
            font-stretch: normal;
            font-display: swap;
            src: url({{ config('app.url') }}/wp-content/fonts/open-sans/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgshZ1x4gaVQUwaEQXjM.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
    </style>
    <style type="text/css" data-type="vc_custom-css">
        .vcr_top-align ul.slick-dots {
            padding-top: 50px;
        }
    </style>
    <style type="text/css" data-type="vc_shortcodes-custom-css">
        .vc_custom_1505975565958 {
            padding-bottom: 5% !important;
        }

        .vc_custom_1505912071786 {
            margin-bottom: 7% !important;
            padding-top: 10% !important;
            padding-bottom: 10% !important;
        }

        .vc_custom_1505827997454 {
            padding-top: 80px !important;
            padding-bottom: 80px !important;
            background-color: #f0f3f6 !important;
        }

        .vc_custom_1505906682200 {
            margin-bottom: 6% !important;
        }

        .vc_custom_1505975565958 {
            padding-bottom: 5% !important;
        }

        .vc_custom_1505823797609 {
            margin-bottom: 20px !important;
        }

        .vc_custom_1505913847098 {
            margin-bottom: 20px !important;
            max-width: 400px;
            max-height: 200px;
        }

        .vc_custom_1505913882341 {
            margin-bottom: 20px !important;
        }

        .vc_custom_1505913899213 {
            margin-bottom: 20px !important;
        }

        .vc_custom_1507266410979 {
            padding-left: 20px !important;
            background-image: url({{ config('app.url') }}/wp-content/uploads/2017/09/img1-6.jpg?id=9146) !important;
        }

        .vc_custom_1505823797609 {
            margin-bottom: 20px !important;
        }

        .vc_custom_1505823797609 {
            margin-bottom: 20px !important;
        }

        .vc_custom_1505906822211 {
            padding-top: 11px !important;
        }

        .vc_custom_1505823797609 {
            margin-bottom: 20px !important;
        }

        .vc_custom_1506504484899 {
            margin-bottom: 0px !important;
        }

        .vc_custom_1506504508253 {
            margin-bottom: 0px !important;
        }

        .vc_custom_1506504521193 {
            margin-bottom: 0px !important;
        }

        .vc_custom_1505823797609 {
            margin-bottom: 20px !important;
        }
    </style><noscript><style> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
</head>
