<div id="header-wrapper" class="">
    <header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="column dt-sc-one-half first">
                    <p>{{__('site.top_title')}}</p>
                </div>
                <div class="column dt-sc-one-half alignright">
                    <div class="text-with-icon"><span class="fa fa-map-marker"></span>
                        <a onclick="scrollToElem('footer')" title="{{__('site.top_location')}}" target="_self"
                           href="javascript:void(0)">
                            {{__('site.top_location')}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- **Main Header Wrapper** -->
        <div id="main-header-wrapper-sticky-wrapper" class="sticky-wrapper" style="height: 100px;">
            <div id="main-header-wrapper" class="main-header-wrapper" style="">

                <div class="container">

                    <!-- **Main Header** -->
                    <div class="main-header">
                        <div id="logo"><a href="{{ config('app.url') }}/" rel="home">
                                <img class="normal_logo"
                                     src="{{ storage_path('web/' . settings('site', 'logo', '', '*')) }}"
                                     alt="CCTV" title="CCTV">
                            </a></div>
                        <div id="menu-wrapper" class="menu-wrapper menu-default">
                            <div onclick="toggleMenu()" class="dt-menu-toggle" id="dt-menu-toggle">
                                {{__('site.menu')}} <span class="dt-menu-toggle-icon"></span>
                            </div>
                            <nav id="main-menu" class="menu-menu1-container">
                                <ul class="menu">
                                    <li
                                            onclick="scrollToElem('about-us-block')"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home page_item page-item-9275">
                                        <a href="javascript:void(0)">{{__('site.about_us')}}</a>
                                    </li>
                                    <li
                                            onclick="scrollToElem('our-success-block')"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home page_item page-item-9275">
                                        <a href="javascript:void(0)">{{__('site.our_success')}}</a>
                                    </li>
                                    <li
                                            onclick="scrollToElem('advantages-block')"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home page_item page-item-9275">
                                        <a href="javascript:void(0)">{{__('site.advantages')}}</a>
                                    </li>
                                    <li
                                            onclick="scrollToElem('partners-block')"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home page_item page-item-9275">
                                        <a href="javascript:void(0)">{{__('site.partners')}}</a>
                                    </li>
                                    <li
                                            onclick="scrollToElem('contact-us')"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home page_item page-item-9275">
                                        <a href="javascript:void(0)">{{__('site.contact_us')}}</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- **Main Header** -->
        <div id="slider">
            <div class="dt-sc-main-slider">
                <div class="container">
                    <div class="dt-sc-title script-with-sub-title banner-title">
                        <h1>{{__('site.banner_title')}}</h1>
                        <a href="javascript:void(0)" onclick="scrollToElem('contact-us')" target="_self" title=""
                           class="dt-sc-button   medium   bordered  ">
                            {{__('site.contact_us')}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
</div>
<style>
    .dt-sc-main-slider {
        height: 700px;
        background-image: url('https://vigil.wpengine.com/wp-content/uploads/revslider/cctv-home/slider2.jpg');
        width: 100%;
    }

    .dt-sc-title.script-with-sub-title.banner-title {
        text-align: right;
        margin-right: 100px;
        margin-top: 60px;
        float: right;
        width: 700px;
    }

    .dt-sc-title.script-with-sub-title.banner-title h1 {
        font-size: 36px;
        color: #fff;
    }

    @media only screen and (max-width: 1024px) {
        .dt-sc-title.script-with-sub-title.banner-title {
            text-align: center;
            margin-top: 100px;
            float: initial;
            width: 100%;
        }
    }
</style>
