<!doctype html>
<html lang="en">
<head>
    @include('layouts.parts._head')
</head>
<body class="home page-template-default page page-id-9275 wp-embed-responsive theme-vigil woocommerce-js layout-wide fullwidth-header semi-transparent-header sticky-header standard-header header-with-topbar woo-type1 page-with-slider no-breadcrumb wpb-js-composer js-comp-ver-6.5.0 vc_responsive">
<div class="wrapper">
    <div class="inner-wrapper">
        @include('layouts.parts._header')
        @yield('content')
        @include('layouts.parts._footer')
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
                integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
                crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
                integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
                crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
                integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
                crossorigin="anonymous"></script>

        <script type='text/javascript'
                src='{{ config('app.url') }}/wp-content/plugins/revslider/public/assets/js/rbtools.min.js'
                id='tp-tools-js'></script>
        <script type='text/javascript'
                src='{{ config('app.url') }}/wp-content/plugins/revslider/public/assets/js/rs6.min.js'
                id='revmin-js'></script>
        <script type='text/javascript'
                src='{{ config('app.url') }}/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js'
                id='jquery-blockui-js'></script>
        <script type='text/javascript' id='wc-add-to-cart-js-extra'>
            /* <![CDATA[ */
            var wc_add_to_cart_params = {
                "ajax_url": "\/wp-admin\/admin-ajax.php",
                "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
                "i18n_view_cart": "View cart",
                "cart_url": "https:\/\/vigil.wpengine.com\/cart\/",
                "is_cart": "",
                "cart_redirect_after_add": "no"
            };
            /* ]]> */
        </script>
        <script type='text/javascript'
                src='{{ config('app.url') }}/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js'
                id='wc-add-to-cart-js'></script>
        <script type='text/javascript'
                src='{{ config('app.url') }}/wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart.js'
                id='vc_woocommerce-add-to-cart-js-js'></script>
        <script type='text/javascript'
                src='{{ config('app.url') }}/wp-content/plugins/Ultimate_VC_Addons/assets/min-js/ultimate-params.min.js'
                id='ultimate-vc-params-js'></script>
        <script type='text/javascript'
                src='{{ config('app.url') }}/wp-content/plugins/Ultimate_VC_Addons/assets/min-js/custom.min.js'
                id='ultimate-custom-js'></script>
        <script type='text/javascript'
                src='{{ config('app.url') }}/wp-content/plugins/Ultimate_VC_Addons/assets/min-js/jquery-appear.min.js'
                id='ultimate-appear-js'></script>
        <script type='text/javascript'
                src='{{ config('app.url') }}/wp-content/plugins/Ultimate_VC_Addons/assets/min-js/slick.min.js'
                id='ult-slick-js'></script>
        <script type='text/javascript'
                src='{{ config('app.url') }}/wp-content/plugins/Ultimate_VC_Addons/assets/min-js/slick-custom.min.js'
                id='ult-slick-custom-js'></script>
        <script type='text/javascript'
                src='{{ config('app.url') }}/wp-content/themes/vigil/framework/js/modernizr.custom.js'
                id='modernizr-custom-js'></script>
        <script type='text/javascript'
                src='{{ config('app.url') }}/assets/js/countMe.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"
                integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg=="
                crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://unpkg.com/imask"></script>
        @yield('script')
    </div>
</div>
</body>
</html>
