<?php

return [
    'name' => 'Имя',
    'guard_name' => 'Guard',
    'created_at' => 'Создан',
    'updated_at' => 'Обновлен',
    'permissions' => 'Доступы',
];
