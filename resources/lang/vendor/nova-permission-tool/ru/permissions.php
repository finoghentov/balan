<?php

return [
    'name' => 'Название',
    'display_name' => 'Отображаемое имя',
    'guard_name' => 'Guard',
    'created_at' => 'Создан',
    'updated_at' => 'Обновлен',
    'roles' => 'Роли'
];
