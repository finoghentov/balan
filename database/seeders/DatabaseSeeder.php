<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $permissions = [
            'users' => [
                Permission::query()->firstOrCreate(['name' => 'view users', 'guard_name' => 'web']),
                Permission::query()->firstOrCreate(['name' => 'create users', 'guard_name' => 'web']),
                Permission::query()->firstOrCreate(['name' => 'update users', 'guard_name' => 'web']),
                Permission::query()->firstOrCreate(['name' => 'delete users', 'guard_name' => 'web']),
            ],
            'clients' => [
                Permission::query()->firstOrCreate(['name' => 'view clients', 'guard_name' => 'web']),
                Permission::query()->firstOrCreate(['name' => 'create clients', 'guard_name' => 'web']),
                Permission::query()->firstOrCreate(['name' => 'update clients', 'guard_name' => 'web']),
                Permission::query()->firstOrCreate(['name' => 'delete clients', 'guard_name' => 'web']),
            ],
            'site' => [
                Permission::query()->firstOrCreate(['name' => 'manage site', 'guard_name' => 'web'])
            ]
        ];

        $roles = [
            config('permission.admin_role_name') => [
                'users', 'clients', 'site'
            ],
            'Менеджер сайта' => [
                'site'
            ],
            'Оператор' => [
                'clients'
            ]
        ];

        foreach ($roles as $role => $rolePermissions) {
            $role = Role::query()->firstOrCreate([
                'name' => $role,
                'guard_name' => 'web'
            ]);

            foreach ($rolePermissions as $permission) {
                $role->givePermissionTo($permissions[$permission]);
            }
        }

        $this->createAdmin();
    }

    protected function createAdmin()
    {
        if (!$user = User::query()->whereEmail('admin@gmail.com')->first()) {
            $user = User::query()->create([
                'name' => 'Admin',
                'email' => 'admin@gmail.com',
                'password' => Hash::make('admin')
            ]);
        }

        $user->assignRole(Role::query()->firstOrCreate([
            'name' => config('permission.admin_role_name'),
            'guard_name' => 'web'
        ]));
    }
}
